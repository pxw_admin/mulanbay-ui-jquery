## 项目介绍
木兰湾管理系统是用于管理个人消费、锻炼、音乐、阅读、健康、饮食、人生经历等各个衣食住行信息的系统，通过提醒、计划模块利用调度系统来统计分析执行情况。
并通过积分和评分体系来综合评估个人的总体状态。

该系统是前后端分离的项目，当前项目mulanbay-ui-jquery为前端PC版本，需要结合后端项目才能完整访问。
这个Jquery版本的PC端是最早开发的，项目模式比较简单。

木兰湾管理系统后端项目：
* 服务器端[mulanbay-server](https://gitee.com/mulanbay/mulanbay-server)

木兰湾管理系统前端项目：

VUE版本
* 基于Vue的前端(PC端)[mulanbay-ui-vue](https://gitee.com/mulanbay/mulanbay-ui-vue)
* 基于Vue的前端(移动端)[mulanbay-mobile-vue](https://gitee.com/mulanbay/mulanbay-mobile-vue)

Jquery版本(V3.0版本后不再维护，以VUE版本为主)
* 基于Jquery的前端(PC端)[mulanbay-ui-jquery](https://gitee.com/mulanbay/mulanbay-ui-jquery)
* 基于Jquery的前端(移动端)[mulanbay-mobile-jquery](https://gitee.com/mulanbay/mulanbay-mobile-jquery)

[木兰湾项目说明](https://gitee.com/mulanbay)

## 开发说明

### 所用技术

Jquery、EasyUI、Echarts

### 文档地址

木兰湾文档[https://www.yuque.com/mulanbay/rgvt6k/uy08n4](https://www.yuque.com/mulanbay/rgvt6k/uy08n4)

### 运行方式

项目为纯粹的html+css方式，因此没有打包等步骤，直接双击就可以在浏览器运行

主页地址: html/main/main.html

由于项目采用的是EasyUI，里面的默认请求无法设置header个性化内容，而后端的权限是基于header中的Authorization字段，
因此如果要正常使用该Jquery版本，需要在后端的配置文件中设置令牌获取方式为cookie。

```
security.token.way=cookie

```

## 项目截图
<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1116/153740_6c3633bb_352331.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/1116/153808_a7fbf93b_352331.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/1116/153830_c0f8f54b_352331.png"/></td>
    </tr>

</table>