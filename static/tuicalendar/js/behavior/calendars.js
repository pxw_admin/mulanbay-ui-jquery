'use strict';

/* eslint-disable require-jsdoc, no-unused-vars */

var CalendarList = [];

function CalendarInfo() {
    this.id = null;
    this.name = null;
    this.checked = true;
    this.color = null;
    this.bgColor = null;
    this.borderColor = null;
    this.dragBgColor = null;
}

function addCalendar(calendar) {
    CalendarList.push(calendar);
}

function findCalendar(id) {
    var found;

    CalendarList.forEach(function(calendar) {
        if (calendar.id === id) {
            found = calendar;
        }
    });

    return found || CalendarList[0];
}

function hexToRGBA(hex) {
    var radix = 16;
    var r = parseInt(hex.slice(1, 3), radix),
        g = parseInt(hex.slice(3, 5), radix),
        b = parseInt(hex.slice(5, 7), radix),
        a = parseInt(hex.slice(7, 9), radix) / 255 || 1;
    var rgba = 'rgba(' + r + ', ' + g + ', ' + b + ', ' + a + ')';

    return rgba;
}

(function() {
    //加载数据
    var url='/common/getEnumTree?enumClass=UserBehaviorType&idType=ORDINAL';
    var colors = ['#9e5fff','#00a9ff','#ff5583','#03bd9e','#bbdc00','#ffbb3b','#191970','#800080','#EE82EE','#B22222'];
    //不能异步执行，否则app.js方法setSchedules先执行了，导致第一次无法显示日历
    doAjax3(null,url,'GET',false,false,false,function(data){
        var n = data.length;
        for(var i=0;i<n;i++){
            var calendar = new CalendarInfo();
            calendar.id = data[i].id;
            calendar.name = data[i].text;
            calendar.color = '#ffffff';
            calendar.bgColor = colors[i];
            calendar.dragBgColor = colors[i];
            calendar.borderColor = colors[i];
            addCalendar(calendar);
        }
    });

})();
