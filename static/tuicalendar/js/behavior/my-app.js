function changeDataSource(dataSource) {
    $('#dataSource').val(dataSource);
    setSchedules();
}

function getUserCalendars(viewName,renderStart, renderEnd) {
    var dataSource = $('#dataSource').val();
    if(dataSource==null||dataSource=='BEHAVIOR_DATA'){
        getUserBehaviorCalendars(viewName,renderStart, renderEnd);
    }else{
        getUserOperateCalendars(viewName,renderStart, renderEnd);
    }
}

function getUserBehaviorCalendars(viewName,renderStart, renderEnd) {
    ScheduleList = [];
    var para = {
        startDate:(new Date(renderStart)).Format("yyyy-MM-dd"),
        endDate:(new Date(renderEnd)).Format("yyyy-MM-dd"),
    };
    var url='/userBehavior/calendarStat';
    //不能异步执行，否则app.js方法setSchedules先执行了，导致第一次无法显示日历
    doAjax3(para,url,'GET',false,false,false,function(data){
        var n = data.length;
        for(var i=0;i<n;i++){
            generateUserSchedule(data[i],renderStart, renderEnd);
        }
        setTotalInfo(n);
    });
}

function getUserOperateCalendars(viewName,renderStart, renderEnd) {
    ScheduleList = [];
    var para = {
        startTime:(new Date(renderStart)).Format("yyyy-MM-dd"),
        endTime:(new Date(renderEnd)).Format("yyyy-MM-dd")+' 23:59:59',
        page:0,
        pageSize:10
    };
    var url='/userOperationConfig/userOperationStat';
    //不能异步执行，否则app.js方法setSchedules先执行了，导致第一次无法显示日历
    doAjax3(para,url,'GET',false,true,false,function(data){
        var n = 0;
        for(var i=0; i<data.length; i++){
            var pdata =data[i].operations;
            for(var j=0;j<pdata.length;j++){
                var title = pdata[j].content;
                var uc = {
                    id : ++n,
                    behaviorTypeIndex: pdata[j].behaviorTypeIndex,
                    title:title,
                    content:pdata[j].content,
                    readOnly:true,
                    bussDay:pdata[j].occurTime,
                    expireTime:pdata[j].occurTime,
                    allDay:false,
                    delayCounts:0,
                    location:data[i].title,
                    remark:'无'
                };
                generateUserSchedule(uc,renderStart, renderEnd);
            }
        }
        setTotalInfo(n);
    });
}

function setTotalInfo(n) {
    setElementInnerHTML('totalCalendars','<font color="green">共'+n+'条数据</font>');
}
function generateUserSchedule(uc, renderStart, renderEnd) {
    var schedule = new ScheduleInfo();
    var calendar = CalendarList[uc.behaviorTypeIndex];
    schedule.id = uc.id;
    schedule.calendarId = calendar.id;
    schedule.title = uc.title;
    schedule.body = uc.content;
    schedule.isReadOnly = true;
    generateUserCalendarTime(uc,schedule);
    schedule.isPrivate = false;
    if(null!=uc.location){
        schedule.location=uc.location;
    }
    //参与者，目前为超链接
    schedule.attendees = [];
    schedule.recurrenceRule = uc.delayCounts>0 ? '延迟次数:'+uc.delayCounts : '';
    schedule.state = 'Free';
    schedule.color = calendar.color;
    schedule.bgColor = calendar.bgColor;
    schedule.dragBgColor = calendar.dragBgColor;
    schedule.borderColor = calendar.borderColor;

    if (schedule.category === 'milestone') {
        schedule.color = schedule.bgColor;
        schedule.bgColor = 'transparent';
        schedule.dragBgColor = 'transparent';
        schedule.borderColor = 'transparent';
    }
    schedule.raw.memo = uc.remark;
    ScheduleList.push(schedule);
}

function generateUserCalendarTime(uc,schedule) {
    schedule.isAllday = uc.allDay;
    var startDate = moment(uc.bussDay);
    var endDate = moment(uc.expireTime);
    schedule.start= startDate.toDate();
    schedule.end=endDate.toDate();
    console.log(schedule.start);
    console.log(schedule.end);
    schedule.goingDuration = 5;
    schedule.comingDuration = 5;
    //The schedule type('milestone', 'task', allday', 'time')
    if (schedule.isAllday) {
        schedule.category = 'allday';
    }else {
        schedule.category = 'time';
    }
}
