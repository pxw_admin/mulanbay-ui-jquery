function getUserCalendars(viewName,renderStart, renderEnd) {
    ScheduleList = [];
    var para = {
        startDate:(new Date(renderStart)).Format("yyyy-MM-dd"),
        endDate:(new Date(renderEnd)).Format("yyyy-MM-dd"),
        needFinished: $('#needFinished').is(':checked'),
        needPeriod:$('#needPeriod').is(':checked'),
        needBudget: $('#needBudget').is(':checked'),
        needTreatDrug: $('#needTreatDrug').is(':checked'),
        needBandLog:$('#needBandLog').is(':checked')
    };
    var url='/userCalendar/getList';
    //不能异步执行，否则app.js方法setSchedules先执行了，导致第一次无法显示日历
    doAjax3(para,url,'GET',false,false,false,function(data){
        var n = data.length;
        for(var i=0;i<n;i++){
            generateUserSchedule(data[i],renderStart, renderEnd);
        }
        setElementInnerHTML('totalCalendars','<font color="green">共'+n+'个日历</font>')
    });
}

function generateUserSchedule(uc, renderStart, renderEnd) {
    var schedule = new ScheduleInfo();
    var calendar = CalendarList[uc.sourceTypeIndex];
    schedule.id = uc.id;
    schedule.calendarId = calendar.id;
    if(uc.finishTypeName==null){
        schedule.title = uc.title;
        schedule.body = uc.content;
    }else{
        if(uc.match){
            schedule.title = '★'+uc.title;
        }else{
            schedule.title = '☆'+uc.title;
        }
        var cc = '<img src="../../static/image/blue_star.png"></img>日历内容:'+uc.content+'<br>';
        cc+='<img src="../../static/image/blue_star.png"></img>完成类型:'+uc.finishTypeName;
        if(!isStrEmpty(uc.finishedTime)){
            cc+=','+uc.finishedTime;
        }
        if(false==uc.match){
            cc+='<img src="../../static/image/unmatch.png" onclick="showInfoMsg(\'实际完成时间与日历的设置时间没有匹配\')"></img>';
        }else{
            cc+='<img src="../../static/image/match.png" onclick="showInfoMsg(\'实际完成时间与日历的设置时间匹配\')"></img>';
        }
        cc+='<br>';
        if(uc.value!=null){
            cc+='<img src="../../static/image/blue_star.png"></img>完成详情:'+uc.value;
        }
        if(uc.unit!=null){
            cc+=uc.unit;
        }
        schedule.body = cc;
    }
    schedule.isReadOnly = uc.readOnly;
    //generateUserCalendarTime(uc,schedule);
    generateUserCalendarTime(uc,schedule);
    schedule.isPrivate = false;
    if(null!=uc.location){
        schedule.location=uc.location;
    }
    //参与者，目前为超链接
    schedule.attendees = [];
    if(uc.messageId!=null){
        schedule.attendees.push('<a href="#" onclick="showOriginMessage('+uc.messageId+')">源消息内容</a>');
    }
    if(uc.sourceId!=null){
        schedule.attendees.push('<a href="#" onclick="showCalendarSource('+uc.id+')">来源信息</a>');
    }
    if('ONCE'==uc.period){
        schedule.recurrenceRule = uc.delayCounts>0 ? '延迟次数:'+uc.delayCounts : '';
    }else{
        schedule.recurrenceRule = uc.periodName;
        if(uc.periodValues){
            if('WEEKLY'==uc.period){
                schedule.recurrenceRule += '（周:'+uc.periodValues.replace('7','日')+'）';
            }else if('MONTHLY'==uc.period){
                schedule.recurrenceRule += '（'+uc.periodValues+'号）';
            }else{
                schedule.recurrenceRule += '（'+uc.periodValues+'）';
            }
        }
    }
    schedule.state = 'Free';
    schedule.color = calendar.color;
    schedule.bgColor = calendar.bgColor;
    schedule.dragBgColor = calendar.dragBgColor;
    schedule.borderColor = calendar.borderColor;

    if (schedule.category === 'milestone') {
        schedule.color = schedule.bgColor;
        schedule.bgColor = 'transparent';
        schedule.dragBgColor = 'transparent';
        schedule.borderColor = 'transparent';
    }
    schedule.raw.memo = uc.remark;
    // schedule.raw.creator.name = '系统产生';
    // schedule.raw.creator.avatar = '../../static/image/tick.png';
    // schedule.raw.creator.company = '木兰湾';
    // schedule.raw.creator.email = 'fenghong007@hotmail.com';
    // schedule.raw.creator.phone = '123';
    ScheduleList.push(schedule);
}

function generateUserCalendarTime(uc,schedule) {
    var startDate = moment(uc.bussDay)
    var endDate = moment(uc.expireTime);
    schedule.start= startDate.toDate();
    schedule.end=endDate.toDate();
    //todo 日历开始的前后时间（分钟）
    schedule.goingDuration = 30;
    schedule.comingDuration = 30;
    schedule.isAllday = uc.allDay;
    //The schedule type('milestone', 'task', allday', 'time')
    if (schedule.isAllday) {
        schedule.category = 'allday';
    }else {
        schedule.category = 'time';
    }
}

function showOriginMessage(messageId) {
    var url='/userMessage/getByUser?id='+ messageId;
    doAjax(null,url,'GET',false,function(data){
        if(data){
            $.messager.alert(data.title, data.content, 'info');
        }else{
            $.messager.alert('提示', '未找到原始的消息内容', 'info');
        }
    });
}

function showCalendarSource(id) {
    var url='/userCalendar/getSource?id='+ id;
    doAjax(null,url,'GET',false,function(data){
        if(data){
            alert(json2String(data));
        }else{
            $.messager.alert('提示', '未找到来源内容', 'info');
        }
    });
}

function createUserCalendar(e) {
    var postData={
        calendarId:e.calendarId,
        title:e.title,
        allDay:e.isAllDay,
        location:e.location
    };
    if(e.start){
        postData.bussDay=formatDate2String(e.start._date);
    }
    if(e.end){
        postData.expireTime=formatDate2String(e.end._date);
    }
    var vurl='/userCalendar/create2';
    doAjax3(postData, vurl, 'POST', true,true,false, function(dd) {
        saveNewSchedule(e);
    });
}

function updateUserCalendar(schedule,changes) {
    if(changes==null){
        return;
    }
    var postData={
        id:schedule.id,
        calendarId:schedule.calendarId,
        title:schedule.title,
        allDay:schedule.isAllDay,
        location:schedule.location
    };
    for(let key in changes) {
        if(postData[key]){
            postData[key] = changes[key];
        }
    }
    if(changes['isAllDay']){
        postData.allDay=changes.isAllDay;
    }
    if(changes.start){
        postData.bussDay=formatDate2String(changes.start._date);
    }
    if(changes.end){
        postData.expireTime=formatDate2String(changes.end._date);
    }
    //console.log(json2String(changes));
    var url='/userCalendar/update';
    doAjax2(postData,url,'POST',false,false,function(data){
        refreshScheduleVisibility();
    });
}

function deleteUserCalendar(e) {
    $.messager.confirm('提示信息', '是否要删除该条日历?', function(data) {
        if (data) {
            var vurl = '/userCalendar/delete';
            var postData={ids:e.schedule.id};
            doAjax3(postData, vurl, 'POST', true,true,false, function(dd) {
                cal.deleteSchedule(e.schedule.id, e.schedule.calendarId);
            });
        }
    });
}

function finishUserCalendar(e) {
    $.messager.confirm('提示信息', '您是否要把该条行程手动设置为完成?', function(data) {
        if (data) {
            var vurl='/userCalendar/finish';
            var postData={id:e.schedule.id};
            doAjax3(postData, vurl, 'POST', true,true,false, function(dd) {
                cal.deleteSchedule(e.schedule.id, e.schedule.calendarId);
            });
        }
    });
}

function refreshUserCalendar() {
    setSchedules();
}
function formatDate2String(d) {
    return d.Format("yyyy-MM-dd hh:mm:ss")
}
