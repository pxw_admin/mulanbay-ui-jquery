function initPanel(){
    $('#pp').portal({
        onStateChange:function(){
        }
    });
    var url='/notifyStat/getData?showInIndex=true&status=ENABLE&page=-1';
    doAjax(null,url,'GET',false,function(data){
        var panels=new Array();
        for(var i=0; i<data.rows.length; i++){
            var pdata =data.rows[i];
            var title ='';
            var color ="black";
            if (pdata.overAlertValue>0) {
                title='<img src="../../static/image/alert.gif"></img>';
                color="red";
            } else if (pdata.overWarningValue>0) {
                title='<img src="../../static/image/warn.png"></img>';
                color="BlueViolet";
            } else {
                title='<img src="../../static/image/tick.png"></img>';
                color="#2E8B57";
            }
            title+=pdata.userNotify.title+'(W:'+pdata.userNotify.warningValue+'>>A:'+pdata.userNotify.alertValue+')';
            var content ='<div align="center" class="wrap"><font size="6" color="'+color+'">'+pdata.compareValue+'</font>&nbsp;&nbsp;'+pdata.userNotify.notifyConfig.valueTypeName+'</div>';
            if(pdata.userNotify.notifyConfig.resultType=='NAME_DATE'||pdata.userNotify.notifyConfig.resultType=='NAME_NUMBER'){
                content ='<div align="center" class="wrap"><font size="6" color="'+color+'">'+pdata.name+'</font>&nbsp;&nbsp;'+pdata.compareValue+'&nbsp;&nbsp;'+pdata.userNotify.notifyConfig.valueTypeName+'</div>';
            }
            var enhContent = '<a href="#" onclick="addTab(\''+pdata.userNotify.notifyConfig.tabName+'\',\'..'+pdata.userNotify.notifyConfig.url+'\')">'+content+'</a>';
            var panel = {
                id: pdata.id,
                title: title,
                height:230,
                collapsible:true,
                content:enhContent
            };
            panels.push(panel);
        }
        addPanelsToContent(panels);
        $('#pp').portal('resize');
    });
}

function addPanelsToContent(panels){
    for(var i=0; i<panels.length; i++){
        var panel = panels[i];
        var p = $('<div/>').attr('id',panel.id).appendTo('body');
        p.panel(panel);
        //columnIndex为0，1，2,对于0，2需要调换位置
        var index = panel.id%4;
        if(index==0){
            index=3;
        }else if(index==3){
            index=0;
        }
        $('#pp').portal('add',{
            panel:p,
            columnIndex:index
        });
    }
}

function logout(){
    $.messager.confirm('提示信息', '您确认要退出系统吗?', function(data) {
        if (data) {
            var vurl ='/main/logout';
            doAjax(null,vurl,'POST',false,function(data){
                window.location.href='login.html';
            });
        }
    });
}

function showUserCalendar() {
    addTab('我的日历','../userCalendar/userCalendar.html');
}
/**
 * 今日行程
 */
function todayCalendar(){
    var url='/userCalendar/todayCalendarList';
    doAjax(null,url,'GET',false,function(data){
        $('#todayCalendar-window').window('open');
        $('#todayCalendarTg').datagrid({
            //url: '../userCalendar/todayCalendarList',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'id',title:'序号',width:50,align:'center'},
                {field:'title',title:'标题',width:150,align:'center'},
                {field:'expireTime',title:'失效时间',width:140,align:'center'},
                {field:'delayCounts',title:'延迟次数',width:60,align:'center'},
                {field:'sourceTypeName',title:'来源',width:60,align:'center'},
                {field:'createdTime',title:'创建时间',width:140,align:'center'},
                {field:'ops',title:'操作',width:50,align:'center'}
            ]]
        });
        //清空所有数据
        $('#todayCalendarTg').datagrid('loadData', { total: 0, rows: [] });
        for (var i = 0; i < data.length; i++) {
            var title =data[i].title;
            if(data[i].delayCounts>2){
                title='<font color="red">'+data[i].title+'</font>';
            }
            title='<a href="#" onclick="showUserCalendarDetail('+data[i].id+')">'+title+'</a>'
            var row = {
                id:i+1,
                title:title,
                expireTime:data[i].expireTime,
                delayCounts:data[i].delayCounts,
                sourceTypeName:data[i].sourceTypeName,
                createdTime:data[i].createdTime,
                ops:'<a href="#" onclick="finishUserCalendar('+data[i].id+')">完成</a>'
            };
            $('#todayCalendarTg').datagrid('appendRow',row);
        }
    });
}

function showUserCalendarDetail(id) {
    var url='/userCalendar/get?id='+id;
    doAjax(null,url,'GET',false,function(data) {
        $.messager.alert('日历内容', data.content, 'info');
        $('#todayCalendarTg').datagrid('clearSelections');
    });
}

function finishUserCalendar(id) {
    var url='/userCalendar/finish';
    var para ={
        id:id
    };
    doAjax(para,url,'POST',false,function() {
        todayCalendar();
    });
}

function showUserScore() {
    $('#userScore-window').window('open');
    userScoreStat();
}
/**
 * 评分
 */
function userScoreStat(){
    var url='/user/getScore';
    var para =form2Json("userScore-search-form");
    document.getElementById("totalScore").innerText="";
    doAjax(para,url,'GET',false,function(data){
        $('#userScoreTb').datagrid({
            //url: '../userCalendar/todayCalendarList',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'id',title:'序号',width:50,align:'center'},
                {field:'name',title:'名称',width:150,align:'center'},
                {field:'score',title:'得分',width:80,align:'center'},
                {field:'maxScore',title:'单项满分',width:80,align:'center'},
                {field:'statValue',title:'统计值',width:80,align:'center'},
                {field:'limitValue',title:'参考值',width:80,align:'center'},
                {field:'remark',title:'说明',width:40,align:'center'}
            ]]
        });
        //清空所有数据
        $('#userScoreTb').datagrid('loadData', { total: 0, rows: [] });
        var totalScore=0;
        var totalMaxScore=0;
        for (var i = 0; i < data.length; i++) {
            var img = '';
            if(data[i].compareType=='MORE'){
                img='<img src="../../static/image/arrow_up_red.png"></img>';
            }else{
                img='<img src="../../static/image/arrow_down_blue.png"></img>';
            }
            var mm='<a href="javascript:showScoreRemark('+'\''+data[i].remark+'\''+');"><img src="../../static/image/info.png"></img></a>';;
            var row = {
                id:i+1,
                name:data[i].name,
                score:data[i].score,
                maxScore:data[i].maxScore,
                statValue:data[i].statValue,
                limitValue:data[i].limitValue,
                remark:mm
            };
            $('#userScoreTb').datagrid('appendRow',row);
            totalScore+=data[i].score;
            totalMaxScore+=data[i].maxScore;
        }
        var row = {
            id:'',
            name:'<font color="red">'+'总分'+'</font>',
            score:'<font color="red">'+totalScore+'</font>',
            maxScore:'<font color="red">'+totalMaxScore+'</font>',
            statValue:'',
            limitValue:''
        };
        $('#userScoreTb').datagrid('appendRow',row);
        //显示总分
        document.getElementById("totalScore").innerText="总分："+totalScore;
    });
}

function showScoreRemark(msg){
    $('#userScoreTb').datagrid('uncheckAll');
    $.messager.alert('详情', msg, 'info');
}

function loadUserInfo() {
    var url='/main/myInfo';
    doAjax2(null,url,'GET',false,false,function(data) {
        document.getElementById("version").innerText=data.version;
        document.getElementById("username").innerText=data.username;
        // var todayCalendars = data.todayCalendars;
        // if(todayCalendars>0){
        //     document.getElementById("todayCalendars").innerText='('+data.todayCalendars+')';
        // }
    });
}

function dailyRewardPointsStat() {
    var url='/userRewardPointRecord/dailyStat';
    doAjax2(null,url,'GET',false,false,function(data) {
        var ss='';
        if(data){
            if(data.totalPoints>=0){
                ss='<a href="javascript:showDailyRewardPointsStat()"><font color="green">(积分:+'+data.totalPoints+')</font></a>';
            }else{
                ss='<a href="javascript:showDailyRewardPointsStat()"><font color="red">(积分:'+data.totalPoints+')</font></a>';
            }
        }else{
            ss='<a href="javascript:showDailyRewardPointsStat()"><font color="green">(积分:+0)</font></a>';
        }
        document.getElementById("dailyRewardPointsStat").innerHTML=ss;
    });
}
var currentDays=0;

function showDailyRewardPointsStat() {
    $('#dailyPointsStat-window').window('open');
    currentDays=0;
    doDailyRewardPointsStat(0);
}

function doDailyRewardPointsStat(n) {
    currentDays=currentDays+n;
    var date = getDay(currentDays);
    $('#dailyPointsStatTb').datagrid({
        url: '/userRewardPointRecord/aa',
        showGroup: true,
        scrollbarSize: 0,
        columns:[[
            {field:'name',title:'项目',width:150,align:'center'},
            {field:'value',title:'值',width:100,align:'center'},
            {field:'unit',title:'单位',width:100,align:'center'}
        ]]
    });
    //清空所有数据
    $('#dailyPointsStatTb').datagrid('loadData', { total: 0, rows: [] });
    var url='/userRewardPointRecord/dailyStat?date='+date;
    doAjax(null,url,'GET',false,function(data) {
        if(data){
            $('#dailyPointsStatTb').datagrid('appendRow',{
                name:'统计日期',
                value:data.dateStr,
                unit:'天'
            });
            $('#dailyPointsStatTb').datagrid('appendRow',{
                name:'得分次数',
                value:data.totalCount,
                unit:'次'
            });
            if(data.totalPoints>=0){
                $('#dailyPointsStatTb').datagrid('appendRow',{
                    name:'当日积分',
                    value:'<font color="green">+'+data.totalPoints+'</font>',
                    unit:'分'
                });
            }else{
                $('#dailyPointsStatTb').datagrid('appendRow',{
                    name:'当日积分',
                    value:'<font color="red">'+data.totalPoints+'</font>',
                    unit:'分'
                });
            }
            $('#dailyPointsStatTb').datagrid('appendRow',{
                name:'累计积分',
                value:data.currentPoints,
                unit:'分'
            });
        }
    });
}

function showDailyRewardPointsList() {
    closeWindow('dailyPointsStat-window');
    addTab('用户积分记录','../user/userRewardPointRecordList.html');
}

function showCalendar() {
    closeWindow('todayCalendar-window');
    addTab('用户日历视图','../userCalendar/userCalendar.html');
}
function editMyInfo() {
    var url='/user/getMyInfo';
    doAjax(null,url,'GET',false,function(data){
        $('#myInfo-window').window('open');
        $('#myInfo-form').form('clear');
        $('#myInfo-form').form('load', data);
        $("#password").textbox('setValue',null);
    });
}

function saveMyInfo() {
    var url='/user/editMyInfo';
    doFormSubmit('myInfo-form',url,function(){
        closeWindow('myInfo-window');
    });
}

function generalStat(){
    var para =form2Json("general-stat-search-form");
    var url='/main/generalStat';

    doAjax(para,url,'GET',false,function(data){
        $('#stat-form').form('clear');
        var vbuyBudgetRate='--';
        if(data.monthBudget>0){
            vmonthConsumeBudgetRate = getPercent(data.monthConsumeAmount,data.monthBudget);
        }
        //月度剩余
        var vremainMoney = data.totalIncome- data.totalConsumeAmount;

        var formData = {
            monthBudget: data.monthBudget,
            yearBudget: data.yearBudget,
            totalIncome: data.totalIncome,
            totalConsumeAmount: data.totalConsumeAmount,
            totalBuyCount: data.totalBuyCount,
            totalTreatAmount: data.totalTreatAmount,
            totalTreatCount: data.totalTreatCount,
            totalSportExerciseHours: data.totalSportExerciseHours,
            totalSportExerciseCount: data.totalSportExerciseCount,
            totalReadingHours: data.totalReadingHours,
            totalReadingCount: data.totalReadingCount,
			totalMusicPracticeHours:data.totalMusicPracticeHours,
			totalMusicPracticeCount:data.totalMusicPracticeCount,
            monthConsumeBudgetRate:vmonthConsumeBudgetRate,
            remainMoney:vremainMoney,
            dayMonthRate:data.dayMonthRate,
            remainMonthDays:data.remainMonthDays,
            monthConsumeAmount:data.monthConsumeAmount,
            monthDays:data.monthDays,
            monthPassDays:data.monthPassDays
        };
        $('#stat-form').form('load', formData);
        //生成饼图
        generalStatPie();
    });
}

function generalStatPie(){
    var para =form2Json("general-stat-search-form");
    var url='/buyRecord/statWithTreat';

    doAjax(para,url,'GET',false,function(data){
        //生成饼图
        createPieData(data);
    });
}

function quickGeneralStat(type) {
    var formData;
    if(type==1){
        // 查询条件这个星期的
        var nowDate = new Date();
        formData = {
            startDate: getFirstDayOfWeek(nowDate),
            endDate: getLastDayOfWeek(nowDate)
        };
    }else if(type==2){
        // 查询条件这个月的
        var nowDate = new Date();
        formData = {
            startDate: getFirstDayOfMonth(nowDate),
            endDate: getLastDayOfMonth(nowDate)
        };
    }else if(type==3){
        // 查询条件今年的
        formData = {
            startDate: getYear(0)+'-01-01',
            endDate: getYear(0)+'-12-31'
        };
    }
    $('#general-stat-search-form').form('load', formData);
    generalStat();
}


function showGeneralStat() {
    $('#general-stat-window').window('open');
    quickGeneralStat(2);
}

function openStatDetail(tabName,url) {
    $('#general-stat-window').window('close');
    addTab(tabName,url);
}

function todayUserBehaviorStat() {
    // 查询条件今年的
    var para = {
        date: getDay(0),
        dateGroupType: 'DAY'
    };
    var vurl = '/userBehavior/stat';
    doAjax(para, vurl, 'GET',false, function(data) {
        createPolarBarChartEnhanced(data,"container");
    });
}

/**
 * 月度预算详情
 */
function monthBudgetDetail(){
    var url='/budget/getInfoList?period=MONTHLY';
    doAjax(null,url,'GET',false,function(data){
        $('#monthBudgetDetail-window').window('open');
        $('#monthBudgetDetailTg').datagrid({
            //url: '../userCalendar/todayCalendarList',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'id',title:'序号',width:50,align:'center'},
                {field:'name',title:'预算名称',width:140,align:'center'},
                {field:'amount',title:'预算金额',width:80,align:'center'},
                {field:'typeName',title:'类型',width:60,align:'center'},
                {field:'periodName',title:'周期类型',width:60,align:'center'},
                {field:'occurDate',title:'完成时间',width:140,align:'center'},
                {field:'consumeAmount',title:'实际花费金额',width:110,align:'center'}
            ]]
        });
        //清空所有数据
        $('#monthBudgetDetailTg').datagrid('loadData', { total: 0, rows: [] });
        var unComMonthBudget=0;
        var totalAmount =0;
        var totalConsumeAmount =0;
        for (var i = 0; i < data.length; i++) {
            var od='--';
            var consumeAmount='--';
            var name =data[i].name;
            totalAmount+=data[i].amount;
            if(data[i].occurDate){
                od=data[i].occurDate;
                consumeAmount=data[i].ncAmount+data[i].bcAmount+data[i].trAmount;
                //实际花费和预算差值
                var ca = consumeAmount - data[i].amount;
                var csStr=consumeAmount;
                if(ca>0){
                    csStr=formatMoneyWithSymbal(consumeAmount)+'<font color="red">(+'+ca.toFixed(2)+')</font>';
                }else if(ca<0){
                    csStr=formatMoneyWithSymbal(consumeAmount)+'<font color="green">(-'+(0-ca).toFixed(2)+')</font>';
                }else{
                    csStr=formatMoneyWithSymbal(consumeAmount);
                }
                totalConsumeAmount+=consumeAmount;
                name='★'+name;
                consumeAmount=csStr;
            }else{
                if(data[i].bindFlow){
                    name='★<font color="red">'+data[i].name+'</font>';
                    var ld = data[i].leftDays;
                    if(ld){
                        var lds = formatDays(ld);
                        od = '<font color="red">未完成(剩余：'+lds+')</font>';
                    }else{
                        od = '<font color="red">未完成</font>';
                    }
                    unComMonthBudget +=data[i].amount;
                }else{
                    od = '无需完成';
                }
            }
            var row = {
                id:i+1,
                name:name,
                amount:formatMoneyWithSymbal(data[i].amount),
                typeName:data[i].typeName,
                periodName:data[i].periodName,
                occurDate:od,
                consumeAmount:consumeAmount
            };
            $('#monthBudgetDetailTg').datagrid('appendRow',row);
        }
        var row = {
            id:'--',
            name:'小计',
            amount:formatMoneyWithSymbal(totalAmount),
            typeName:'--',
            periodName:'--',
            occurDate:'--',
            consumeAmount:formatMoneyWithSymbal(totalConsumeAmount)
        };
        $('#monthBudgetDetailTg').datagrid('appendRow',row);
        var para =form2Json("stat-form");
        var mca = para.monthConsumeAmount;
        //剩余可消费金额
        var rmm = para.monthBudget-mca-unComMonthBudget;
        //已经消费+未完成的预算
        var ac = parseInt(unComMonthBudget,10)+parseInt(mca,10);
        var formData = {
            monthBudget:para.monthBudget,
            monthConsumeAmount:para.monthConsumeAmount,
            dayMonthRate:para.dayMonthRate,
            monthConsumeBudgetRate:para.monthConsumeBudgetRate,
            unComMonthBudget:unComMonthBudget,
            remainMonthMoney:rmm,
            remainMonthMoneyPerDay:rmm/para.remainMonthDays,
            actualConsume:ac
        };
        $('#monthBudgetDetail-form').form('load', formData);
        //预期月消费总额
        var aa = para.monthConsumeAmount*para.monthDays/para.monthPassDays;
        var sss='';
        if(aa>para.monthBudget){
            sss+='<font color="red">本月预期总消费金额:'+formatMoneyWithSymbal(aa)+'元,超出预算值'+formatMoneyWithSymbal(aa-para.monthBudget)+'元</font>';
        }else{
            sss+='<font color="green">本月预期总消费金额:'+formatMoneyWithSymbal(aa)+'元,低于预算值'+formatMoneyWithSymbal(para.monthBudget-aa)+'元</font>';
        }
        document.getElementById("achieveMonthInfo").innerHTML=sss;
    });
}