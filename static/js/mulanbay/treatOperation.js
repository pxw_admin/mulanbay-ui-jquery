$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/treatOperation/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2Json("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
        columns : [ [ {
            field : 'id',
            title : '记录号',
            sortable : true,
            align : 'center'
        }, {
            field : 'treatRecord.hospital',
            title : '医院',
            formatter : function(value, row, index) {
                return row.treatRecord.hospital;
            }
        }, {
            field : 'treatRecord.disease',
            title : '疾病症状',
            formatter : function(value, row, index) {
                return row.treatRecord.disease;
            }
        }, {
            field : 'treatRecord.organ',
            title : '器官',
            formatter : function(value, row, index) {
                return row.treatRecord.organ;
            }
        }, {
            field : 'treatRecord.diagnosedDisease',
            title : '确诊疾病',
            formatter : function(value, row, index) {
                return row.treatRecord.diagnosedDisease;
            }
        }, {
            field : 'name',
            title : '手术/检查项目'
        }, {
            field : 'fee',
            title : '费用',
            align : 'center',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'treatDate',
            title : '手术/检查日期'
        }, {
            field : 'aaa',
            title : '距离现在',
            formatter : function(value, row, index) {
                var days = tillNowDays('',row.treatDate);
                days =Math.abs(days);
                var ss = formatDays(days);
                if(days>=365){
                    return '<font color="red">'+ss+'</font>';
                }else if(days>=90){
                    return '<font color="purple">'+ss+'</font>';
                }else if(days>=30){
                    return '<font color="green">'+ss+'</font>';
                }else {
                    return ss;
                }
            }
        }, {
            field : 'reviewDate',
            title : '复查日期',
            formatter : function(value, row, index) {
                if(value){
                    var days = tillNowDays('',value);
                    var ld ='';
                    if(days>0){
                        ld='('+formatDays(days)+')';
                    }
                    var dv = value+ld;
                    if(days>=365){
                        return dv;
                    }else if(days>=90){
                        return '<font color="purple">'+dv+'</font>';
                    }else if(days>=30){
                        return '<font color="#8b008b">'+dv+'</font>';
                    }else {
                        return '<font color="red">'+dv+'</font>';
                    }
                }else{
                    return '--';
                }
            },
            align : 'center'
        }, {
            field : 'available',
            title : '是否有效',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        }, {
            field : 'isSick',
            title : '是否有病',
            align : 'center',
            formatter : function(value, row, index) {
                if (value == true) {
                    return '<img src="../../static/image/warn.png"></img>';
                } else {
                    return '--';
                }
            }
        }, {
            field : 'category',
            title : '分类',
            align : 'center'
        }, {
            field : 'aa',
            title : '检验报告',
            formatter : function(value, row, index) {
                return '<a href="javascript:showTreatTest('+row.id+');"><img src="../../static/image/sum.png"></img></a>';
            },
            align : 'center'
        }] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
            id : 'statBtn',
            text : '统计',
            iconCls : 'icon-stat',
            handler : showStat
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
    $.messager.alert('提示', '目前该功能不支持，请到看病记录管理中操作!', 'error');
    return;
    $('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
}

function initForm(){
    combotreeLoad('operationNameCategoryList','/treatOperation/getTreatOperationCategoryTree?groupField=name');
}

function loadLastOperationName(name) {
    var url='/treatOperation/getLastTreatOperation';
    var para ={
        name:name
    }
    doAjax(para,url,'GET',false,function(data){
        var formData = {
            category: data.category
        };
        $('#ff').form('load', formData);
    });
}

function loadSearchForm(){
    combotreeLoad('tagsList','/treatRecord/getTagsTree?needRoot=true');
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/treatOperation/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
        data.treatRecordId=data.treatRecord.id;
		$('#ff').form('load', data);
        //$("#treatRecordId").val(data.treatRecord.id);
        //设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/treatOperation/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/treatOperation/create';
    }
    $('#operationNameCategoryList').combobox('setValue',$('#operationNameCategoryList').combobox('getText') );

    doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		showAll();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/treatOperation/delete';
    commonDeleteByIds(delUrlPrefix);
}

function showStat() {
    $('#stat-window').window('open');
    stat();
}
function stat(){
    var para =form2Json("stat-search-form");
    var url='/treatOperation/stat';

    doAjax(para,url,'GET',false,function(data){
        $('#pg').datagrid({
            url: '/treatOperation/aa',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'name',title:'手术名',width:450,align:'center'},
                {field:'totalCount',title:'次数',width:60,align:'center'},
                {field:'totalFee',title:'花费（元）',width:100,align:'center'}
            ]]
        });
        //清空所有数据
        $('#pg').datagrid('loadData', { total: 0, rows: [] });
        for (var i = 0; i < data.length; i++) {
        	var name =data[i].name;
        	if(name==null){
        	    name="未知";
            }
        	if(i==data.length-1){
        		name='<font color="red">'+data[i].name+'</font>';
			}
            var row = {
                name:name,
                totalCount:data[i].totalCount,
                totalFee:data[i].totalFee
            };
            $('#pg').datagrid('appendRow',row);
        }
    });
}

function showTreatTest(treatOperationId) {
    $('#treatTest-window').window('open');
    $("#treatTestTreatOperationSearchId").val(treatOperationId);
    $('#grid').datagrid('clearSelections');
    initGridTreatTest();
}