$(function() {
    loadSearchForm();
	initGrid();
    // $('#quitDate').datebox({
    //     onChange: calculateDays
    // });
});

var dataUrl='/role/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称'
        },{
			field : 'status',
			title : '状态',
			formatter : function(value, row, index) {
				return getStatusImage(value);
			},
			align : 'center'
		} ,{
			field : 'orderIndex',
			title : '排序号',
			align : 'center'
		},{
			field : 'aa',
			title : '授权',
			formatter : function(value, row, index) {
				return '<a href="javascript:openAuth('+row.id+');"><img src="../../static/image/auth.png"></img></a>';;
			},
			align : 'center'
		} ,{
			field : 'createdTime',
			title : '创建时间',
			align : 'center'
		} ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		}, '-', {
			id : 'searchBtn',
			text : '刷新系统缓存',
			iconCls : 'icon-refresh2',
			handler : refreshSystemConfig
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
}

function initForm(){
}
function loadSearchForm() {

}
function edit() {
	var rows = getSelectedSingleRow();
	var url='/role/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    //自动设置，原来在增加与修改时设置经常出问题
    var url='/role/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/role/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/role/delete';
    commonDeleteByIds(delUrlPrefix);
}

function refreshSystemConfig() {
	var url='/role/refreshSystemConfig';
	doAjax(null,url,'POST',true,function(data){
	});
}

function openAuth(roleId) {
	$('#grid').datagrid('clearSelections');
	$('#eidt-roleFunction-window').window('open');
	$("#functionRoleId").val(roleId);
	$('#roleFunctionList').tree({
		url : getFullApiUrl('/role/getRoleFunctionTree?needRoot=true&roleId='+roleId),
		valueField : 'id',
		textField : 'text',
		loadFilter: function(data){
			return loadDataFilter(data);
		}
	});
}

function saveRoleFunction() {
	var roleId =  $("#functionRoleId").val();
	var nodes = $('#roleFunctionList').tree('getChecked');
	var dd=new Array();
	for(var i=0; i<nodes.length; i++){
		var id = nodes[i].id;
		if (id!=undefined&&id!=null&&id!= ''){
			dd.push(id);
		}
	}
	var postData ={
		roleId:roleId,
		functionIds:dd.join(',')
	};
	var url='/role/saveRoleFunction';
	doAjax(postData,url,'POST',true,function (data) {
		closeWindow('eidt-roleFunction-window');
	});
}