/**
 * Created by fenghong on 2017/2/7.
 */
var colorList = ['#284554', '#61a0a8', '#d48265', '#91c7ae','#749f83',
    '#ca8622', '#bda29a','#6e7074', '#546570','#c23531',  '#8B0A50',
    '#080808','#800000','#006400','#191970'];
function createChart(data) {
    createChartEnhanced(data,'container');
}
function createChartEnhanced(data,containId) {
    var myChart = echarts.init(document.getElementById(containId));

    // 指定图表的配置项和数据
    var option = data;

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option,true);
}

function createBarData(data){
    createBarDataEnhanced(data,'container');
}
//柱状图
function createBarDataEnhanced(data,containId){
    var series =new Array();
    for(var i=0;i<data.ydata.length;i++){
        var serie = {
            name:data.ydata[i].name,
            type:'bar',
            stack: data.ydata[i].stack,
            data:data.ydata[i].data,
            itemStyle: {
                normal: {
                    label: {
                        show: true,
                        position: 'top',
                        formatter: '{c}'
                    }
                }
            },
            markPoint : {
                data : [
                    {type : 'max', name: '最大值'},
                    {type : 'min', name: '最小值'}
                ]
            },
            markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }
        };
        series.push(serie);
    }
    var option = {
        title : {
            text: data.title,
            subtext: data.subTitle
        },
        tooltip : {
            trigger: 'axis'
        },
        label:{
            normal:{
                show: true,
                position: 'top'}
        },
        legend: {
            data:data.legendData
        },
        toolbox: {
            show : true,
            feature : {
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        color: colorList,
        calculable : true,
        xAxis : [
            {
                type : 'category',
                data : data.xdata
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : series
    };
    createChartEnhanced(option,containId);
}

function createLineData(data){
    createLineDataEnhanced(data,'container');
}

//折线图
function createLineDataEnhanced(data,containId){
    var series =new Array();
    for(var i=0;i<data.ydata.length;i++){
        var serie = {
            name:data.ydata[i].name,
            type:'line',
            dataView : {show: true, readOnly: false},
            itemStyle : { normal: {label : {show: true}}},
            data:data.ydata[i].data,
            markPoint: {
                data: [
                    {type: 'max', name: '最大值'},
                    {type: 'min', name: '最小值'}
                ]
            },
            markLine: {
                data: [
                    {type: 'average', name: '平均值'},
                    [{
                        symbol: 'none',
                        x: '90%',
                        yAxis: 'max'
                    }, {
                        symbol: 'circle',
                        label: {
                            normal: {
                                position: 'start',
                                formatter: '最大值'
                            }
                        },
                        type: 'max',
                        name: '最高点'
                    }]
                ]
            }
        };
        series.push(serie);
    }
    var option = {
        title: {
            text: data.title,
            subtext: data.subTitle
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data:data.legendData
        },
        toolbox: {
            show: true,
            feature: {
                dataZoom: {
                    yAxisIndex: 'none'
                },
                dataView : {show: true, readOnly: false},
                magicType: {type: ['line', 'bar']},
                restore: {},
                saveAsImage: {}
            }
        },
        color: colorList,
        xAxis:  {
            type: 'category',
            boundaryGap: false,
            data: data.xdata
        },
        yAxis: {
            type: 'value',
            axisLabel: {
                formatter: '{value}'
            }
        },
        series: series
    };
    createChartEnhanced(option,containId);
}

function createPieData(data){
    createPieDataEnhanced(data,'container');
}

// 饼图
function createPieDataEnhanced(data,containId){
    var series =new Array();
    for(var i=0;i<data.detailData.length;i++){
        var serie = {
            name: data.detailData[i].name,
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:data.detailData[i].data,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                },
                normal:{
                    label:{
                        show: true,
                        formatter: '{b} : {c} ({d}%)'
                    },
                    labelLine :{show:true}
                }
            }
        };
        series.push(serie);
    }
    var option = {
        title : {
            text: data.title,
            subtext: data.subTitle,
            x:'center'
        },
        color: colorList,
        toolbox: {
            show: false,
            feature: {
                dataZoom: {
                    yAxisIndex: 'none'
                },
                dataView : {show: true, readOnly: false},
                magicType: {type: ['line', 'bar']},
                restore: {},
                saveAsImage: {}
            }
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: data.xdata
        },
        series : series
    };
    createChartEnhanced(option,containId);
}

function createDoublePieData(data){
    createDoublePieDataEnhanced(data,'container');
}


// 双饼图
function createDoublePieDataEnhanced(data,containId){
    var option = {
        title : {
            text: data.title,
            subtext: data.subTitle,
            x:'center'
        },
        color: colorList,
        toolbox: {
            show: false,
            feature: {
                dataZoom: {
                    yAxisIndex: 'none'
                },
                dataView : {show: true, readOnly: false},
                magicType: {type: ['line', 'bar']},
                restore: {},
                saveAsImage: {}
            }
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: data.xdata
        },
        series: [
            {
                name:data.detailData[0].name,
                type:'pie',
                selectedMode: 'single',
                radius: [0, '30%'],

                label: {
                    normal: {
                        position: 'inner'
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                data:data.detailData[0].data,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    },
                    normal:{
                        label:{
                            show: true,
                            formatter: '{b} : {c} ({d}%)'
                        },
                        labelLine :{show:true}
                    }
                }
            },
            {
                name:data.detailData[1].name,
                type:'pie',
                radius: ['40%', '55%'],
                data:data.detailData[1].data,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    },
                    normal:{
                        label:{
                            show: true,
                            formatter: '{b} : {c} ({d}%)'
                        },
                        labelLine :{show:true}
                    }
                }
            }
        ]
    };
    createChartEnhanced(option,containId);
}

function createCalanderData(data){
    createCalanderDataEnhanced(data,'container');
}

// 日历图
function createCalanderDataEnhanced(data,containId){

    var graphData = data.graphData;
    var links={};
    if(graphData){
        links = graphData.map(function (item, idx) {
            return {
                source: idx,
                target: idx + 1
            };
        });
        links.pop();
    }

    var minValue = data.minValue;
    var maxValue = data.maxValue;
    //最大值最小值分成3分
    var peace = (maxValue-minValue)/5;


    // 获取每个格子的点的大小，数值为2,4,6,8,10
    function getSymbolSize(value) {
        if(1==1){
            //算法不好，强行写死
            return 10;
        }
        if(peace==0){
            //最小和最大值相等
            return 10;
        }
        var v = ((value[1]-minValue)/peace)*2;
        if(v==0){
            return 2;
        }
        return v;
    }

    var seriesData =new Array();
    var serie={
        type: 'graph',
        edgeSymbol: ['none', 'arrow'],
        coordinateSystem: 'calendar',
        links: links,
        symbolSize: 15,
        calendarIndex: 0,
        itemStyle: {
            normal: {
                color: 'green',
                shadowBlue: 9,
                shadowOffsetX: 1.5,
                shadowOffsetY: 3,
                shadowColor: '#555'
            }
        },
        lineStyle: {
            normal: {
                color: '#D10E00',
                width: 1,
                opacity: 1
            }
        },
        data: data.graphData,
        z: 20
    };
    seriesData.push(serie);
    serie={
        type: 'graph',
        edgeSymbol: ['none', 'arrow'],
        coordinateSystem: 'calendar',
        links: links,
        symbolSize: 15,
        calendarIndex: 1,
        itemStyle: {
            normal: {
                color: 'green',
                shadowBlue: 9,
                shadowOffsetX: 1.5,
                shadowOffsetY: 3,
                shadowColor: '#555'
            }
        },
        lineStyle: {
            normal: {
                color: '#D10E00',
                width: 1,
                opacity: 1
            }
        },
        data: data.graphData,
        z: 20
    };
    seriesData.push(serie);
    serie={
        name: data.legendData[0],
        type: 'scatter',
        coordinateSystem: 'calendar',
        data: data.series,
        symbolSize: function (val) {
            //return val[1] / data.compareSizeValue;
            return getSymbolSize(val);
        },
        itemStyle: {
            normal: {
                color: 'red'
            }
        }
    };
    seriesData.push(serie);
    serie={
        name: data.legendData[0],
        type: 'scatter',
        coordinateSystem: 'calendar',
        calendarIndex: 1,
        data: data.series,
        symbolSize: function (val) {
            //return val[1] / data.compareSizeValue;
            return getSymbolSize(val);
        },
        itemStyle: {
            normal: {
                color: 'red'
            }
        }
    };
    seriesData.push(serie);
    if(data.top&&data.top>0){
        serie={
            name: data.legendData[1],
            type: 'effectScatter',
            coordinateSystem: 'calendar',
            calendarIndex: 1,
            data: data.series.sort(function (a, b) {
                return b[1] - a[1];
            }).slice(0, data.top),
            symbolSize: function (val) {
                //return val[1] / data.compareSizeValue;
                return getSymbolSize(val);
            },
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true,
            itemStyle: {
                normal: {
                    color: '#f4e925',
                    shadowBlur: 10,
                    shadowColor: '#333'
                }
            },
            zlevel: 1
        };
        seriesData.push(serie);
        serie={
            name: data.legendData[1],
            type: 'effectScatter',
            coordinateSystem: 'calendar',
            data: data.series.sort(function (a, b) {
                return b[1] - a[1];
            }).slice(0, data.top),
            symbolSize: function (val) {
                //return val[1] / data.compareSizeValue;
                return getSymbolSize(val);
            },
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true,
            itemStyle: {
                normal: {
                    color: '#f4e925',
                    shadowBlur: 10,
                    shadowColor: '#333'
                }
            },
            zlevel: 1
        };
        seriesData.push(serie);
    }


    var option = {
        backgroundColor: '#404a59',

        title: {
            top: 10,
            text: data.title,
            subtext: data.subTitle,
            left: 'center',
            textStyle: {
                color: '#fff'
            }
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a}<br/>{b}({c}"+data.unit+")"
        },
        legend: {
            top: '30',
            left: '100',
            data: data.legendData,
            textStyle: {
                color: '#fff'
            }
        },
        calendar: [{
            top: 80,
            left: 'center',
            range: data.rangeFirst,
            splitLine: {
                show: true,
                lineStyle: {
                    color: '#000',
                    width: 4,
                    type: 'solid'
                }
            },
            yearLabel: {
                formatter: '{start}  上半年',
                textStyle: {
                    color: '#fff'
                }
            },
            itemStyle: {
                normal: {
                    color: '#323c48',
                    borderWidth: 1,
                    borderColor: '#111'
                }
            }
        }, {
            top: 260,
            left: 'center',
            range: data.rangeSecond,
            splitLine: {
                show: true,
                lineStyle: {
                    color: '#000',
                    width: 4,
                    type: 'solid'
                }
            },
            yearLabel: {
                formatter: '{start}  下半年',
                textStyle: {
                    color: '#fff'
                }
            },
            itemStyle: {
                normal: {
                    color: '#323c48',
                    borderWidth: 1,
                    borderColor: '#111'
                }
            }
        }],
        series : seriesData
    };
    createChartEnhanced(option,containId);
}

function createCalanderPieData(data){
    createCalanderPieDataEnhanced(data,'container');
}

function createCalanderPieDataEnhanced(data,containerId) {
    var cellSize = [65, 65];
    var pieRadius = 25;
    var app = {};
    var dom = document.getElementById(containerId);
    var myChart = echarts.init(dom);
    var seriesData = data.seriesData;
    var scatterData = data.scatterData;

    function getPieSeries(scatterData, chart) {
        return echarts.util.map(scatterData, function (item, index) {
            var center = chart.convertToPixel('calendar', item);
            return {
                id: index + 'pie',
                type: 'pie',
                center: center,
                label: {
                    normal: {
                        formatter: '{c}',
                        position: 'inside'
                    }
                },
                radius: pieRadius,
                data: seriesData[item[0]]
            };
        });
    }

    function getPieSeriesUpdate(scatterData, chart) {
        return echarts.util.map(scatterData, function (item, index) {
            var center = chart.convertToPixel('calendar', item);
            return {
                id: index + 'pie',
                center: center
            };
        });
    }

    var option = {
        //backgroundColor: '#404a59',

        title: {
            show : true,
            top: 0,
            text: data.title,
            subtext: data.subTitle,
            left: 'left'
        },
        tooltip : {},
        legend: {
            data: data.legendData,
            bottom: 10
        },
        calendar: {
            top: 40,
            left: 'center',
            orient: 'vertical',
            cellSize: cellSize,
            yearLabel: {
                show: false,
                textStyle: {
                    fontSize: 30
                }
            },
            dayLabel: {
                margin: 20,
                firstDay: 1,
                nameMap: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
            },
            monthLabel: {
                show: false
            },
            range: data.range
        },
        series: [{
            id: 'label',
            type: 'scatter',
            coordinateSystem: 'calendar',
            symbolSize: 1,
            label: {
                normal: {
                    show: true,
                    formatter: function (params) {
                        return echarts.format.formatTime('dd', params.value[0]);
                    },
                    offset: [-cellSize[0] / 2 + 10, -cellSize[1] / 2 + 10],
                    textStyle: {
                        color: '#000',
                        fontSize: 12
                    }
                }
            },
            data: data.scatterData
        }]
    };

    if (!app.inNode) {
        var pieInitialized;
        setTimeout(function () {
            pieInitialized = true;
            myChart.setOption({
                series: getPieSeries(scatterData, myChart)
            });
        }, 10);

        app.onresize = function () {
            if (pieInitialized) {
                myChart.setOption({
                    series: getPieSeriesUpdate(scatterData, myChart)
                });
            }
        };
    }
    myChart.setOption(option,true);
}

function createCalanderHeatMapData(data){
    createCalanderHeatMapDataEnhanced(data,'container');
}

// 日历热点图
function createCalanderHeatMapDataEnhanced(data,containId){
    var calendars = new Array();
    for(var i=0;i<data.years.length;i++){
        var c={
            top: 80+i*180,
            range: data.years[i],
            cellSize: ['auto', 20],
            right: 5
        };
        calendars.push(c);
    }
    var seriesData =new Array();
    for(var i=0;i<data.series.length;i++){
        var serie={
            type: 'heatmap',
            calendarIndex: i,
            coordinateSystem: 'calendar',
            data: data.series[i]
        };
        seriesData.push(serie);
    }

    var option = {
        title: {
            top: 0,
            left: 'center',
            text: data.title
        },
        tooltip: {
            position: 'top',
            formatter: function (p) {
                var format = echarts.format.formatTime('yyyy-MM-dd', p.data[0]);
                return format + ': ' + p.data[1]+data.unit;
            }
        },
        visualMap: {
            min: data.minValue,
            max: data.maxValue,
            calculable: true,
            orient: 'horizontal',
            left: 'center',
            top: 30
        },
        calendar: calendars,
        series: seriesData
    };

    createChartEnhanced(option,containId);
}

function createScatterData(data){
    createScatterDataEnhanced(data,'container');
}

// 日历热点图
function createScatterDataEnhanced(data,containId){
    var seriesData =new Array();
    // 获取每个格子的点的大小，数值为2,4,6,8,10
    function getSymbolSize(value) {
        if(value==1){
            return 10;
        }else if(value<5){
            return 20;
        }else if(value<10){
            return 30;
        }else {
            return 4;
        }
    };
    for(var i=0;i<data.seriesDatas.length;i++){
        var serie={
            name:data.seriesDatas[i].name,
            type:'scatter',
            data: data.seriesDatas[i].data,
            symbolSize: function (data) {
                return getSymbolSize(data[2]);
            },
            markArea: {
                silent: true,
                itemStyle: {
                    normal: {
                        color: 'transparent',
                        borderWidth: 1,
                        borderType: 'dashed'
                    }
                },
                data: [[{
                    name: data.seriesDatas[i].name+'分布区间',
                    xAxis: 'min',
                    yAxis: 'min'
                }, {
                    xAxis: 'max',
                    yAxis: 'max'
                }]]
            },
            markPoint : {
                data : [
                    {type : 'max', name: '最大值'},
                    {type : 'min', name: '最小值'}
                ]
            },
            markLine : {
                lineStyle: {
                    normal: {
                        type: 'solid'
                    }
                },
                data : [
                    {type : 'average', name: '平均值'},
                    { xAxis: data.seriesDatas[i].xAxisAverage }
                ]
            }
        };
        seriesData.push(serie);
    }

    var option = {
        title : {
            text: data.title,
            subtext: data.subTitle
        },
        grid: {
            left: '3%',
            right: '7%',
            bottom: '3%',
            containLabel: true
        },
        tooltip : {
            // trigger: 'axis',
            showDelay : 0,
            formatter : function (params) {
                if (params.value.length > 1) {
                    return '['+params.seriesName + ']<br/>'
                        + params.value[0] + data.xUnit + ' :'
                        + params.value[1] + data.yUnit + ',值：' + params.value[2] + data.vUnit;
                }
                else {
                    return params.seriesName + ' :<br/>'
                        + params.name + ' : '
                        + params.value + data.yUnit;
                }
            },
            axisPointer:{
                show: true,
                type : 'cross',
                lineStyle: {
                    type : 'dashed',
                    width : 1
                }
            }
        },
        toolbox: {
            feature: {
                dataZoom: {},
                brush: {
                    type: ['rect', 'polygon', 'clear']
                }
            }
        },
        brush: {
        },
        legend: {
            data: data.legendData,
            left: 'center'
        },
        xAxis : [
            {
                type : 'value',
                scale:true,
                axisLabel : {
                    formatter: '{value} '+data.xUnit
                },
                splitLine: {
                    show: false
                }
            }
        ],
        yAxis : [
            {
                type : 'value',
                scale:true,
                axisLabel : {
                    formatter: '{value} '+data.yUnit
                },
                splitLine: {
                    show: false
                }
            }
        ],
        series : seriesData
    };

    createChartEnhanced(option,containId);
}

function createCompareCalanderData(data){
    createCompareCalanderDataEnhanced(data,'container');
}

function createCompareCalanderDataEnhanced(data,containId){

    var graphData = data.graphData;
    var links={};
    if(graphData){
        links = graphData.map(function (item, idx) {
            return {
                source: idx,
                target: idx + 1
            };
        });
        links.pop();
    }

    var minValue = data.minValue;
    var maxValue = data.maxValue;
    //最大值最小值分成3分
    var peace = (maxValue-minValue)/5;

    // 获取每个格子的点的大小，数值为2,4,6,8,10
    function getSymbolSize(value) {
        if(1==1){
            //算法不好，强行写死
            return 10;
        }
        if(peace==0){
            //最小和最大值相等
            return 10;
        }
        var v = ((value[1]-minValue)/peace)*2;
        if(v==0){
            return 2;
        }
        return v;
    }

    var seriesData =new Array();
    var serie={
        type: 'graph',
        edgeSymbol: ['none', 'arrow'],
        coordinateSystem: 'calendar',
        links: links,
        symbolSize: 15,
        calendarIndex: 0,
        itemStyle: {
            normal: {
                color: 'green',
                shadowBlue: 9,
                shadowOffsetX: 1.5,
                shadowOffsetY: 3,
                shadowColor: '#555'
            }
        },
        lineStyle: {
            normal: {
                color: '#D10E00',
                width: 1,
                opacity: 1
            }
        },
        data: data.graphData,
        z: 20
    };
    seriesData.push(serie);
    serie={
        type: 'graph',
        edgeSymbol: ['none', 'arrow'],
        coordinateSystem: 'calendar',
        links: links,
        symbolSize: 15,
        calendarIndex: 1,
        itemStyle: {
            normal: {
                color: 'green',
                shadowBlue: 9,
                shadowOffsetX: 1.5,
                shadowOffsetY: 3,
                shadowColor: '#555'
            }
        },
        lineStyle: {
            normal: {
                color: '#D10E00',
                width: 1,
                opacity: 1
            }
        },
        data: data.graphData,
        z: 20
    };
    seriesData.push(serie);
    serie={
        name: data.legendData[0],
        type: 'scatter',
        coordinateSystem: 'calendar',
        data: data.series,
        symbolSize: function (val) {
            //return val[1] / data.compareSizeValue;
            return getSymbolSize(val);
        },
        itemStyle: {
            normal: {
                color: 'red'
            }
        }
    };
    seriesData.push(serie);
    serie={
        name: data.legendData[0],
        type: 'scatter',
        coordinateSystem: 'calendar',
        calendarIndex: 1,
        data: data.series,
        symbolSize: function (val) {
            //return val[1] / data.compareSizeValue;
            return getSymbolSize(val);
        },
        itemStyle: {
            normal: {
                color: 'red'
            }
        }
    };
    seriesData.push(serie);
    if(data.series2&&data.series2.length>0){
        serie={
            name: data.legendData[1],
            type: 'effectScatter',
            coordinateSystem: 'calendar',
            calendarIndex: 1,
            data: data.series2,
            symbolSize: function (val) {
                //return val[1] / data.compareSizeValue;
                return getSymbolSize(val);
            },
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true,
            itemStyle: {
                normal: {
                    color: '#f4e925',
                    shadowBlur: 10,
                    shadowColor: '#333'
                }
            },
            zlevel: 1
        };
        seriesData.push(serie);
        serie={
            name: data.legendData[1],
            type: 'effectScatter',
            coordinateSystem: 'calendar',
            data: data.series2,
            symbolSize: function (val) {
                //return val[1] / data.compareSizeValue;
                return getSymbolSize(val);
            },
            showEffectOn: 'render',
            rippleEffect: {
                brushType: 'stroke'
            },
            hoverAnimation: true,
            itemStyle: {
                normal: {
                    color: '#f4e925',
                    shadowBlur: 10,
                    shadowColor: '#333'
                }
            },
            zlevel: 1
        };
        seriesData.push(serie);
    }


    var option = {
        backgroundColor: '#404a59',

        title: {
            top: 10,
            text: data.title,
            subtext: data.subTitle,
            left: 'center',
            textStyle: {
                color: '#fff'
            }
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a}<br/>{b}({c}"+data.unit+")"
        },
        legend: {
            top: '30',
            left: '100',
            data: data.legendData,
            textStyle: {
                color: '#fff'
            }
        },
        calendar: [{
            top: 80,
            left: 'center',
            range: data.rangeFirst,
            splitLine: {
                show: true,
                lineStyle: {
                    color: '#000',
                    width: 4,
                    type: 'solid'
                }
            },
            yearLabel: {
                formatter: '{start}  上半年',
                textStyle: {
                    color: '#fff'
                }
            },
            itemStyle: {
                normal: {
                    color: '#323c48',
                    borderWidth: 1,
                    borderColor: '#111'
                }
            }
        }, {
            top: 260,
            left: 'center',
            range: data.rangeSecond,
            splitLine: {
                show: true,
                lineStyle: {
                    color: '#000',
                    width: 4,
                    type: 'solid'
                }
            },
            yearLabel: {
                formatter: '{start}  下半年',
                textStyle: {
                    color: '#fff'
                }
            },
            itemStyle: {
                normal: {
                    color: '#323c48',
                    borderWidth: 1,
                    borderColor: '#111'
                }
            }
        }],
        series : seriesData
    };
    createChartEnhanced(option,containId);
}

function createRadarData(data){
    createRadarDataEnhanced(data,'container')
}

//雷达图
function createRadarDataEnhanced(data,containId){
    var option = {
        title: {
            text: data.title
        },
        legend: {
            data: data.legendData
        },
        tooltip: {},
        radar: {
            name: {
                textStyle: {
                    color: '#fff',
                    backgroundColor: '#999',
                    borderRadius: 3,
                    padding: [3, 5]
                }
            },
            indicator : data.indicatorData
        },
        series : (function (){
            var series = [];
            for(var i=0;i<data.series.length;i++){
                series.push({
                    name: data.series[i].name,
                    type: 'radar',
                    data:[
                        {
                            value: data.series[i].data,
                            name: data.series[i].name
                        }
                    ]
                });
            }
            return series;
        })()
    };
    //alert(JSON.stringify(option));
    createChartEnhanced(option,containId);
}

function createTreeMapData(data) {
    createTreeMapDataEnhanced(data,'container')
}

function createTreeMapDataEnhanced(data,containId) {
    var myChart = echarts.init(document.getElementById(containId));
    myChart.showLoading();
    myChart.hideLoading();

    function colorMappingChange(value) {
        var levelOption = getLevelOption(value);
        chart.setOption({
            series: [{
                levels: levelOption
            }]
        });
    }

    var formatUtil = echarts.format;

    function getLevelOption() {
        return [
            {
                itemStyle: {
                    normal: {
                        borderWidth: 0,
                        gapWidth: 5
                    }
                }
            },
            {
                itemStyle: {
                    normal: {
                        gapWidth: 1
                    }
                }
            },
            {
                colorSaturation: [0.35, 0.5],
                itemStyle: {
                    normal: {
                        gapWidth: 1,
                        borderColorSaturation: 0.6
                    }
                }
            }
        ];
    }

    var option = {

        title: {
            text: data.name,
            left: 'center'
        },
        color: colorList,
        tooltip: {
            formatter: function (info) {
                var value = info.value;
                var treePathInfo = info.treePathInfo;
                var treePath = [];

                for (var i = 1; i < treePathInfo.length; i++) {
                    treePath.push(treePathInfo[i].name);
                }

                return [
                    '<div class="tooltip-title">' + formatUtil.encodeHTML(treePath.join('/')) + '</div>',
                    data.name+': ' + formatUtil.addCommas(value) + ' '+data.unit,
                ].join('');
            }
        },

        series: [
            {
                name:data.name,
                type:'treemap',
                visibleMin: 300,
                label: {
                    show: true,
                    formatter: '{b}'
                },
                itemStyle: {
                    normal: {
                        borderColor: '#fff'
                    }
                },
                levels: getLevelOption(),
                data: data.data
            }
        ]
    };
    myChart.setOption(option,true);
}

function createPolarBarChartEnhanced(data,containerId) {
    var seriesData =new Array();
    for(var i=0;i<data.ydata.length;i++){
        var serie={
            type: 'bar',
            data: data.ydata[i].data,
            coordinateSystem: 'polar',
            name: data.ydata[i].name,
            stack: data.ydata[i].stack
        };
        seriesData.push(serie);
    }
    var option = {
        angleAxis: {
            type: 'category',
            data: data.xdata,
            z: 10
        },
        title : {
            text: data.title,
            x:'center'
        },
        color: colorList,
        radiusAxis: {
        },
        tooltip: {
            position: 'top',
            formatter: function (p) {
                return p.name+' ['+p.seriesName+']';
                //return JSON.stringify(p);
            }
        },
        polar: {},
        series: seriesData,
        legend: {
            show: true,
            orient: 'vertical',
            left: 'left',
            data: data.legendData
        }
    };
    createChartEnhanced(option,containerId);
}

function createTreeChartEnhanced(data,containerId) {
    var myChart = echarts.init(document.getElementById(containerId));
    myChart.showLoading();
    myChart.hideLoading();

    echarts.util.each(data.children, function (datum, index) {
        index % 2 === 0 && (datum.collapsed = true);
    });

    myChart.setOption(option = {
        tooltip: {
            trigger: 'item',
            triggerOn: 'mousemove'
        },
        series: [
            {
                type: 'tree',

                data: [data],

                top: '1%',
                left: '7%',
                bottom: '1%',
                right: '20%',

                symbolSize: 7,

                label: {
                    normal: {
                        position: 'left',
                        verticalAlign: 'middle',
                        align: 'right',
                        fontSize: 9
                    }
                },

                leaves: {
                    label: {
                        normal: {
                            position: 'right',
                            verticalAlign: 'middle',
                            align: 'left'
                        }
                    }
                },
                initialTreeDepth:3,
                expandAndCollapse: true,
                animationDuration: 550,
                animationDurationUpdate: 750
            }
        ]
    });
}
function createGaugeChart(data) {
    createGaugeChartEnhanced(data,'container');
}
//仪表盘
function createGaugeChartEnhanced(data,containerId)  {
    var alertOption = {
        tooltip : {
            formatter: "{a} <br/>{b} : {c}%"
        },
        toolbox: {
            feature: {
                restore: {},
                saveAsImage: {}
            }
        },
        title : {
            text: data.title,
            subtext: data.subTitle,
            x:'center'
        },
        series: [
            {
                center: ["50%", "60%"], // 仪表位置
                name: '业务指标',
                type: 'gauge',
                detail: {formatter:'{value}%'},
                data: [{value: data.value, name: data.name}]
            }
        ]
    };
    createChartEnhanced(alertOption,containerId);
}