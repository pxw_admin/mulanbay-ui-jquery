$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/userScore/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'score',
			width :  80,
			title : '评分',
			align : 'center'
		}, {
			field : 'aa',
			title : '详情',
			formatter : function(value, row, index) {
				return '<a href="javascript:showScoreDetail('+row.id+');"><img src="../../static/image/info.png"></img></a>';
			},
			align : 'center'
		}, {
			field : 'endTime',
			title : '数据日期',
			formatter : function(value, row, index) {
				return formatDateStr(value);
			}
		}, {
            field : 'startTime',
            title : '统计时间段',
			formatter : function(value, row, index) {
				return formatDateStr(row.startTime)+'~~'+formatDateStr(row.endTime);
			}
        }, {
			field : 'createdTime',
			title : '创建时间'
		}] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		}, '-', {
			id : 'statBtn',
			text : '统计',
			iconCls : 'icon-stat-line',
			handler : showStat
		}, '-', {
			id : 'reSaveBtn',
			text : '重新评分',
			iconCls : 'icon-reload',
			handler : showReSave
		} ]
	});
}

function initForm(){
}

function loadSearchForm(){
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function showScoreDetail(id) {
	$('#grid').datagrid('clearSelections');
	var url='/userScore/getScoreDetail?id='+id;
	doAjax(null,url,'GET',false,function(data){
		$('#userScore-window').window('open');
		$('#userScoreTb').datagrid({
			//url: '/userCalendar/todayCalendarList',
			showGroup: true,
			scrollbarSize: 0,
			columns:[[
				{field:'id',title:'序号',width:50,align:'center'},
				{field:'name',title:'名称',width:150,align:'center'},
				{field:'score',title:'得分',width:80,align:'center'},
				{field:'maxScore',title:'单项满分',width:80,align:'center'},
				{field:'statValue',title:'统计值',width:80,align:'center'},
				{field:'limitValue',title:'参考值',width:80,align:'center'}
			]]
		});
		//清空所有数据
		$('#userScoreTb').datagrid('loadData', { total: 0, rows: [] });
		var totalScore=0;
		var totalMaxScore=0;
		for (var i = 0; i < data.length; i++) {
			var row = {
				id:i+1,
				name:data[i].name,
				score:data[i].score,
				maxScore:data[i].maxScore,
				statValue:data[i].statValue,
				limitValue:data[i].limitValue
			};
			$('#userScoreTb').datagrid('appendRow',row);
			totalScore+=data[i].score;
			totalMaxScore+=data[i].maxScore;
		}
		var row = {
			id:'',
			name:'<font color="red">'+'总分'+'</font>',
			score:'<font color="red">'+totalScore+'</font>',
			maxScore:'<font color="red">'+totalMaxScore+'</font>',
			statValue:'',
			limitValue:''
		};
		$('#userScoreTb').datagrid('appendRow',row);
	});
}

function showStat(){
	$('#stat-window').window('open');
	var formData = {
		startDate: getYear(0)+'-01-01',
		endDate: getYear(0)+'-12-31'
	};
	$('#stat-window').form('load', formData);
	stat();
}

function stat(){
	var para =form2Json("stat-search-form");
	var url='/userScore/stat';
	doAjax(para,url,'GET',false,function(data){
		if(para.chartType=='PIE'){
			createPieData(data);
		}else{
			createLineData(data);
		}
	});
}

function showReSave() {
	$('#reSave-window').window('open');
}

function reSave(){
	var para =form2JsonEnhanced("reSave-form");
	var url='/userScore/reSave';
	doAjax(para,url,'POST',true,function(data){
		$('#reSave-window').window('close');
		showAll();
	});
}