$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/taskTrigger/getData';
var isScheduleRun = false;

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
        onLoadSuccess : function(data) {
            isScheduleRun = data.isSchedule;
            if (isScheduleRun == false) {
                $('#statusBtn').linkbutton({
                    iconCls : 'icon-cancel',
                    text : '调度系统未启动',
                    onClick: function (){
                        showInfoMsg("如需开启调度功能，请修改配置文件重启系统");
                        //changeScheduleStatus(isScheduleRun);
                    }
                });
            } else {
                var scheduleStatus = data.scheduleStatus;
                var ss='总调度运行正常';
                if(!scheduleStatus){
                    ss='<font color="red">总调度已运行但未启用</font>';
                }
                $('#statusBtn').linkbutton({
                    iconCls : 'icon-ok',
                    text : ss,
                    onClick: function (){
                        changeScheduleStatus(scheduleStatus);
                    }
                });
            }
            $('#scheduleInfo').linkbutton({
                iconCls : 'icon-info',
                text : '正在执行的任务数(' + data.currentlyExecutingJobsCount + ')，已经被调度的任务数('+data.scheduleJobsCount+')'
            });
        },
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称',
            formatter : function(value, row, index) {
                if(row.executing==true){
                    return value+'<font color="red">[执行中]</font>';

                }else {
                    return value;
                }

            }
        }, {
            field : 'aa',
            title : '详情',
            formatter : function(value, row, index) {
                return '<a href="javascript:showScheduleDetail('+row.id+');"><img src="../../static/image/info.png"></img></a>';
            },
            align : 'center'
        },{
            field : 'deployId',
            title : '部署点',
            align : 'center'
        } ,{
            field : 'triggerType',
            title : '调度周期',
            formatter : function(value, row, index) {
            	if(row.triggerIntervel==1){
            		return '每'+row.triggerTypeName+"一次";
				}else{
                    return '每'+row.triggerInterval+row.triggerTypeName+"一次";
                }
            },
            align : 'center'
        } ,{
            field : 'firstExecuteTime',
            title : '首次执行时间',
            align : 'center'
        } ,{
            field : 'nextExecuteTime',
            title : '下一次执行时间',
            formatter : function(value, row, index) {
                if(row.nextExecuteTime==null){
                    return row.firstExecuteTime;
                }else{
                    return value;
                }
            },
            align : 'center'
        } ,{
            field : 'tillNextExecuteTime',
            title : '距离下一次执行还有',
            formatter : function(value, row, index) {
                var ss=tillNowString(value);
                if(value<0||value<=60){
                    return '<font color="red">'+ss+'</font>';

                }else if(value<=3600){
                    return '<font color="#9acd32">'+ss+'</font>';

                }else if(value<=3600*24){
                    return '<font color="purple">'+ss+'</font>';

                }else {
                    return ss;

                }

            },
            align : 'center'
        },{
            field : 'triggerStatus',
            title : '调度状态',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        } ,{
            field : 'lastExecuteResult',
            title : '最后一次执行结果',
            formatter : function(value, row, index) {
                if(value==null||value==''){
                    return '--';
                }else{
                    var name ='<a href="javascript:showLastTaskLog('+row.id+');">'+row.lastExecuteResultName+'</a>';
                    if(value=='SUCCESS'){
                        return name+'<img src="../../static/image/tick.png"></img>';
                    }else if(value=='FAIL'){
                        return name+'<img src="../../static/image/cross.png"></img>';
                    }else{
                        return name;
                    }
                }
            },
            align : 'center'
        } ,{
            field : 'lastExecuteTime',
            title : '最后一次执行时间',
            width :  160,
            formatter : function(value, row, index) {
                if(value==null||value==''){
                    return '--';
                }else{
                    return value+'<a href="javascript:openTaskExecAnalyseWindow('+row.id+');"><img src="../../static/image/sum.png"></img></a>';
                }
            },
            align : 'center'
        } ,{
            field : 'offsetDays',
            title : '日期偏移量',
            align : 'center'
        } ,{
            field : 'totalCount',
            title : '总执行次数',
            formatter : function(value, row, index) {
                return '<a href="javascript:resetTotalCount('+row.id+');">'+value+'</a>';
            },
            align : 'center'
        } ,{
            field : 'failCount',
            title : '总失败次数',
            formatter : function(value, row, index) {
                return '<a href="javascript:resetFailCount('+row.id+');">'+value+'</a>';
            },
            align : 'center'
        },{
            field : 'groupName',
            title : '组名',
            align : 'center'
        },{
            field : 'userId',
            title : '类型',
            formatter : function(value, row, index) {
                if(value==0){
                    return '系统调度';
                }else{
                    return '用户调度';
                }
            },
            align : 'center'
        } ,{
            field : 'distriable',
            title : '支持分布式',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'redoTypeName',
            title : '重做类型',
            align : 'center'
        },{
            field : 'allowedRedoTimes',
            title : '允许重做次数',
            align : 'center'
        } ,{
            field : 'timeout',
            title : '超时时间(秒)',
            align : 'center'
        } ,{
            field : 'checkUnique',
            title : '需要检查唯一',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        } ,{
            field : 'uniqueTypeName',
            title : '唯一性类型',
            formatter : function(value, row, index) {
                if(value==null){
                    return '--';
                }else{

                }
                return value;
            },
            align : 'center'
        } ,{
            field : 'loggable',
            title : '记录日志',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        } ,{
            field : 'notifiable',
            title : '需要提醒',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        } ,{
            field : 'createdTime',
            title : '创建时间',
            align : 'center'
        },{
            field : 'version',
            title : '版本号',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'statBtn',
			text : '统计',
			iconCls : 'icon-stat',
			handler : stat
		}, '-', {
            id : 'planStatBtn',
            text : '计划执行统计',
            iconCls : 'icon-stat',
            handler : planStat
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		}, '-', {
            id : 'manualExecBtn',
            text : '手动执行',
            iconCls : 'icon-cmd',
            handler : openManualExecWindow
        }, '-', {
            id : 'refreshScheduleBtn',
            text : '刷新调度',
            iconCls : 'icon-refresh',
            handler : openRefreshScheduleWindow
        }, '-', {
            id : 'statusBtn',
            text : '调度状态加载中',
            iconCls : 'icon-loading'
        }, '-', {
            id : 'scheduleInfo',
            text : '调度信息加载中',
            iconCls : 'icon-loading',
            handler : showScheduleInfo
        } ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var formData = {
        timeout: 60,
        distriable: true,
        redoType: 'MUNUAL_REDO',
        allowedRedoTimes: 5,
        triggerInterval: 1,
        triggerType: 'HOUR',
        offsetDays: 0,
        triggerStatus: 'ENABLE',
        checkUnique: true,
        uniqueType:'IDENTITY',
        loggable: true,
        notifiable:false

    };
    $('#ff').form('load', formData);
}

function initForm(){
    combotreeLoad('groupNameCategoryList','/taskTrigger/getTaskTriggerCategoryTree?groupField=groupName');
    combotreeLoad('deployIdCategoryList','/taskServer/getTaskServerTree');
}

function loadSearchForm(){
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/taskTrigger/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
    });
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    //解决easyui-combobox在editable:true无法获取值问题
    $('#groupNameCategoryList').combobox('setValue',$('#groupNameCategoryList').combobox('getText') );
    var triggerParas = $("#triggerParas").val();
    var execTimePeriods = $("#execTimePeriods").val();
    var encodePara={
        triggerParas:encodeJsonString(triggerParas),
        execTimePeriods:encodeJsonString(execTimePeriods)
    };
    $('#ff').form('load', encodePara);

    var url='/taskTrigger/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/taskTrigger/create';
    }
    doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}


function del() {
    var delUrlPrefix = '/taskTrigger/delete';
    commonDeleteByIds(delUrlPrefix);
}

function stat(){
	var para =form2JsonEnhanced("search-window");
	var url='/taskTrigger/stat';

	doAjax(para,url,'GET',false,function(data){
		$('#stat-window').window('open');
        //生成饼图
        createPieData(data);
	});
}

function planStat(){
    showUserPlanWindow('TaskTrigger');
}

function showScheduleInfo() {
    var vurl = '/taskTrigger/getScheduleInfo';
    doAjax(null, vurl, 'GET', false, function(data) {
        $('#schedule-info-window').window('open');
        $('#schedule-info-form').form('load', data.scheduleInfo);
        $('#ceJobsTg').datagrid({
            //url: '/userCalendar/todayCalendarList',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'id',title:'编号',width:50,align:'center'},
                {field:'name',title:'调度名称',width:260,align:'center'},
                {field:'info',title:'调度信息',width:70,align:'center'}
            ]]
        });
        //清空所有数据
        $('#ceJobsTg').datagrid('loadData', { total: 0, rows: [] });
        var ceJobs = data.currentlyExecutingJobs;
        if(ceJobs!=null&&ceJobs.length>0){
            for (var i = 0; i < ceJobs.length; i++) {
                var il ='<a href="javascript:showScheduleDetail('+ceJobs[i].id+');"><img src="../../static/image/info.png"></img></a>';
                var row = {
                    id:ceJobs[i].id,
                    name:ceJobs[i].name,
                    info:il
                };
                $('#ceJobsTg').datagrid('appendRow',row);
            }
        }

    });
}

function openManualExecWindow() {
    $('#manual-exec-window').window('open');
    combotreeLoad('taskTriggerList','/taskTrigger/getTaskTriggerTree?needRoot=true');
}

function manualExec(){
    var para =form2JsonEnhanced("manual-exec-form");
    var url='/taskTrigger/manualNew';
    doAjax(para,url,'POST',true,function(data){
    });
}

function editTriggerParas() {
    var className = $("#taskClass").val();
    var vurl = '/taskTrigger/getParaDefine?className='+className;
    doAjax(null, vurl, 'GET', false, function(data) {
        $('#edit-trigger-para-window').window('open');
        var html='';
        html+='<table cellpadding="5"  border="1">\n';
        html+='<tr>';
        html+='<td width="150px;" align="center">参数项</td>';
        html+='<td width="210;" align="center">配置值</td>';
        html+='<td width="190px;" align="center">说明</td>';
        html+='</tr>';
        html+='<br>';
        var paraDefine = data.paraDefine;
        for(var i=0;i<paraDefine.length;i++){
            html+='<tr>';
            html+='<td>'+paraDefine[i].name+':</td>';
            html+='<td>';
            html+=generateParaHtml(paraDefine[i]);
            html+='</td>';
            html+='<td>';
            if(paraDefine[i].desc){
                html+='<font color="green">'+paraDefine[i].desc+'</font>';
            }
            html+='</td>';
            html+='</tr>';
            html+='<br>';
        }
        html+='</table>';
        //setElementInnerHTML('edit-trigger-para-div',html);
        $("#edit-trigger-para-div").html(html);
        //document.getElementById("edit-trigger-para-div").innerHTML=html;
        var pp = $("#triggerParas").textbox('getValue');
        if(pp==null||pp==''){
            //采用默认的配置
            var formData = JSON.parse(data.defaultConfig);
            $('#edit-trigger-para-form').form('load', formData);
        }else{
            //转换为json
            var formData = JSON.parse(pp);
            $('#edit-trigger-para-form').form('load', formData);
        }
    });
}

function generateParaHtml(df) {
    var html='';
    if(df.editType=='BOOLEAN'||df.dataType=='java.lang.Boolean'){
        html+='<input name="'+df.field+'" type="radio" class="easyui-validatebox" required="true" value="true" style="width:30px" >是';
        html+='<input name="'+df.field+'" type="radio" class="easyui-validatebox" required="true" value="false" style="width:30px" >否';
    }else if(df.editType=='COMBOBOX'){
        html+='<select class="selector" name="'+df.field+'" style="width:200px;height:25px">\n';
        var cbData = JSON.parse(df.editData);
        for(var j=0;j<cbData.length;j++){
            html+='<option value="'+cbData[j].id+'">'+cbData[j].text+'</option>\n';
        }
        html+='</select>';
    }else{
        html+='<input class="easyui-textbox" name="'+df.field+'" style="width:200px;height:20px" />';
    }
    return html;
}

function saveTriggerPara() {
    var para =form2JsonEnhanced("edit-trigger-para-form");
    var ss = JSON.stringify(para);
    //替换
    $("#triggerParas").textbox('setValue', ss);
    $('#edit-trigger-para-window').window('close');
}

function editTimeExecConfig() {
    $('#edit-execTimeConfig-window').window('open');
    $('#edit-execTimeConfig-form').form('clear');
    var pp = $("#execTimePeriods").textbox('getValue');
    if(pp!=null&&pp!=''){
        //转换为json
        var formData = JSON.parse(pp);
        $('#edit-execTimeConfig-form').form('load', formData);
    }
}

function saveTimeExecConfig() {
    var para =form2JsonEnhanced("edit-execTimeConfig-form");
    var ss = JSON.stringify(para);
    //替换
    $("#execTimePeriods").textbox('setValue', ss);
    $('#edit-execTimeConfig-window').window('close');
}

function openTaskExecAnalyseWindow(id) {
    $('#task-exec-analyse-window').window('open');
    $('#grid').datagrid('clearSelections');
    $('#taskTriggerList2').combotree({
        url : getFullApiUrl('/taskTrigger/getTaskTriggerTree?needRoot=true'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onChange : function (newValue, oldValue) {
            statTaskExecAnalyse();
        }
    });
    $('#taskTriggerList2').combotree('setValue', id);
}

//时间统计
function statTaskExecAnalyse(){
    var para =form2Json("task-exec-analyse-search-form");
    if(para.taskTriggerId==null||para.taskTriggerId==''){
        $.messager.alert('提示', '请选择具体调度器', 'error');
        return;
    }
    var url='/taskLog/costTimeStat';
    doAjax(para,url,'GET',false,function(data){
        createLineDataEnhanced(data,'container');
    });
}

function showLastTaskLog(taskTriggerId) {
    var url='/taskLog/getLastTaskLog?taskTriggerId='+taskTriggerId;
    $('#grid').datagrid('clearSelections');
    doAjax(null,url,'GET',false,function(data){
        $('#last-exec-log-form').form('clear');
        if(data==null){
            $.messager.alert('提示', '未找到最近一次执行记录,可能该调度配置为不记录日志', 'error');
        }else{
            $('#last-exec-log-window').window('open');
            var formData = {
                taskTriggerId: data.taskTrigger.id,
                taskTriggerName: data.taskTrigger.name,
                executeResultName: data.executeResultName,
                costTime: data.costTime,
                logComment: data.logComment,
                firstExecuteTime:data.startTime,
                lastRedoTime:data.lastStartTime,
                redoTimes:data.redoTimes,
                id:data.id
            };
            $('#last-exec-log-form').form('load', formData);
        }
    });
}

function redoSchedule() {
    $.messager.confirm('提示信息', '是否要重做该次调度?', function(data) {
        if (data) {
            var para =form2JsonEnhanced("last-exec-log-form");
            var url='/taskLog/redo?id='+para.id;
            doAjax(null,url,'GET',true,function(data){
                showLastTaskLog(para.taskTriggerId);
            });
        }
    });
}

function changeScheduleStatus(beforeStatus){
    var info ="是否停止调度?";
    if(!beforeStatus){
        info ="是否开启调度?";
    }
    $.messager.confirm('提示信息', info, function(data) {
        if (data) {
            var url='/taskTrigger/changeScheduleStatus';
            var para = {
                afterStatus: !beforeStatus
            };
            doAjax(para,url,'POST',true,function(data){
                reloadDatagrid();
            });
        }
    });
}

function openRefreshScheduleWindow() {
    $('#refresh-schedule-window').window('open');
    combotreeLoad('taskTriggerList3','/taskTrigger/getTaskTriggerTree?needRoot=true');
}

function refreshSchedule(){
    var para =form2JsonEnhanced("refresh-schedule-form");
    var info ="";
    if(para.taskTriggerId==null||para.taskTriggerId==''){
        info ="是否刷新全部调度?";
    }else{
        info ="是否刷新该调度?";
    }
    $.messager.confirm('提示信息', info, function(data) {
        if (data) {
            var url='/taskTrigger/refreshSchedule';
            doAjax(para,url,'POST',true,function(data){
                reloadDatagrid();
            });
        }
    });
}

function resetTotalCount(id){
    $('#grid').datagrid('clearSelections');
    $.messager.confirm('提示信息', '是否重置该调度的总执行次数?', function(data) {
        if (data) {
            var url='/taskTrigger/resetTotalCount';
            var para = {
                id: id
            };
            doAjax(para,url,'POST',true,function(data){
                reloadDatagrid();
            });
        }
    });
}

function resetFailCount(id){
    $('#grid').datagrid('clearSelections');
    $.messager.confirm('提示信息', '是否重置该调度的总失败次数?', function(data) {
        if (data) {
            var url='/taskTrigger/resetFailCount';
            var para = {
                id: id
            };
            doAjax(para,url,'POST',true,function(data){
                reloadDatagrid();
            });
        }
    });
}

function showScheduleDetail(id) {
    $('#grid').datagrid('clearSelections');
    var url='/taskTrigger/getScheduleDetail?id='+ id;
    doAjax(null,url,'GET',false,function(data){
        $('#trigger-compare-window').window('open');
        loadScheduleDetailData(data);
    });
}

//显示比较数据
function loadScheduleDetailData(data) {
    $('#dbInfoContent').treegrid('loadData', {"total":1,"rows":[
            {"id":'根目录',"name":'根目录'}
        ],"footer":[
            {"name":"Total Persons:","persons":7,"iconCls":"icon-sum"}
        ]});
    if(data.dbInfo){
        showFormatTree('dbInfoContent','根目录',objKeySort(data.dbInfo));
    }
    $('#scheduleInfoContent').treegrid('loadData', {"total":1,"rows":[
            {"id":'根目录',"name":'根目录'}
        ],"footer":[
            {"name":"Total Persons:","persons":7,"iconCls":"icon-sum"}
        ]});
    if(data.scheduleInfo){
        if(data.addToScheduleTime){
            var currentTitle='加入调度器时间:'+data.addToScheduleTime+",当前状态:";
            if(data.isExecuting!=null&&data.isExecuting==true){
                currentTitle+='<font color="green">正在运行中</font>';
            }else{
                currentTitle+='<font color="red">未运行</font>';
            }
        }
        var showData = appendChangeTagToCurrent(data.dbInfo,data.scheduleInfo);
        showFormatTree('scheduleInfoContent','根目录',objKeySort(showData));
        document.getElementById("scheduleRunInfo").innerHTML=currentTitle;
    }else{
        document.getElementById("scheduleRunInfo").innerHTML='未加入调度服务器中';
    }
}
