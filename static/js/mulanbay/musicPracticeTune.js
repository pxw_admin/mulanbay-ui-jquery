$(function() {
	initGrid();
});

var dataUrl='/musicPracticeTune/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2Json("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'musicPractice.id',
            title : '乐器',
            sortable : true,
            formatter : function(value, row, index) {
                return row.musicPractice.musicInstrument.name;
            },
        }, {
            field : 'tune',
            title : '曲子名称',
            sortable : true
        }, {
            field : 'times',
            title : '练习次数',
            sortable : true,
            align : 'center'
        }, {
            field : 'levelName',
            title : '水平',
            sortable : true,
            align : 'center'
        }, {
            field : 'tuneTypeName',
            title : '类型',
            sortable : true,
            align : 'center'
        }, {
            field : 'musicPractice.practiceDate',
            title : '练习日期',
            sortable : true,
            formatter : function(value, row, index) {
                return row.musicPractice.practiceDate;
            }
        }, {
			field : 'aa',
			title : '曲子统计',
			formatter : function(value, row, index) {
				return '<a href="javascript:showNameStat('+row.id+');"><img src="../../static/image/sum.png"></img></a>';
			},
			align : 'center'
		}, {
			field : 'bb',
			title : '水平统计',
			formatter : function(value, row, index) {
				return '<a href="javascript:showLevelStat('+row.id+');"><img src="../../static/image/sum.png"></img></a>';
			},
			align : 'center'
		}] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'statBtn',
			text : '统计',
			iconCls : 'icon-stat',
			handler : getJsonData
		}, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function showNameStat(id) {
	$('#grid').datagrid('clearSelections');
	$('#nameStat-window').window('open');
	$('#nid').val(id);
	nameStat();
}

function nameStat(){
	var para =form2Json("nameStat-search-form");
	var url='/musicPracticeTune/nameStat';
	doAjax(para,url,'GET',false,function(data){
		$('#nameStatPg').datagrid({
			url: '',
			showGroup: true,
			scrollbarSize: 0,
			columns:[[
				{field:'name',title:'名称',width:170,align:'center'},
				{field:'value',title:'值',width:185,align:'center'},
			]]
		});
		//清空所有数据
		$('#nameStatPg').datagrid('loadData', { total: 0, rows: [] });
		var row0 = {
			name:'曲子名称',
			value:data.tune
		};
		$('#nameStatPg').datagrid('appendRow',row0);
		var row1 = {
			name:'首次练习时间',
			value:data.minPracticeDate
		};
		$('#nameStatPg').datagrid('appendRow',row1);
		var row2 = {
			name:'最后练习时间',
			value:data.maxPracticeDate
		};
		$('#nameStatPg').datagrid('appendRow',row2);
		var days = dateDiff(data.minPracticeDate,data.maxPracticeDate);
		var row3 = {
			name:'练习间隔',
			value: formatDays(days)
		};
		$('#nameStatPg').datagrid('appendRow',row3);
		var row4 = {
			name:'练习天数',
			value:data.totalCounts+'天'
		};
		$('#nameStatPg').datagrid('appendRow',row4);
		var row5 = {
			name:'总练习次数',
			value:data.totalTimes+'次'
		};
		$('#nameStatPg').datagrid('appendRow',row5);
	});
}

function showLevelStat(id) {
	$('#grid').datagrid('clearSelections');
	$('#levelStat-window').window('open');
	$('#lid').val(id);
	levelStat();
}

function levelStat(){
	var para =form2Json("levelStat-search-form");
	var url='/musicPracticeTune/levelStat';
	doAjax(para,url,'GET',false,function(data){
		$('#levelStatPg').datagrid({
			url: '',
			showGroup: true,
			scrollbarSize: 0,
			columns:[[
				{field:'levelName',title:'水平',width:118,align:'center'},
				{field:'level',title:'等级',width:118,align:'center'},
				{field:'date',title:'时间',width:118,align:'center'},
			]]
		});
		//清空所有数据
		$('#levelStatPg').datagrid('loadData', { total: 0, rows: [] });
		if(data==null||data.length==0){
			showInfoMsg("未找到相关数据");
			return;
		}
		var n = data.length;
		var lastDate=data[0].minPracticeDate;
		for(var i=0;i<n;i++){
			var dd = data[i];
			if(i>0&&n>1){
				var days = dateDiff(lastDate,dd.minPracticeDate);
				var timeRow = {
					levelName:'<img src="../../static/image/arrow_down_blue.png"></img>',
					level:'',
					date:formatDays(days)
				};
				$('#levelStatPg').datagrid('appendRow',timeRow);
			}
			var row = {
				levelName:dd.levelName,
				level:'L'+dd.levelIndex,
				date:dd.minPracticeDate
			};
			$('#levelStatPg').datagrid('appendRow',row);
			lastDate=dd.minPracticeDate;
		}

	});
}
