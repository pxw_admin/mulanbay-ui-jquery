$(function() {
    loadSearchForm();
    initGrid();
});

var dataUrl='/budget/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称',
            formatter : function(value, row, index) {
                if(row.status=='DISABLE'){
                    return value+'<font color="red">[失效]</font>';
                }else{
                    return value;
                }
            }
        } , {
            field : 'amount',
            title : '金额',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            },
            align : 'center'
        } , {
            field : 'typeName',
            title : '类型',
            align : 'center'
        } , {
            field : 'periodName',
            title : '周期类型',
            align : 'center'
        }, {
            field : 'keywords',
            title : '标签',
            formatter : function(value, row, index) {
                if(value==null||value==''){
                    return '--';
                }else{
                    return '<a href="javascript:showStatByKeywords('+row.id+');">'+value+'</a>';
                }
            },
            align : 'center'
        }, {
            field : 'expectPaidTime',
            title : '本期支付时间',
            formatter : function(value, row, index) {
                if(row.cpPaidTime){
                    return row.cpPaidTime+'(已支付)';
                }else{
                    if(row.nextPaytime){
                        if(row.leftDays==0){
                            return row.nextPaytime+'(今天)';
                        }else if(row.leftDays<0){
                            var ld = formatDays(0-row.leftDays);
                            return row.nextPaytime+'(已过去'+ld+')';
                        }else{
                            var ld = formatDays(row.leftDays);
                            return row.nextPaytime+'('+ld+'后)';
                        }
                    }else{
                        return '--';
                    }
                }
            },
            align : 'center'
        }, {
            field : 'cpPaidAmount',
            title : '本期支付金额',
            formatter : function(value, row, index) {
                if(row.cpPaidAmount){
                    var ca = (row.cpPaidAmount-row.amount).toFixed(2);
                    if(ca>0){
                        return formatMoneyWithSymbal(row.cpPaidAmount)+'<font color="red">(+'+ca+')</font>';
                    }else if(ca<0){
                        return formatMoneyWithSymbal(row.cpPaidAmount)+'<font color="green">(-'+(0-ca)+')</font>';
                    }else{
                        return formatMoneyWithSymbal(row.cpPaidAmount);
                    }
                }else{
                    return '--';
                }
            },
            align : 'center'
        }, {
            field : 'log',
            title : '新增流水',
            formatter : function(value, row, index) {
                return '<a href="javascript:addLog('+row.id+','+row.amount+');"><img src="../../static/image/add.png"></img></a>';
            },
            align : 'center'
        } , {
            field : 'firstPaidTime',
            title : '首次支付时间'
        } , {
            field : 'lastPaidTime',
            title : '上一次支付时间'
        },{
            field : 'remind',
            title : '配置提醒',
            formatter : function(value, row, index) {
                if(value==null||value==false){
                    return '<img src="../../static/image/cross.png"></img>';
                }else{
                    return '<img src="../../static/image/tick.png"></img>';
                }
            },
            align : 'center'
        },{
            field : 'bindFlow',
            title : '绑定流水',
            formatter : function(value, row, index) {
                if(value==null||value==false){
                    return '<img src="../../static/image/cross.png"></img>';
                }else{
                    return '<img src="../../static/image/tick.png"></img>';
                }
            },
            align : 'center'
        },{
            field : 'status',
            title : '状态',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
            id : 'statBtn',
            text : '统计',
            iconCls : 'icon-stat',
            handler : showStat
        }, '-', {
            id : 'timelineStatBtn',
            text : '进度统计',
            iconCls : 'icon-stat',
            handler : showTimelineStat
        }, '-', {
            id : 'statBtn',
            text : '预算执行情况',
            iconCls : 'icon-stat',
            handler : showLogStat
        }, '-', {
            id : 'analyseBtn',
            text : '预算分析',
            iconCls : 'icon-stat',
            handler : showBudgetAnalyse
        }, '-', {
            id : 'createLogBtn',
            text : '新增流水',
            iconCls : 'icon-add',
            handler : addLog
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var formData = {
        type:'INSURANCE',
        period:'MONTHLY',
        status:'ENABLE',
		remind:true,
        bindFlow:true
    };
    $('#ff').form('load', formData);
}

function initForm(){
    var startDate = getDay(-365);
    combotreeLoad('keywordsCategoryList','/buyRecord/getKeywordsTree?needRoot=true&startDate='+startDate);
}

function loadSearchForm() {

}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/budget/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/budget/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/budget/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

//增加流水
function addLog(budgetId,amount) {
    $('#grid').datagrid('clearSelections');
    $('#budget-log-window').window('open');
    $('#budget-log-ff').form('clear');
    combotreeLoad('budgetList2','/budget/getBudgetTree?needRoot=true');
    $('#budgetList2').combotree('setValue', budgetId);
    var formData = {
        amount:amount,
        occurDate:getNowDateTimeString()
    };
    $('#budget-log-ff').form('load', formData);
}

function saveLogData() {
    var url='/budgetLog/create';
    doFormSubmit('budget-log-ff',url,function(){
        closeWindow('budget-log-window');
    });
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/budget/delete';
    commonDeleteByIds(delUrlPrefix);
}

function showStat() {
    $('#budget-stat-window').window('open');
    stat();
}

function stat(){
    var para =form2Json("budget-stat-search-form");
    var url='/budget/stat';
    doAjax(para,url,'GET',false,function(data){
        //生成饼图
        createPieData(data);
    });
}

function showLogStat() {
    $('#budget-log-stat-window').window('open');
    $('#budgetList').combotree({
        url : getFullApiUrl('/budget/getBudgetTree?needRoot=true'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onChange : function (newValue, oldValue) {
            logStat();
        }
    });
    // 查询条件今年的
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31',
        needOutBurst:true,
        budgetKey:'p3'
    };
    $('#budget-log-stat-search-form').form('load', formData);
    logStat();
}

function logStat(){
    var para =form2Json("budget-log-stat-search-form");
    if(para.budgetKey==null||para.budgetKey==''){
    	return;
	}
    var url='/budgetLog/stat';

    doAjax(para,url,'GET',false,function(data){
        //生成饼图
        createBarDataEnhanced(data,'logContainer');
    });
}

function showStatByKeywords(id) {
    $('#grid').datagrid('clearSelections');
    $('#budget-stat-by-keywords-window').window('open');
    // 查询条件今年的
    var formData = {
        groupField: 'sub_goods_type_id',
        id:id
    };
    $('#budget-stat-by-keywords-search-form').form('load', formData);
    statByKeywords();
}

function statByKeywords() {
    var para =form2Json("budget-stat-by-keywords-search-form");
    var url='/budget/statByKeywords';

    doAjax(para,url,'GET',false,function(data){
        //生成饼图
        createPieDataEnhanced(data,'keywordsStatContainer');
    });
}

function showTimelineStat() {
    $('#budget-timeline-stat-window').window('open');
    var formData = {
        bussDay: getDay(0),
        statType:'RATE'
    };
    $('#budget-timeline-stat-search-form').form('load', formData);
    timelineStat();
}

function timelineStat(){
    var para =form2Json("budget-timeline-stat-search-form");
    var url='/budget/timelineStat';

    doAjax(para,url,'GET',false,function(data){
        //生成折现图
        createLineDataEnhanced(data,'timelineStatContainer');
    });
}

function showBudgetAnalyse() {
    $('#budget-analyse-window').window('open');
    var formData = {
        period:'MONTHLY',
        date: getDay(0)
    };
    $('#budget-analyse-search-form').form('load', formData);
    budgetAnalyse();
}


function budgetAnalyse() {
    var para =form2Json("budget-analyse-search-form");
    var url='/budget/analyse';
    doAjax(para,url,'GET',false,function(data){
        document.getElementById("title").innerHTML='<font color="red">'+data.title+'</font>';
        var consumeBudgetRate = getPercent(data.consumeAmount,data.budgetAmount);
        var formData = {
            budgetAmount:data.budgetAmount,
            consumeBudgetRate:consumeBudgetRate,
            ncAmount:data.ncAmount,
            bcAmount:data.bcAmount,
            trAmount:data.trAmount,
            consumeAmount:data.consumeAmount,
            lastIncome:data.lastIncome
        };
        $('#budget-analyse-form').form('load', formData);
        $('#budgetListTg').datagrid({
            //url: '/userCalendar/todayCalendarList',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'id',title:'序号',width:50,align:'center'},
                {field:'name',title:'预算名称',width:200,align:'center'},
                {field:'amount',title:'预算金额',width:80,align:'center'},
                {field:'typeName',title:'类型',width:60,align:'center'},
                {field:'periodName',title:'周期类型',width:60,align:'center'},
                {field:'drate',title:'系数',width:50,align:'center'},
                {field:'ttAmount',title:'总金额',width:80,align:'center'},
                {field:'pp',title:'百分比',width:70,align:'center'},
                {field:'bir',title:'占据收入',width:70,align:'center'}
            ]]
        });
        //清空所有数据
        $('#budgetListTg').datagrid('loadData', { total: 0, rows: [] });
        var budgetList = data.budgetList;
        for (var i = 0; i < budgetList.length; i++) {
            var ttAmount = budgetList[i].drate*budgetList[i].amount;
            var bir='--';
            if(data.lastIncome>0){
                bir = getPercent(ttAmount,data.lastIncome)+'%'
            }
            var row = {
                id:i+1,
                name:budgetList[i].name,
                amount:formatMoneyWithSymbal(budgetList[i].amount),
                typeName:budgetList[i].typeName,
                periodName:budgetList[i].periodName,
                drate:budgetList[i].drate,
                ttAmount:formatMoneyWithSymbal(ttAmount),
                pp:getPercent(ttAmount,data.budgetAmount)+'%',
                bir:bir
            };
            $('#budgetListTg').datagrid('appendRow',row);
        }
    });
}

function reStatTimeline(){
    var dd =form2Json("budget-timeline-stat-search-form");
    var msg;
    if(dd.period=='MONTHLY'){
        msg="是否重新统计该月的所有预算及具体消费数据?";
    }else{
        msg="是否重新统计该年的所有预算及具体消费数据?";
    }
    $.messager.confirm('提示信息', msg, function(data) {
        if (data) {
            var para ={
                period:dd.period,
                bussDay:dd.bussDay
            };
            var url='/budget/reStatTimeline';
            doAjax(para,url,'POST',true,function(data){
                //重新显示
                timelineStat();
            });
        }else{
        }
    });
}