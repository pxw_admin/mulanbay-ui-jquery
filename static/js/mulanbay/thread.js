$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/thread/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '编号',
			sortable : true,
			align : 'center'
		}, {
            field : 'threadName',
            title : '名称'
        }, {
            field : 'aaa',
            title : '线程详情',
            formatter : function(value, row, index) {
                return '<a href="javascript:showThreadInfo('+row.id+');"><img src="../../static/image/info.png"></img></a>';
            },
            align : 'center'
        }, {
            field : 'interval',
            title : '周期(秒)',
            align : 'center'
        }, {
            field : 'continuedSeconds',
            title : '持续时间(秒)',
            align : 'center'
        }, {
            field : 'failCount',
            title : '失败总次数',
            align : 'center'
        }, {
            field : 'totalCount',
            title : '执行总次数',
            align : 'center'
        },{
			field : 'stop',
			title : '状态',
            formatter : function(value, row, index) {
                if(value==true){
                    return '<img src="../../static/image/cross.png"></img>';
                }else{
                    return '<img src="../../static/image/tick.png"></img>';
                }
            },
            align : 'center'
        }, {
            field : 'created',
            title : '创建时间'
        }, {
            field : 'lastExecuteTime',
            title : '最近一次执行时间'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function loadSearchForm() {

}

function showAll() {
    refreshDatagrid(dataUrl,1,true);
}

function showThreadInfo(id) {
    $('#threadInfo-window').window('open');
    $('#threadInfoTb').datagrid({
        url: '/thread/aa',
        showGroup: true,
        scrollbarSize: 0,
        columns:[[
            {field:'name',title:'项目',width:150,align:'center'},
            {field:'value',title:'值',width:300,align:'center'}
        ]]
    });
    //清空所有数据
    $('#threadInfoTb').datagrid('loadData', { total: 0, rows: [] });
    var url='/thread/getThreadInfo?id='+id;
    doAjax(null,url,'GET',false,function(data){
        if(data){
            var ti = data;
            for (var i = 0; i < ti.length; i++) {
                $('#threadInfoTb').datagrid('appendRow',{
                    name:ti[i].name,
                    value:ti[i].value
                });
            }
        }

    });
    $('#grid').datagrid('clearSelections');
}