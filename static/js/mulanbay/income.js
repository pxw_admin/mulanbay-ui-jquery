$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/income/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称'
        }, {
            field : 'amount',
            title : '金额',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'buyRecordId',
            title : '详情',
            align : 'center',
            formatter : function(value, row, index) {
                if(value!=null){
                    return '<a href="javascript:showBuyRecordDetail('+value+');"><img src="../../static/image/info.png"></img></a>';
                }else{
                    return '';
                }
            }
        }, {
            field : 'typeName',
            title : '类型'
        }, {
            field : 'occurTime',
            title : '发生时间'
        }, {
            field : 'aa',
            title : '关联账户',
            formatter : function(value, row, index) {
                if(row.account){
                    return row.account.name;
                }else{
                    return '--';
                }
            }
        }, {
            field : 'createdTime',
            title : '创建时间'
        }, {
            field : 'lastModifyTime',
            title : '最后更新时间'
        },{
			field : 'status',
			title : '状态',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
            id : 'statBtn',
            text : '统计',
            iconCls : 'icon-stat',
            handler : showStat
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var formData = {
        type:'SALARY',
        status:'ENABLE'
    };
    $('#ff').form('load', formData);

}

function initForm(){
    combotreeLoad('accountList2','/account/getAccountTree');
}

function loadSearchForm(){
    combotreeLoad('accountList','/account/getAccountTree?needRoot=true');
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/income/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
        $('#accountList2').combotree('setValue', data.account.id);
        //设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/income/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/income/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/income/delete';
    commonDeleteByIds(delUrlPrefix);
}

function stat(){
    var para =form2Json("income-stat-search-form");
    var url='/income/stat';
    doAjax(para,url,'GET',false,function(data){
        //生成饼图
        createPieData(data);
    });
}

function showStat() {
    $('#income-stat-window').window('open');
    combotreeLoad('accountList3','/account/getAccountTree?needRoot=true');
    stat();
}

function showBuyRecordDetail(buyRecordId) {
    $('#buyRecordDetail-window').window('open');
    $('#grid').datagrid('clearSelections');
    var url='/buyRecord/get?id='+buyRecordId;
    doAjax(null,url,'GET',false,function(data){
        $('#buyRecordDetailTb').datagrid({
            //url: '/buyRecord/get',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'item',title:'项',width:80,align:'center'},
                {field:'value',title:'值',width:150,align:'center'}
            ]]
        });
        $('#buyRecordDetailTb').datagrid('loadData', { total: 0, rows: [] });
        $('#buyRecordDetailTb').datagrid('appendRow',{
            item:'买入价',
            value:formatMoneyWithSymbal(data.totalPrice)
        });
        $('#buyRecordDetailTb').datagrid('appendRow',{
            item:'售出价',
            value:formatMoneyWithSymbal(data.soldPrice)
        });
        var dd = data.soldPrice*10/data.totalPrice;
        var sd = dd.toFixed(1)+'折';
        $('#buyRecordDetailTb').datagrid('appendRow',{
            item:'折旧率',
            value:sd
        });
        var ts = data.totalPrice-data.soldPrice;
        var ss='';
        if(ts>0){
            ss='<font color="green">亏损'+formatMoneyWithSymbal(ts)+'</font>';
        }else{
            ss='<font color="red">盈利'+formatMoneyWithSymbal(0-ts)+'</font>';
        }
        $('#buyRecordDetailTb').datagrid('appendRow',{
            item:'盈亏',
            value:ss
        });
    });
}