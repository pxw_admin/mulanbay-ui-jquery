$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/treatDrug/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2Json("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
        columns : [ [ {
            field : 'id',
            title : '记录号',
            sortable : true,
            align : 'center'
        }, {
            field : 'treatRecord.hospital',
            title : '医院',
            sortable : true,
            formatter : function(value, row, index) {
                return row.treatRecord.hospital;
            }
        }, {
            field : 'treatDate',
            title : '看病日期',
            sortable : true
        }, {
            field : 'name',
            title : '药品名',
            formatter : function(value, row, index) {
                var s1 = tillNowSeconds(null,row.beginDate);
                var s2 = tillNowSeconds(null,row.endDate);
                //alert(s1+'--'+s2);
                if(s1<0&&s2>0){
                    //正在用药期
                    return '<font color="red">'+value+'</font>';
                }else{
                    return value;
                }
            }
        }, {
            field : 'aaa',
            title : '新增用药',
            formatter : function(value, row, index) {
                return '<a href="javascript:addDetail('+row.id+');"><img src="../../static/image/add.png"></img></a>';
            },
            align : 'center'
        }, {
            field : 'amount',
            title : '数量',
            formatter : function(value, row, index) {
                return value+row.unit;
            },
            align : 'center'
        }, {
            field : 'treatRecord.disease',
            title : '疾病症状',
            formatter : function(value, row, index) {
                return row.treatRecord.disease;
            }
        }, {
            field : 'treatRecord.organ',
            title : '器官',
            formatter : function(value, row, index) {
                return row.treatRecord.organ;
            }
        }, {
            field : 'treatRecord.diagnosedDisease',
            title : '确诊疾病',
            formatter : function(value, row, index) {
                return row.treatRecord.diagnosedDisease;
            }
        }, {
            field : 'unitPrice',
            title : '单价',
            align : 'center',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'totalPrice',
            title : '总价',
            align : 'center',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'perDay',
            title : '用药频率',
            formatter : function(value, row, index) {
                return row.perDay+'天'+row.perTimes+'次';
            },
            align : 'center'
        }, {
            field : 'eu',
            title : '用药量',
            formatter : function(value, row, index) {
                if(value!=null){
                    return '每次'+row.ec+row.eu;
                }else {
                    return '--';
                }
            },
            align : 'center'
        }, {
            field : 'useWay',
            title : '使用方式',
            align : 'center'
        }, {
            field : 'beginDate',
            title : '用药开始日期',
            align : 'center'
        }, {
            field : 'endDate',
            title : '用药结束日期',
            align : 'center'
        }, {
            field : 'available',
            title : '是否有效',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        }, {
            field : 'remind',
            title : '提醒',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        }] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		},{
            id : 'createTmpBtn',
            text : '快速新增用药记录',
            iconCls : 'icon-add',
            handler : showAddDetailByTemplate
        }, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
            id : 'statBtn',
            text : '统计',
            iconCls : 'icon-stat',
            handler : showStat
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
    $.messager.alert('提示', '目前该功能不支持，请到看病记录管理中操作!', 'error');
}

function initForm(){
    combotreeLoad('diseaseCategoryList2','/treatDrug/getTreatDrugCategoryTree?groupField=disease');
    combotreeLoad('drugNameCategoryList','/treatDrug/getTreatDrugCategoryTree?groupField=name');
}

function loadSearchForm(){
    combotreeLoad('diseaseCategoryList','/treatDrug/getTreatDrugCategoryTree?groupField=disease');
    combotreeLoad('tagsList','/treatRecord/getTagsTree?needRoot=true');
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/treatDrug/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
        $("#treatDrugSearchId").val(data.id);
        $("#treatRecordId").val(data.treatRecord.id);
        initGridDrugDetail();
    });
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/treatDrug/edit';
    if($("#treatDrugId").val()==null||$("#treatDrugId").val()==''){
        url='/treatDrug/create';
    }
    $('#diseaseCategoryList2').combobox('setValue',$('#diseaseCategoryList2').combobox('getText') );
    $('#drugNameCategoryList').combobox('setValue',$('#drugNameCategoryList').combobox('getText') );

    doFormSubmit('ff',url,function(data){
		//closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
        $("#treatDrugSearchId").val(data.id);
    });
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/treatDrug/delete';
    commonDeleteByIds(delUrlPrefix);
}

/**
 * 快速增加用药记录
 * @param treatDrugId
 */
function addDetail(treatDrugId) {
    $('#grid').datagrid('clearSelections');
    //这行代码其实只是为了保证增加详情时的外键验证作用
    $("#treatDrugId").val(treatDrugId);
    $('#eidt-window-drugDetail').window('open');
    $('#ff-drugDetail').form('clear');
    $('#ff-drugDetail').form.url='/treatDrugDetail/create';
    var formData = {
        treatDrugId:treatDrugId,
        occurTime:getNowDateTimeString()
    };
    $('#ff-drugDetail').form('load', formData);
}

function showAddDetailByTemplate() {
    $('#create-by-template-window').window('open');
    var formData = {
        templateDate:getDay(-1)
    };
    $('#create-by-template-form').form('load', formData);
}

function addDetailByTemplate() {
    var para =form2Json("create-by-template-form");
    var vurl = '/treatDrugDetail/createByTemplate';
    doAjax(para, vurl, 'POST',true, function(data) {
        $('#create-by-template-window').window('close');
        //reloadDatagrid();
    });
}

function showStat() {
    $('#stat-window').window('open');
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31',
        mergeSameName:false
    };
    $('#stat-search-form').form('load', formData);
    stat();
}

function stat() {
    var para =form2Json("stat-search-form");
    var url='/treatDrugDetail/stat';
    doAjax(para, url, 'GET',false, function(data) {
        $('#pg').datagrid({
            url: '/treatOperation/aa',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'id',title:'序号',width:50,align:'center'},
                {field:'name',title:'药品名',width:200,align:'center'},
                {field:'treatDate',title:'看病时间',width:120,align:'center'},
                {field:'minTime',title:'第一次用药时间',width:150,align:'center'},
                {field:'maxTime',title:'最后一次用药时间',width:150,align:'center'},
                {field:'totalCount',title:'总次数',width:100,align:'center'},
                {field:'days',title:'用药天数',width:100,align:'center'},
                {field:'star',title:'用药周数',width:100,align:'center'}
            ]]
        });
        //清空所有数据
        $('#pg').datagrid('loadData', { total: 0, rows: [] });
        for (var i = 0; i < data.length; i++) {
            var name =data[i].name;
            var s1 = tillNowSeconds(null,data[i].minTime);
            var s2 = tillNowSeconds(null,data[i].maxTime);
            if(s1<0&&s2>=-12*3600){
                //正在用药期
                name= '<font color="red">'+name+'</font>';
            }
            var days = data[i].daysLong+1;
            var star='';
            var weeks = Math.round(days/7);
            if(weeks<1){
                star='';
            }else if(weeks<=2){
                star='<font color="green">'+weeks+'周</font>';
            }else if(weeks<=4){
                star='<font color="purple">'+weeks+'周</font>';
            }else {
                star='<font color="red">'+weeks+'周</font>';
            }
            var row = {
                id:i+1,
                name:name,
                treatDate:data[i].treatDate,
                minTime:data[i].minTime,
                maxTime:data[i].maxTime,
                totalCount:data[i].totalCount,
                days:days+'天',
                star:star
            };
            $('#pg').datagrid('appendRow',row);
        }
    });
}

function showBodyAbnomarlAnalyse(comoboxId,groupField) {
    var name=$('#'+comoboxId).combobox('getValue');;
    if(name!=null&&name!=''){
        showBodyAbnormalRecordAnalyseWindow(name,groupField);
    }else{
        $.messager.alert('提示', '请先输入相应的疾病或器官信息！', 'info');
    }
}