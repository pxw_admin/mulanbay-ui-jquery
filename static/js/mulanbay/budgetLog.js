$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/budgetLog/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '预算账户',
            formatter : function(value, row, index) {
                if(row.budget){
                    return row.budget.name;
                }else{
                    return '['+row.periodName+']预算合计';
                }
            }
        }, {
            field : 'bussKey',
            title : '业务KEY'
        }, {
            field : 'occurDate',
            title : '发生时间',
            formatter : function(value, row, index) {
                return formatDateStr(value);
            }
        }, {
            field : 'budgetAmount',
            title : '预算金额',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'aa',
            title : '实际消费金额',
            formatter : function(value, row, index) {
                var tt = row.ncAmount+row.bcAmount+row.trAmount;
                if(tt>row.budgetAmount){
                    return '<font color="red">'+formatMoneyWithSymbal(tt)+'</font>';
                }else {
                    return formatMoneyWithSymbal(tt);
                }
            }
        }, {
            field : 'ncAmount',
            title : '实际普通消费金额',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'bcAmount',
            title : '实际突发消费金额',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'trAmount',
            title : '实际看病消费金额',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'incomeAmount',
            title : '收入统计',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        },{
            field : 'aaa',
            title : '重新统计',
            formatter : function(value, row, index) {
                if(row.budget){
                    return '--';
                }else{
                    return '<a href="javascript:reSave('+row.id+');"><img src="../../static/image/sum.png"></img></a>';
                }
            },
            align : 'center'
        },{
            field : 'bbb',
            title : '预算快照',
            formatter : function(value, row, index) {
                if(row.budget){
                    return '--';
                }else{
                    return '<a href="javascript:showBudgetSnapshotList('+row.id+');"><img src="../../static/image/info.png"></img></a>';
                }
            },
            align : 'center'
        } , {
            field : 'remark',
            title : '说明'
        }, {
            field : 'createdTime',
            title : '创建时间'
        }, {
            field : 'lastModifyTime',
            title : '最后更新时间'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
            id : 'createBtn',
            text : '新增',
            iconCls : 'icon-add',
            handler : add
        }, '-', {
            id : 'editBtn',
            text : '修改',
            iconCls : 'icon-edit',
            handler : edit
        }, '-', {
            id : 'deleteBtn',
            text : '删除',
            iconCls : 'icon-remove',
            handler : del
        },{
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
    loadBudgetTree();
}

function edit() {
    var rows = getSelectedSingleRow();
    if(rows[0].budget){
        var url='/budgetLog/get?id='+ rows[0].id;
        doAjax(null,url,'GET',false,function(data){
            $('#eidt-window').window('open');
            $('#ff').form('clear');
            loadBudgetTree();
            data.amount = data.ncAmount;
            $('#ff').form('load', data);
            if(data.budget){
                $('#budgetList2').combotree('setValue', data.budget.id);
            }
        });
    }else{
        $.messager.alert('提示', '自动统计类无法修改，请选择重新统计功能', 'error');
    }
    $('#grid').datagrid('clearSelections');
}

function saveData() {
    var url='/budgetLog/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/budgetLog/create';
    }
    doFormSubmit('ff',url,function(){
        closeWindow('eidt-window');
        $('#grid').datagrid('clearSelections');
        reloadDatagrid();
    });
}

function loadSearchForm(){
    combotreeLoad('budgetList','/budget/getBudgetTree?needRoot=true');
}

function loadBudgetTree(){
    $('#budgetList2').combotree({
        url : getFullApiUrl('/budget/getBudgetTree?needRoot=true'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onChange : function (newValue, oldValue) {
            getBudget(newValue);
        }
    });
}

function getBudget(budgetId) {
    if(budgetId==null||budgetId==''){
        return;
    }
    var url='/budget/get?id='+budgetId;
    doAjax(null,url,'GET',false,function(data){
        var formData = {
            amount: data.amount
        };
        $('#ff').form('load', formData);

    });
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/budgetLog/delete';
    commonDeleteByIds(delUrlPrefix);
}

function reSave(id){
    $('#grid').datagrid('clearSelections');
    $.messager.confirm('提示信息', '是否要重新统计该条记录?', function(data) {
        if (data) {
            var para = {
                id: id
            };
            var url='/budgetLog/reSave';
            doAjax(para,url,'POST',true,function(data){
                refreshDatagrid(dataUrl,1,true);
            });
        }
    });

}

function showBudgetSnapshotList(id) {
    $('#grid').datagrid('clearSelections');
    var url='/budgetSnapshot/getData';
    var para={
        page:0,
        needTotal:false,
        budgetLogId:id
    };
    doAjax(para,url,'GET',false,function(data){
        $('#budgetSnapshotList-window').window('open');
        $('#budgetSnapshotListTg').datagrid({
            //url: '../userCalendar/todayCalendarList',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'id',title:'序号',width:50,align:'center'},
                {field:'name',title:'名称',width:150,align:'center'},
                {field:'amount',title:'预算金额',width:80,align:'center'},
                {field:'typeName',title:'类型',width:60,align:'center'},
                {field:'periodName',title:'周期类型',width:60,align:'center'},
                {field:'cpPaidAmount',title:'支付金额',width:80,align:'center'},
                {field:'cpPaidTime',title:'支付时间',width:140,align:'center'},
                {field:'createdTime',title:'创建时间',width:140,align:'center'}
            ]]
        });
        //清空所有数据
        $('#budgetSnapshotListTg').datagrid('loadData', { total: 0, rows: [] });
        var bsList = data.rows;
        for (var i = 0; i < bsList.length; i++) {
            var row = {
                id:i+1,
                name:bsList[i].name,
                amount:formatMoneyWithSymbal(bsList[i].amount),
                typeName:bsList[i].typeName,
                periodName:bsList[i].periodName,
                cpPaidAmount:formatMoneyWithSymbal(bsList[i].cpPaidAmount),
                cpPaidTime:bsList[i].cpPaidTime,
                createdTime:bsList[i].createdTime
            };
            $('#budgetSnapshotListTg').datagrid('appendRow',row);
        }
    });
}