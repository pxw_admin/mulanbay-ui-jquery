
var dataUrlTreatTest='/treatTest/getData';

function initGridTreatTest(){
	$('#treatTestGrid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrlTreatTest),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2Json("treatTest-search-form"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#gridDrugDetail').datagrid('uncheckAll');
            $('#gridDrugDetail').datagrid('checkRow', rowIndex);
            editTreatTest();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'treatOperation.name',
            title : '手术/检查项目',
            formatter : function(value, row, index) {
                return row.treatOperation.name;
            }
        }, {
            field : 'name',
            title : '名称',
            align : 'center'
        }, {
            field : 'value',
            title : '检查结果',
            align : 'center'
        }, {
            field : 'aaa',
            title : '参考范围',
            formatter : function(value, row, index) {
                if(row.minValue!=null){
                    return row.minValue+'~'+row.maxValue;
                }else if(row.referScope!=null){
                    return row.referScope;
                }else {
                    return '--';
                }
            },
            align : 'center'
        }, {
            field : 'unit',
            title : '单位',
            align : 'center'
        }, {
            field : 'result',
            title : '分析结果',
            formatter : function(value, row, index) {
                if(value=='LOWER'){
                    return '<font color="purple">'+row.resultName+'</font>';
                }else if(value=='NORMAL'){
                    return '<font color="green">'+row.resultName+'</font>';
                }else if(value=='HIGHER'){
                    return '<font color="red">'+row.resultName+'</font>';
                }else if(value=='DISEASE'){
                    return '<font color="red">'+row.resultName+'</font>';
                }else {
                    return '--';
                }
            },
            align : 'center'
        }, {
            field : 'typeName',
            title : '分类/试验方法',
            align : 'center'
        }, {
            field : 'testDate',
            title : '采样时间',
            align : 'center'
        }] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtnTreatTest',
			text : '新增',
			iconCls : 'icon-add',
			handler : addTreatTest
		}, '-', {
			id : 'editBtnTreatTest',
			text : '修改',
			iconCls : 'icon-edit',
			handler : editTreatTest
		}, '-', {
			id : 'deleteBtnTreatTest',
			text : '删除',
			iconCls : 'icon-remove',
			handler : delTreatTest
		}, '-', {
			id : 'searchBtnTreatTest',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAllTreatTest
		} ]
	});
}

function initTreatTestForm() {
    $('#treatTestNameCategoryList').combobox({
        url : getFullApiUrl('/treatTest/getTreatTestCategoryTree'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onSelect:function(node) {
            loadLastTest(node.id);
        }
    });
}

function loadLastTest(name) {
    var url='/treatTest/getLastTest';
    var para ={
        name:name
    }
    doAjax(para,url,'GET',false,function(data){
        var formData = {
            unit: data.unit,
            minValue: data.minValue,
            maxValue: data.maxValue
        };
        $('#ff-treatTest').form('load', formData);
    });
}

function addTreatTest() {
	$('#edit-treatTest-window').window('open');
	$('#ff-treatTest').form('clear');
	$('#ff-treatTest').form.url='/treatTest/create';
    initTreatTestForm();
    $("#fkTreatOperationId").val($("#treatTestTreatOperationSearchId").val());
}

/**
 * 继续添加
 */
function continueAddTreatTest() {
    var fd = form2Json("ff-treatTest");
    var initData = {
        typeName:fd.typeName,
        testDate:fd.testDate,
        treatOperationId:fd.treatOperationId
    };
    $('#ff-treatTest').form('clear');
    $('#ff-treatTest').form('load', initData);
    $("#fkTreatOperationId").val($("#treatTestTreatOperationSearchId").val());
}

function editTreatTest() {
    var rows = $('#treatTestGrid').datagrid('getSelections');
    var num = rows.length;
    if (num == 0) {
        $.messager.alert('提示', '请选择一条记录进行操作!', 'info');
        return;
    }
	var url='/treatTest/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
        $('#edit-treatTest-window').window('open');
        $('#ff-treatTest').form('clear');
        $('#ff-treatTest').form('load', data);
		//设置字符
		$('#treatTestGrid').datagrid('clearSelections');
        $("#fkTreatOperationId").val($("#treatTestTreatOperationSearchId").val());
    });
}

function showAllTreatTest() {
    var vurl =dataUrlTreatTest;
    $('#treatTestGrid').datagrid({
        url : getFullApiUrl(vurl),
        type : 'GET',
        queryParams: form2Json("treatTest-search-form"),
        pageNumber : 1,
        onLoadError : function() {
            $.messager.alert('错误', '加载数据异常！', 'error');
        }
    });
}

function saveDataTreatTest(con) {
    var url='/treatTest/edit';
    if($("#treatTestId").val()==null||$("#treatTestId").val()==''){
        url='/treatTest/create';
    }
    $('#treatTestNameCategoryList').combobox('setValue',$('#treatTestNameCategoryList').combobox('getText') );
    doFormSubmit('ff-treatTest',url,function(data){
        $('#treatTestGrid').datagrid('clearSelections');
        showAllTreatTest();
        if(con){
            continueAddTreatTest();
        }else{
            closeWindow('edit-treatTest-window');
        }
	});
}

function getSelectedIdsTreatTest() {
	var ids = [];
    var rows = $('#treatTestGrid').datagrid('getSelections');
    var num = rows.length;
    if (num == 0) {
        $.messager.alert('提示', '请选择一条记录进行操作!', 'info');
        return;
    }
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function delTreatTest() {
	var arr = getSelectedIdsTreatTest();
	if (arr.length > 0) {
		$.messager.confirm('提示信息', '您确认要删除吗?', function(data) {
			if (data) {
                var postData={ids:arr.join(',')};
                var vurl = '/treatTest/delete';
				doAjax(postData, vurl, 'POST', true, function(data) {
					$('#treatTestGrid').datagrid('clearSelections');
                    showAllTreatTest();
				});
			}
		});
	} else {
		$.messager.show({
			title : '警告',
			msg : '请先选择要删除的记录。'
		});
	}
}

function closeWindowTreatTest(){
    closeWindow('edit-treatTest-window');
}