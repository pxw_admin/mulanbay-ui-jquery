$(function() {
    $('#kwDays').numberbox('setValue', 365);
    getLeftKeywordsTree();
	getGoodsTypeTree();
	getBuyTypeTree();
    initSearchFormYear();
	initGrid();
});

var dataUrl='/buyRecord/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2Json("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
			field : 'goodsName',
			title : '商品名称',
            width :  400,
            formatter : function(value, row, index) {
				var vv=value;
				if(row.keywords!=null){
					vv="★"+vv;
				}
                if(vv.length>35){
                    return vv.substring(0,35)+'<a href="javascript:showDetail('+'\''+value+'\''+');"> ......</a>';
                }
                return vv;
            },
			sortable : true
		}, {
			field : 'buyType.id',
			title : '购买来源',
			sortable : true,
			formatter : function(value, row, index) {
				return row.buyType.name;
			},
            align : 'center'
        }, {
			field : 'goodsType.id',
			title : '商品类型',
			sortable : true,
			formatter : function(value, row, index) {
				return row.goodsType.name;
			},
            align : 'center'
        }, {
            field : 'subGoodsType.id',
            title : '商品子类型',
            sortable : true,
            formatter : function(value, row, index) {
            	if(row.subGoodsType){
                    return row.subGoodsType.name;
                }else {
            		return '--';
				}
            },
            align : 'center'
        }, {
			field : 'price',
			title : '商品价格',
			sortable : true,
			formatter : function(value, row, index) {
				return formatMoneyWithSymbal(value);
			},
            align : 'center'
        }, {
			field : 'amount',
			title : '数量',
			sortable : true,
            align : 'center'
        }, {
			field : 'shipment',
			title : '运费',
			sortable : true,
			formatter : function(value, row, index) {
				return formatMoneyWithSymbal(value);
			},
            align : 'center'
        }, {
			field : 'totalPrice',
			title : '总价',
			sortable : true,
			formatter : function(value, row, index) {
				return formatMoneyWithSymbal(value);
			},
            align : 'center'
        }, {
			field : 'buyDate',
			title : '购买日期',
			sortable : true
		}, {
            field : 'shopName',
            title : '店铺名称'
		}, {
            field : 'paymentName',
            title : '支付方式'
        }, {
			field : 'consumeDate',
			title : '消费日期',
			sortable : true
		}, {
			field : 'status',
			title : '商品状态',
			sortable : true,
			formatter : function(value, row, index) {
				if (value == 'UNBUY') {
					return '<a href="javascript:change()"><img src="../../static/image/cross.png"></img></a>';
				} else if (value == 'BUY'){
					return '<a href="javascript:change()"><img src="../../static/image/tick.png"></img></a>';
				} else {
					return '<img src="../../static/image/unknown.png"></img>';
				}
			},
			align : 'center'
		}, {
            field : 'consumeTypeName',
            title : '消费方式'
        }, {
			field : 'secondhand',
			title : '是否二手',
			sortable : true,
			formatter : function(value, row, index) {
				if (value == true) {
					return '<img src="../../static/image/tick.png"></img>';
				}else {
					return '--';
				}
			},
			align : 'center'
		}, {
            field : 'statable',
            title : '是否加入统计',
            sortable : true,
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
            id : 'createAsTemplateBtn',
            text : '以此为模板新增',
            iconCls : 'icon-add',
            handler : addFromTemplate
        }, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'statBtn',
			text : '统计',
			iconCls : 'icon-stat',
			handler : stat
		}, '-', {
            id : 'planStatBtn',
            text : '计划执行统计',
            iconCls : 'icon-stat',
            handler : planStat
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function showDetail(msg){
    $('#grid').datagrid('uncheckAll');
    $.messager.alert('详情', msg, 'info');
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    getKeywordsTree();
	var nowDate =getNowDateTimeString();
    var formData = {
        //'goodsTypeId': 1,
        //'buyTypeId': 1,
        amount: 1,
		buyDate:nowDate,
        // consumeDate:nowDate,
        shipment:0,
        payment:'ALIPAY',
        status:'BUY',
        secondhand:false,
        statable : true,
        consumeType:'NORMAL',
        keywords:null
    };
    $('#ff').form('load', formData);
}

function getKeywordsTree(){
	var startDate = getDay(-365);
    $('#hisKeywordsList').combotree({
        url : getFullApiUrl('/buyRecord/getKeywordsTree?needRoot=true&startDate='+startDate),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onSelect:function(node) {
            appendTagBox('keywordsList',node.id);
            //alert(node.id);
        }
    });
}

function addFromTemplate(){
    var rows = getSelectedSingleRow();
    var url='/buyRecord/get?id='+ rows[0].id;
    doAjax(null,url,'GET',false,function(data){
        $('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
        //需要删除id，否则变为修改了
		data.id="";
		//data.goodsName="";
        //data.price=0;
        //data.keywords="";
        //data.brand="";
        data.skuInfo="";
        //data.totalPrice=0;
        var dd = new Date();
        var bt = dd.Format('yyyy-MM-dd hh:mm:ss');
        data.buyDate=bt;
        data.consumeDate="";
        $('#ff').form('load', data);
        $('#goodsTypeList2').combotree('setValue', data.goodsType.id);
        if(data.subGoodsType){
            $('#subGoodsTypeList').combotree('setValue', data.subGoodsType.id);
        }
        $('#buyTypeList2').combotree('setValue', data.buyType.id);
        //设置字符
        $('#secondhandList').combobox('setValue', data.secondhand);
        $("#ordercon").val("asc");
        $('#grid').datagrid('clearSelections');
    });
}

//编辑使用
function loadSubGoodsTypeList(pid){
    combotreeLoad('subGoodsTypeList','/goodsType/getGoodsTypeTree?pid='+pid);
}

//搜索使用
function loadSubGoodsTypeListForSearch(pid){
    combotreeLoad('subGoodsTypeList2','/goodsType/getGoodsTypeTree?pid='+pid);
}

function initForm(){
	$('#goodsTypeList2').combotree({
		url : getFullApiUrl('/goodsType/getGoodsTypeTree?needRoot=true&pid=0'),
		valueField : 'id',
		textField : 'text',
		loadFilter: function(data){
	    	return loadDataFilter(data);
	    },
        onChange : function (newValue, oldValue) {
            loadSubGoodsTypeList(newValue);
        }
	});
	$('#buyTypeList2').combotree({
		url : getFullApiUrl('/buyType/getBuyTypeTree?needRoot=true'),
		valueField : 'id',
		textField : 'text',
		loadFilter: function(data){
	    	return loadDataFilter(data);
	    }
	});
    $('#keywordsList').tagbox({
        url : getFullApiUrl('/buyRecord/getKeywordsTree'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        }
    });
    //loadKeywords();
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/buyRecord/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        //不手动清空会导致上一次的数据还存在
        $('#keywordsList').tagbox({value: null});
        initForm();
        getKeywordsTree();
        $('#ff').form('load', data);
		$('#goodsTypeList2').combotree('setValue', data.goodsType.id);
		if(data.subGoodsType){
            $('#subGoodsTypeList').combotree('setValue', data.subGoodsType.id);
        }
        $('#buyTypeList2').combotree('setValue', data.buyType.id);
		//设置字符
		//$('#secondhandList').combobox('setValue', data.secondhand);
        $("#ordercon").val("asc");
		$('#grid').datagrid('clearSelections');
	});
}

function getGoodsTypeTree(){
	$('#goodsTypeList').combotree({
		url : getFullApiUrl('/goodsType/getGoodsTypeTree?needRoot=true&pid=0'),
		valueField : 'id',
		textField : 'text',
		loadFilter: function(data){
	    	return loadDataFilter(data);
	    },
        onChange : function (newValue, oldValue) {
            loadSubGoodsTypeListForSearch(newValue);
        }
	});
}

function refreshLeftKeywordsTree(){
    getLeftKeywordsTree();
    showAll();
}

function getLeftKeywordsTree(){
    var v = $('#kwDays').numberbox('getValue');
    var startDate = getDay(0-v);
    $('#leftKeywordsList').tree({
        url : getFullApiUrl('/buyRecord/getKeywordsTree?needRoot=true&startDate='+startDate),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onSelect:function(node) {
            var formData = {
                keywords:node.id
            };
            $('#search-window').form('load', formData);
            showAll();
        }
    });
}

function getBuyTypeTree(){
    combotreeLoad('buyTypeList','/buyType/getBuyTypeTree?needRoot=true');
}

function formatMoney(pp){
	pp =pp+0.0;
	return (pp/100).toFixed(2);
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData(continueCreate) {
    //自动设置，原来在增加与修改时设置经常出问题
    var url='/buyRecord/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/buyRecord/create';
    }
	doFormSubmit('ff',url,function(){
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
        if(continueCreate){
        	// 继续增加
            $("#id").val(null);
            $("#goodsName").val(null);
        }else{
            closeWindow('eidt-window');
		}
    });
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/buyRecord/delete';
    commonDeleteByIds(delUrlPrefix);
}

function stat(){
	var para =form2Json("search-window");
	var url='/buyRecord/stat';

	doAjax(para,url,'GET',false,function(data){
		$('#stat-window').window('open');
		var formData = {
			totalcount: data[0].totalCount,
			totalshipment: formatMoneyWithSymbal(data[0].totalShipment),
			mtotalPrice: formatMoneyWithSymbal(data[0].totalPrice),
            totalcountSale: data[1].totalCount,
            totalshipmentSale: formatMoneyWithSymbal(0-data[1].totalShipment),
            mtotalPriceSale: formatMoneyWithSymbal(0-data[1].totalPrice)
	    };
		$('#stat-form').form('load', formData);
	});
}

function loadKeywords(){
    var url='/buyRecord/getLatestKeywords?days=7';
    doAjax(null,url,'GET',false,function(data){
        $("#latestKeywordsList").attr("value",'test');
    });
}

/**
 * 总价由后台计算
 */
function calculatePrice() {
    var price=$('#price').val();
    var shipment =$('#shipment').val();
    var amount =$('#amount').val();
    var totalPrice = (Number(price)*Number(amount)).toFixed(2);
    $('#totalPrice').numberbox('setValue', totalPrice);
}

function planStat(){
	showUserPlanWindow('BuyRecord');
}

function resetSearchBrForm() {
    $('#search-window').form('reset');
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31',
        keywords:null
    };
    $('#search-window').form('load', formData);
    //左边的树重新设置为不选择
    var node = $('#leftKeywordsList').tree('find', "");
    $('#leftKeywordsList').tree('select', node.target);
}

function showNewGoodsType() {
	$('#eidt-goodsType-window').window('open');
	$('#ff-goodsType').form('clear');
	loadParentGoodsTypeList(0);
	$('#parentGoodsTypeList').combotree('setValue', 0);
	var formData = {
		parentId: 0,
		status: 'ENABLE',
		orderIndex:1,
		statable : true
	};
	$('#ff-goodsType').form('load', formData);
}

function showNewSubGoodsType() {
	$('#eidt-goodsType-window').window('open');
	$('#ff-goodsType').form('clear');
	var pid =$('#goodsTypeList2').combotree('getValue');
	loadParentGoodsTypeList(0);
	$('#parentGoodsTypeList').combotree('setValue', pid);
	var formData = {
		parentId: pid,
		status: 'ENABLE',
		orderIndex:1,
		statable : true
	};
	$('#ff-goodsType').form('load', formData);
}

//新增商品类型/子类
function newGoodsType() {
	var url='/goodsType/create';
	doFormSubmit('ff-goodsType',url,function(returnData){
		closeWindow('eidt-goodsType-window');
		var pid =$('#parentGoodsTypeList').combotree('getValue');
		if(pid==0){
			$('#goodsTypeList2').combotree({
				url : getFullApiUrl('/goodsType/getGoodsTypeTree?needRoot=true&pid=0'),
				valueField : 'id',
				textField : 'text',
				loadFilter: function(data){
					return loadDataFilter(data);
				},
				onChange : function (newValue, oldValue) {
					loadSubGoodsTypeList(newValue);
				},
				onLoadSuccess : function (node,data) {
					$('#goodsTypeList2').combotree('setValue', returnData.id);
				}
			});
		}else{
			$('#subGoodsTypeList').combotree({
				url : getFullApiUrl('/goodsType/getGoodsTypeTree?pid='+pid),
				valueField : 'id',
				textField : 'text',
				loadFilter: function(data){
					return loadDataFilter(data);
				},
				onLoadSuccess : function (node,data) {
					$('#subGoodsTypeList').combotree('setValue', returnData.id);
				}
			});
		}

	});
}

function loadParentGoodsTypeList(pid){
	$('#parentGoodsTypeList').combotree({
		url : getFullApiUrl('/goodsType/getGoodsTypeTree?needRoot=true&rootType=SELF&pid='+pid),
		valueField : 'id',
		textField : 'text',
		loadFilter: function(data){
			return loadDataFilter(data);
		}
	});
}

function showNewBuyType() {
	$('#eidt-buyType-window').window('open');
	$('#ff-buyType').form('clear');
	var formData = {
		status: 'ENABLE',
		orderIndex:1
	};
	$('#ff-buyType').form('load', formData);
}

//新增商品类型/子类
function newBuyType() {
	var url='/buyType/create';
	doFormSubmit('ff-buyType',url,function(returnData){
		closeWindow('eidt-buyType-window');
		$('#buyTypeList2').combotree({
			url : getFullApiUrl('/buyType/getBuyTypeTree?needRoot=true'),
			valueField : 'id',
			textField : 'text',
			loadFilter: function(data){
				return loadDataFilter(data);
			},
			onLoadSuccess : function (node,data) {
				$('#buyTypeList2').combotree('setValue', returnData.id);
			}
		});
	});
}
