$(function() {
	initGrid();
});

var dataUrl='/systemFunction/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称',
            formatter : function(value, row, index) {
                var img='';
                if (row.functionDataType == 'M') {
                    img = '<img src="../../static/image/menu.png"></img>';
                }else if (row.functionDataType == 'D') {
                    img = '<img src="../../static/image/filter.png"></img>';
                }else if (row.functionDataType == 'C') {
                    img = '<img src="../../static/image/webpage.png"></img>';
                }else {
                    img = '<img src="../../static/image/function.png"></img>';
                }
                if(row.permissionAuth==true){
                    img+='<img src="../../static/image/auth.png"></img>';
                }
                if(row.secAuth==true){
                    img+='<img src="../../static/image/2.png"></img>';
                }
                return img+value;
            }
        },{
            field : 'parent.name',
            title : '上层功能点名称',
            formatter : function(value, row, index) {
                if (row.parent) {
                    return row.parent.name;
                } else {
                    return '--';
                }
            },
            align : 'center'
        },{
			field : 'urlAddress',
			title : '请求地址',
            align : 'center'
        },{
            field : 'supportMethods',
            title : '支持的请求方式',
            align : 'center'
        },{
            field : 'visible',
            title : '菜单可见',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'router',
            title : '路由',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'urlTypeName',
            title : '请求地址类型',
            align : 'center'
        },{
            field : 'functionTypeName',
            title : '功能类型',
            align : 'center'
        },{
            field : 'functionDataTypeName',
            title : '数据类型',
            align : 'center'
        },{
            field : 'diffUser',
            title : '数据区分用户',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'loginAuth',
            title : '登陆验证',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'permissionAuth',
            title : '权限验证',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'ipAuth',
            title : 'IP验证',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'autoLogin',
            title : '自动登陆',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'requestLimit',
            title : '请求限制',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'requestLimitPeriod',
            title : '请求限制秒数',
            align : 'center'
        },{
            field : 'dayLimit',
            title : '每天限制次数',
            align : 'center'
        },{
            field : 'doLog',
            title : '记录日志',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'triggerStat',
            title : '用户触发',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'rewardPoint',
            title : '积分奖励',
            align : 'center'
        },{
            field : 'recordReturnData',
            title : '记录返回数据',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'groupId',
            title : '分组号',
            align : 'center'
        },{
            field : 'errorCode',
            title : '错误代码',
            align : 'center'
        },{
            field : 'treeStat',
            title : '树形统计',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'secAuth',
            title : '二次认证',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        } ,{
            field : 'orderIndex',
            title : '排序号',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		}, '-', {
            id : 'searchBtn',
            text : '刷新系统缓存',
            iconCls : 'icon-refresh2',
            handler : refreshSystemConfig
        }, '-', {
            id : 'searchBtn',
            text : '自动配置功能点',
            iconCls : 'icon-cmd',
            handler : initUnConfigedFunctions
        }, '-', {
            id : 'treeBtn',
            text : '功能点树',
            iconCls : 'icon-auth',
            handler : showFunctionTree
        } ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var formData = {
        urlType: 'NORMAL',
        functionType: 'CREATE',
        functionDataType: 'F',
        loginAuth: true,
        permissionAuth: false,
        ipAuth: false,
        alwaysShow: false,
        requestLimit: false,
        requestLimitPeriod :5,
        doLog: true,
        triggerStat: false,
        diffUser:true,
        idField:'id',
        idFieldType:'LONG',
        status:'ENABLE',
        orderIndex :0,
        rewardPoint:0,
        groupId:1,
        dayLimit:0,
        treeStat:true,
        recordReturnData:false,
        errorCode:0,
        secAuth:false,
        visible:true,
        router:false
    };
    $('#ff').form('load', formData);
}


function initForm(){
    combotreeLoad('parentSystemFunctionList','/systemFunction/getSystemFunctionMenuTree');
    combotreeLoad('beanNameList','/systemFunction/getDomainClassNamesTree');
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/systemFunction/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
        $('#parentSystemFunctionList').combotree('setValue', data.parentId);
        //设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/systemFunction/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/systemFunction/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/systemFunction/delete';
    commonDeleteByIds(delUrlPrefix);
}

function refreshSystemConfig() {
    var url='/systemFunction/refreshSystemConfig';
    doAjax(null,url,'POST',true,function(data){
    });
}

function initUnConfigedFunctions() {
    var url='/systemFunction/initUnConfigedFunctions';
    doAjax(null,url,'GET',true,function(data){
        reloadDatagrid();
    });
}

function showFunctionTree() {
    $('#functionTree-window').window('open');
    combotreeLoad('functionDataTypeList','/common/getEnumTree?needRoot=false&enumClass=FunctionDataType&idType=ORDINAL');
    getFunctionTree();
}

function getFunctionTree() {
    var para =getSearchParaMulti("functionTree-search-form");
    var url='/systemFunction/getSystemFunctionTree';
    doAjax(para,url,'GET',true,function(data){
        $('#systemFunctionList').tree('loadData',data);
    });
}