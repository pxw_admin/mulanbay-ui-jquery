$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/chartConfig/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称'
        }, {
            field : 'title',
            title : '标题'
        }, {
            field : 'orderIndex',
            title : '排序号',
            align : 'center'
        }, {
            field : 'level',
            title : '等级',
            align : 'center'
        },{
            field : 'status',
            title : '状态',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'relatedBeans',
            title : '关联业务',
            align : 'center'
        },{
            field : 'chartTypeName',
            title : '图表类型',
            align : 'center'
        },{
            field : 'supportDate',
            title : '支持时间查询',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'createdTime',
            title : '创建时间'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function getChartConfigTree(){
    $('#chartConfigList').combotree({
        url : getFullApiUrl('/chartConfig/getChartConfigTree?needRoot=true'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onClick: function(node){
            //加载模板内容
            var url='/chartConfig/get?id='+ node.id;
            doAjax(null,url,'GET',false,function(data){
                $('#ff').form('clear');
                data.id=null;
                $('#ff').form('load', data);
            });
        }
    });
}


function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    getChartConfigTree();
    $('#chartConfigList').combotree('enable');
    var formData = {
        sqlType: 'SQL',
        resultType: 'DATE',
        valueType: 'DAY',
        status:'ENABLE',
        notifyType: 'WARN',
        level:3,
        rewardPoint:0,
        orderIndex :1,
        supportDate:true
    };
    $('#ff').form('load', formData);
    initGridStatValueConfig(0,'NOTIFY');
}

function initForm(){
    combotreeLoad('relatedBeansList','/common/getBussTypeTree');
    combotreeLoad('bussKeyList','/systemFunction/getDomainClassNamesTree');
}

function loadSearchForm(){
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/chartConfig/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
        //getchartConfigTree();
        $('#chartConfigList').combotree('disable');
        //data.para=encodeJsonString(data.para);
        $('#ff').form('load', data);
        initGridStatValueConfig(data.id,'CHART');
        $("#statValueConfigFid").val(data.id);
        //设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/chartConfig/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/chartConfig/create';
    }
    var encodePara={
        para:encodeJsonString($("#para").val())
    };
    $('#ff').form('load', encodePara);
    //$('#relatedBeansList').combobox('setValue',$('#relatedBeansList').combobox('getText') );
    doFormSubmit('ff',url,function(data){
		//closeWindow('eidt-window');
        $('#ff').form('clear');
        $('#ff').form('load', data);
        //设置子表的外键
        $("#statValueConfigFid").val($("#id").val());
        $('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/chartConfig/delete';
    commonDeleteByIds(delUrlPrefix);
}

function showPara() {
    var para = $("#para").val();

    $('#paraDetail-window').window('open');
    $('#paraDetail').treegrid('loadData', {"total":1,"rows":[
            {"id":'根目录',"name":'根目录'}
        ],"footer":[
            {"name":"Total Persons:","persons":7,"iconCls":"icon-sum"}
        ]});
    showFormatTree('paraDetail','根目录',eval('(' + para + ')'));
}