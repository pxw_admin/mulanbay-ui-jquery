//打开用户计划列表
function showUserPlanWindow(beanName) {
    var title ="木兰湾";
    var url = '../plan/userPlanComm.html?beanName='+beanName;
    var content = '<iframe id="vpn" src="' + url + '" width=100%" height="99%" frameborder="0" scrolling="no"></iframe>';
    var boarddiv = '<div id="userPlanWindow" title="' + title + '" data-options="iconCls:\'icon-mulanbay\'"></div>';
    $(document.body).append(boarddiv);
    $('#userPlanWindow').show();
    var win = $('#userPlanWindow').dialog({
        content: content,
        width: 880,
        height: 520,
        modal: false,
        title: title,
        onClose : function() {
            $(this).dialog('destroy');
            $(this).remove();
        }
    });
}

//打开日志修改记录
function showEditLogCompareWindow(beanName,idTag) {
    var id = $('#'+idTag).val();
    var title ="木兰湾";
    var url = '../log/operationLogCompareComm.html?beanName='+beanName+'&id='+id;
    var content = '<iframe id="vpn" src="' + url + '" width=100%" height="99%" frameborder="0" scrolling="no"></iframe>';
    var boarddiv = '<div id="operationLogCompareWindow" title="' + title + '" data-options="iconCls:\'icon-mulanbay\'"></div>';
    $(document.body).append(boarddiv);
    $('#operationLogCompareWindow').show();
    var win = $('#operationLogCompareWindow').dialog({
        content: content,
        width: 1130,
        height: 530,
        modal: false,
        title: title,
        onClose : function() {
            $(this).dialog('destroy');
            $(this).remove();
        }
    });
}


//打开身体情况分析
function showBodyAbnormalRecordAnalyseWindow(name,groupField) {
    var title ="木兰湾";
    var url = '../health/bodyAbnormalRecordAnalyseComm.html?name='+name+'&groupField='+groupField;
    var content = '<iframe id="vpn" src="' + url + '" width=100%" height="99%" frameborder="0" scrolling="no"></iframe>';
    var boarddiv = '<div id="bodyAbnormalRecordAnalyseWindow" title="' + title + '" data-options="iconCls:\'icon-mulanbay\'"></div>';
    $(document.body).append(boarddiv);
    $('#bodyAbnormalRecordAnalyseWindow').show();
    var win = $('#bodyAbnormalRecordAnalyseWindow').dialog({
        content: content,
        width: 880,
        height: 500,
        modal: false,
        title: title,
        onClose : function() {
            $(this).dialog('destroy');
            $(this).remove();
        }
    });
}

//打开用药使用分析
function showCalendarStatDrugDetailWindow(treatDrugId) {
    var title ="木兰湾";
    var url = '../health/treatDrugDetailChartComm.html?treatDrugId='+treatDrugId;
    var content = '<iframe id="vpn" src="' + url + '" width=100%" height="99%" frameborder="0" scrolling="no"></iframe>';
    var boarddiv = '<div id="treatDrugDetailChartWindow" title="' + title + '" data-options="iconCls:\'icon-mulanbay\'"></div>';
    $(document.body).append(boarddiv);
    $('#treatDrugDetailChartWindow').show();
    var win = $('#treatDrugDetailChartWindow').dialog({
        content: content,
        width: 900,
        height: 540,
        modal: false,
        title: title,
        onClose : function() {
            $(this).dialog('destroy');
            $(this).remove();
        }
    });
}

//打开智能客服
function showCustomServiceWindow() {
    var title ="木兰湾";
    var url = '../system/customServiceComm.html';
    var content = '<iframe id="vpn" src="' + url + '" width=100%" height="99%" frameborder="0" scrolling="no"></iframe>';
    var boarddiv = '<div id="customServiceWindow" title="' + title + '" data-options="iconCls:\'icon-mulanbay\'"></div>';
    $(document.body).append(boarddiv);
    $('#customServiceWindow').show();
    var win = $('#customServiceWindow').dialog({
        content: content,
        width: 700,
        height: 475,
        modal: false,
        title: title,
        onClose : function() {
            $(this).dialog('destroy');
            $(this).remove();
        }
    });
}