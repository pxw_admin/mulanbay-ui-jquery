$(function() {
    loadSearchForm();
	initGrid();
    // $('#quitDate').datebox({
    //     onChange: calculateDays
    // });
});

var dataUrl='/qaConfig/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称'
        },{
			field : 'resultTypeName',
			title : '返回类型',
			align : 'center'
		},{
			field : 'handleCode',
			title : '代码',
            align : 'center'
        },{
			field : 'keywords',
			title : '关键字',
			width :  300,
			formatter : function(value, row, index) {
				var vv=value;
				if(vv.length>35){
					return vv.substring(0,35)+'<a href="javascript:showDetail('+'\''+value+'\''+');"> ......</a>';
				}
				return vv;
			},
			align : 'center'
		},{
			field : 'referQaId',
			title : '跳转QA',
			align : 'center'
		},{
			field : 'parentId',
			title : '上级QA',
			align : 'center'
		},{
			field : 'orderIndex',
			title : '排序号',
			align : 'center'
		},{
			field : 'matchCache',
			title : '缓存',
			formatter : function(value, row, index) {
				return getStatusImage(value);
			},
			align : 'center'
		},{
			field : 'status',
			title : '状态',
			formatter : function(value, row, index) {
				return getStatusImage(value);
			},
			align : 'center'
		},{
			field : 'createdTime',
			title : '创建时间',
			align : 'center'
		} ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		}, '-', {
			id : 'reloadBtn',
			text : '刷新缓存',
			iconCls : 'icon-refresh',
			handler : reloadCache
		}, '-', {
			id : 'csBtn',
			text : '智能客服',
			iconCls : 'icon-cs',
			handler : showCs
		},'-', {
			id : 'treeView',
			text : '拓扑结构',
			iconCls : 'icon-stat',
			handler : showTreeView
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
	var formData = {
		resultType: 'DIRECT',
		orderIndex: 0,
		status:'ENABLE',
		matchCache:true
	};
	$('#ff').form('load', formData);
}

function initForm(){
	combotreeLoad('resultTypeList','/common/getEnumTree?enumClass=QaResultType');
	combotreeLoad('referQaList','/qaConfig/getQaConfigTree?needRoot=true');
	combotreeLoad('parentQaList','/qaConfig/getQaConfigTree?needRoot=true');

}
function loadSearchForm() {

}
function edit() {
	var rows = getSelectedSingleRow();
	var url='/qaConfig/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    //自动设置，原来在增加与修改时设置经常出问题
    var url='/qaConfig/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/qaConfig/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/qaConfig/delete';
    commonDeleteByIds(delUrlPrefix);
}

function showCs() {
	showCustomServiceWindow();
}

function reloadCache() {
	var vurl = '/qaConfig/reloadCache';
	doAjax(null, vurl, 'POST', true, function(data) {
	});
}
function showDetail(msg){
	$('#grid').datagrid('uncheckAll');
	$.messager.alert('详情', msg, 'info');
}
function showTreeView() {
	$('#qaConfig-treeView-window').window('open');
	var url='/qaConfig/treeView';
	doAjax(null,url,'GET',false,function(data){
		createTreeChartEnhanced(data.data,'treeViewContainer');
	});
}