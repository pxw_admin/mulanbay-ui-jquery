$(function() {
    loadSearchForm();
    initGrid();
    // $('#weight').numberbox({
    //     onChange: calculateBmi
    // });
});

var dataUrl='/bodyBasicInfo/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'recordDate',
            title : '采集时间'
        },{
			field : 'weight',
			title : '体重(公斤)',
            align : 'center'
        } ,{
            field : 'height',
            title : '身高(厘米)',
            sortable : true,
            align : 'center'
        },{
            field : 'bmi',
            title : 'BMI指数',
            sortable : true,
            formatter : function(value, row, index) {
                if (value <=18.4) {
                    return '<font color="#bc8f8f">'+value+'(偏瘦)</font>';
                }else if(value >=18.5&&value<=23.9){
                    return '<font color="green">'+value+'(正常)</font>';
                }else if(value >=24&&value<=27.9){
                    return '<font color="yellow">'+value+'(过重)</font>';
                } else {
                    return '<font color="red">'+value+'(肥胖)</font>';
                }
            },
            align : 'center'
        }, {
			field : 'aa',
			title : '采集时间',
			formatter : function(value, row, index) {
				var hs= row.recordDate.substr(11,2);
				var h =parseInt(hs);
				if (0<=h&&h<11) {
					return '上午';
				}else if(11<=h&&h<12){
					return '中午';
				}else if(12<h&&h<=17){
					return '下午';
				}else {
					return '晚上';
				}
			},
			align : 'center'
		}, {
			field : 'createdTime',
			title : '创建时间'
		} ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} , '-', {
			id : 'stBtn',
			text : 'BMI标准',
			iconCls : 'icon-stat',
			handler : bmiStandardInfo
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var nowDate =getNowDateTimeString();
    var formData = {
        recordDate:nowDate
    };
    $('#ff').form('load', formData);
}

function initForm(){
}

function loadSearchForm() {

}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/bodyBasicInfo/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/bodyBasicInfo/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/bodyBasicInfo/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/bodyBasicInfo/delete';
    commonDeleteByIds(delUrlPrefix);
}

function calculateBmi(){
    var height=$('#height').val()/100.0;
    var weight =$('#weight').val();
    var bmi= weight/(height*height);
    $('#bmi').numberbox('setValue', bmi);
}

function bmiStandardInfo() {
	$('#bmiStandard-window').window('open');
	$('#bmiStandardTb').datagrid({
		url: '/bodyBasicInfo/get',
		showGroup: true,
		scrollbarSize: 0,
		columns:[[
			{field:'type',title:'分类',width:80,align:'center'},
			{field:'standard',title:'BMI标准',width:150,align:'center'}
		]]
	});
	//清空所有数据
	$('#bmiStandardTb').datagrid('loadData', { total: 0, rows: [] });
	$('#bmiStandardTb').datagrid('appendRow',{
		type:'<font color="gray">偏瘦</font>',
		standard:'<font color="gray"><=18.4</font>'
	});
	$('#bmiStandardTb').datagrid('appendRow',{
		type:'<font color="green">正常</font>',
		standard:'<font color="green">18.5~23.9</font>'
	});
	$('#bmiStandardTb').datagrid('appendRow',{
		type:'<font color="red">过胖</font>',
		standard:'<font color="red">24.0~27.9</font>'
	});
	$('#bmiStandardTb').datagrid('appendRow',{
		type:'<font color="#8b008b">肥胖</font>',
		standard:'<font color="#8b008b">>=28</font>'
	});
}
