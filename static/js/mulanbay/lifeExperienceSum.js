$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/lifeExperienceSum/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'year',
            title : '年份'
        }, {
            field : 'totalDays',
            title : '总天数',
            align : 'center'
        }, {
            field : 'workDays',
            title : '工作天数',
            formatter : function(value, row, index) {
                if(value<300){
                    return '<font color="red">'+value+'</font>';
                }else{
                    return value;
                }
            },
            align : 'center'
        },{
            field : 'travelDays',
            title : '旅行天数',
            sortable : true,
            align : 'center'
        },{
            field : 'studyDays',
            title : '学习天数',
            sortable : true,
            align : 'center'
        },{
            field : 'aa',
            title : '修正',
            formatter : function(value, row, index) {
                return '<a href="javascript:revise('+row.id+');"><img src="../../static/image/sum.png"></img></a>';;
            },
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
            id : 'deleteBtn',
            text : '删除',
            iconCls : 'icon-remove',
            handler : del
        }, '-', {
            id : 'analyseBtn',
            text : '分析',
            iconCls : 'icon-stat',
            handler : showAnalyse
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
}

function initForm(){
}

function loadSearchForm(){
}


function edit() {
	var rows = getSelectedSingleRow();
	var url='/lifeExperienceSum/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/lifeExperienceSum/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/lifeExperienceSum/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/lifeExperienceSum/delete';
    commonDeleteByIds(delUrlPrefix);
}


function showAnalyse() {
    $('#analyse-window').window('open');
    analyse();
}

function analyse() {
    var para =form2Json("analyse-search-form");
    var url='/lifeExperienceSum/analyse';

    doAjax(para,url,'GET',false,function(data){
        $('#analyse-stat-form').form('clear');
        createShadowDataEnhanced(data.chartShadowData,'shadowContainer');
        createPieDataEnhanced(data.chartPieData,'pieContainer');
    });
}

//
function createShadowDataEnhanced(data,containId){
    var series =new Array();
    for(var i=0;i<data.series.length;i++){
    	var serie = {
            name:data.series[i].name,
            type:'line',
            stack: data.series[i].stack,
            areaStyle: {normal: {}},
            data: data.series[i].data
        };
        series.push(serie);
    }
    var option = {
        title: {
            text: '人生经历时间统计'
        },
        tooltip : {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            }
        },
        legend: {
            data:data.legendData
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                data : data.yaxisData
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series: series
    };
    createChartEnhanced(option,containId);
}

function revise(id) {
    $('#grid').datagrid('clearSelections');
    var msg="是否根据当年的公司在职记录、旅行记录修正数据？";
    $.messager.confirm('提示信息', msg, function(data) {
        if (data) {
            var para ={
                id:id
            };
            var vurl='/lifeExperienceSum/revise';
            doAjax(para, vurl, 'POST', false, function(data) {
                var ss ='向<'+data.year+'年>记录中更新了'+data.nc+'条公司信息,其中无效'+data.unc+'条,更新了'+data.nle+'条人生经历信息,其中无效'+data.unle+'条!';
                $.messager.alert('提示', ss, 'info');
                reloadDatagrid();
            });
        }else{
            $('#grid').datagrid('clearSelections');
        }
    });
}