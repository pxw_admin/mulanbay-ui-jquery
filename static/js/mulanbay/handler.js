$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/handler/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
        },
        onLoadError: loadDataError,
		columns : [ [
		    {
            field: 'id',
            title: '序号',
            formatter: function (value, row, index) {
                return index+1;
            },
            align: 'center'
        },{
            field : 'handlerName',
            title : '名称'
        }, {
            field : 'className',
            title : '类名'
        }, {
            field : 'hash',
            title : 'HASH值'
        }, {
            field : 'aaa',
            title : '详情',
            formatter : function(value, row, index) {
                return '<a href="javascript:showHandlerInfo(\''+row.className+'\');"><img src="../../static/image/info.png"></img></a>';
            },
            align : 'center'
        }, {
            field : 'bbb',
            title : '发送命令',
            formatter : function(value, row, index) {
                return '<a href="javascript:showCmd(\''+row.className+'\');"><img src="../../static/image/cmd.png"></img></a>';
            },
            align : 'center'
        }, {
            field : 'ccc',
            title : '自检',
            formatter : function(value, row, index) {
                return '<a href="javascript:check(\''+row.className+'\');"><img src="../../static/image/cmd.png"></img></a>';
            },
            align : 'center'
        }, {
            field : 'ddd',
            title : '重新加载',
            formatter : function(value, row, index) {
                return '<a href="javascript:reload(\''+row.className+'\');"><img src="../../static/image/cmd.png"></img></a>';
            },
            align : 'center'
        }] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function loadSearchForm() {

}

function showAll() {
    refreshDatagrid(dataUrl,1,true);
}

function showHandlerInfo(className) {
    $('#handlerInfoTb').datagrid({
        url: '/handler/aa',
        showGroup: true,
        scrollbarSize: 0,
        columns:[[
            {field:'key',title:'项目',width:200,align:'center'},
            {field:'value',title:'值',width:250,align:'center'}
        ]]
    });
    //清空所有数据
    $('#handlerInfoTb').datagrid('loadData', { total: 0, rows: [] });
    var url='/handler/getHandlerInfo?className='+className;
    doAjax(null,url,'GET',false,function(data){
        if(data.details){
            $('#handlerInfo-window').window('open');
            var ti = data.details;
            for (var i = 0; i < ti.length; i++) {
                $('#handlerInfoTb').datagrid('appendRow',{
                    key:ti[i].key,
                    value:ti[i].value
                });
            }
        }else{
            showInfoMsg("无详细信息");
        }

    });
    $('#grid').datagrid('clearSelections');
}

function showCmd(className) {
    $('#cmdTb').datagrid({
        url: '/handler/aa',
        showGroup: true,
        scrollbarSize: 0,
        columns:[[
            {field:'name',title:'命令名称',width:150,align:'center'},
            {field:'cmd',title:'命令代码',width:150,align:'center'},
            {field:'send',title:'发送',width:150,align:'center'}
        ]]
    });
    //清空所有数据
    $('#cmdTb').datagrid('loadData', { total: 0, rows: [] });
    var url='/handler/getSupportCmd?className='+className;
    doAjax(null,url,'GET',false,function(data){
        if(data){
            $('#cmd-window').window('open');
            var ti = data;
            for (var i = 0; i < ti.length; i++) {
                $('#cmdTb').datagrid('appendRow',{
                    name:ti[i].name,
                    cmd:ti[i].cmd,
                    send:'<a href="javascript:sendCmd(\''+className+'\',\''+ti[i].cmd+'\');">发送</a>'
                });
            }
        }else{
            showInfoMsg("该处理器不支持命令");
        }

    });
    $('#grid').datagrid('clearSelections');
}

function sendCmd(className,cmd){
    $.messager.confirm('提示信息', '是否要发送该命令', function(data) {
        if (data) {
            var url='/handler/handCmd';
            var postData={
                className:className,
                cmd:cmd
            };
            doAjax(postData,url,'POST',true,function(data){
                showInfoMsg('执行结果:code='+data.code+",msg="+data.message);
            });
        }else{
        }
    });

}

//检查
function check(className){
    $('#grid').datagrid('clearSelections');
    var para = {
        className:className
    };
    var url='/handler/check';
    doAjax(para,url,'POST',false,function(data){
        var mm='';
        if(data==true){
            mm='自检成功';
        }else{
            mm='自检失败';
        }
        showInfoMsg('执行结果:'+mm);
    });
}

//检查
function reload(className){
    $('#grid').datagrid('clearSelections');
    var para = {
        className:className
    };
    var url='/handler/reload';
    doAjax(para,url,'POST',false,function(data){
        var mm='';
        if(data==true){
            mm='重新加载成功';
        }else{
            mm='重新加载失败';
        }
        showInfoMsg('执行结果:'+mm);
    });
}
