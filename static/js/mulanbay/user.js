$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/user/getData';

function initGrid(){
    $('#grid').datagrid({
        iconCls : 'icon-save',
        url : getFullApiUrl(dataUrl),
        method:'GET',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        idField : 'id',
        loadMsg : '正在加载数据...',
        pageSize : 30,
        queryParams: form2Json("search-window"),
        remoteSort : false,
        frozenColumns : [ [ {
            field : 'ID',
            checkbox : true
        } ] ],
        onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
        columns : [ [ {
            field : 'id',
            title : '用户编号',
            sortable : true,
            align : 'center'
        }, {
            field : 'username',
            title : '用户名'
        }, {
            field : 'nickname',
            title : '昵称'
        },{
            field : 'phone',
            title : '手机',
            align : 'center'
        }  ,{
            field : 'level',
            title : '等级',
            align : 'center'
        } ,{
            field : 'points',
            title : '积分',
            align : 'center'
        },{
            field : 'email',
            title : '邮箱',
            align : 'center'
        },{
            field : 'status',
            title : '状态',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'aa',
            title : '权限授权',
            formatter : function(value, row, index) {
                return '<a href="javascript:openRoleAuth('+row.id+');"><img src="../../static/image/auth.png"></img></a>';;
            },
            align : 'center'
        },{
            field : 'bb',
            title : '监控授权',
            formatter : function(value, row, index) {
                return '<a href="javascript:openSystemMonitorAuth('+row.id+');"><img src="../../static/image/auth.png"></img></a>';;
            },
            align : 'center'
        },{
            field : 'cc',
            title : '微信绑定',
            formatter : function(value, row, index) {
                return '<a href="javascript:openUserWxpayInfo('+row.id+');"><img src="../../static/image/wechat.png"></img></a>';;
            },
            align : 'center'
        } ,{
            field : 'expireTime',
            title : '过期时间',
            align : 'center'
        },{
            field : 'secAuthTypeName',
            title : '二次授权',
            align : 'center'
        }  ,{
            field : 'lastLoginTime',
            title : '最后登陆时间',
            align : 'center'
        } ,{
            field : 'lastLoginIp',
            title : '最后登陆IP',
            align : 'center'
        } ] ],
        pagination : true,
        rownumbers : true,
        singleSelect : false,
        toolbar : [ {
            id : 'createBtn',
            text : '新增',
            iconCls : 'icon-add',
            handler : add
        }, '-', {
            id : 'editBtn',
            text : '修改',
            iconCls : 'icon-edit',
            handler : edit
        }, '-', {
            id : 'deleteBtn',
            text : '删除',
            iconCls : 'icon-remove',
            handler : del
        }, '-', {
            id : 'logoutBtn',
            text : '注销',
            iconCls : 'icon-logout',
            handler : logout
        }, '-', {
            id : 'deleteUDBtn',
            text : '格式化数据',
            iconCls : 'icon-format',
            handler : deleteUserData
        }, '-', {
            id : 'initUDBtn',
            text : '初始化数据',
            iconCls : 'icon-init',
            handler : initUserData
        }, '-', {
            id : 'searchBtn',
            text : '刷新',
            iconCls : 'icon-refresh',
            handler : showAll
        } ]
    });
}
function deleteUserData() {
    var rows = getSelectedSingleRow();
    $.messager.confirm('提示信息', '您确认要删除该用户的所有数据?', function(data) {
        if (data) {
            var postData ={
                userId:rows[0].id
            };
            var url='/user/deleteUserData';
            doAjax(postData,url,'POST',true,function (data) {
                $('#grid').datagrid('clearSelections');
            });
        }
    });
}
function initUserData() {
    var rows = getSelectedSingleRow();
    $.messager.confirm('提示信息', '您确认要初始化该用户的所有数据?', function(data) {
        if (data) {
            var postData ={
                userId:rows[0].id
            };
            var url='/user/initUserData';
            doAjax(postData,url,'POST',true,function (data) {
                $('#grid').datagrid('clearSelections');
            });
        }
    });
}
function logout(){
    var rows = getSelectedSingleRow();
    $.messager.confirm('提示信息', '您确认要让该用户注销吗?', function(data) {
        if (data) {
            var postData ={
                userId:rows[0].id
            };
            var url='/user/offline';
            doAjax(postData,url,'POST',true,function (data) {
                $('#grid').datagrid('clearSelections');
            });
        }
    });
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
}

function initForm(){

}

function loadSearchForm(){
}

function edit() {
    var rows = getSelectedSingleRow();
    var url='/user/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
        $("#password").textbox('setValue',null);
        //设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
    refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/user/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/user/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/user/delete';
    commonDeleteByIds(delUrlPrefix);
}

function openRoleAuth(userId) {
    $('#grid').datagrid('clearSelections');
    $('#eidt-role-window').window('open');
    $("#roleUserId").val(userId);
    $('#userRoleList').tree({
        url : getFullApiUrl('/user/getUserRoleTree?needRoot=true&userId='+userId),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        }
    });
}

function saveUserRole() {
    var userId =  $("#roleUserId").val();
    var nodes = $('#userRoleList').tree('getChecked');
    var dd=new Array();
    for(var i=0; i<nodes.length; i++){
        var id = nodes[i].id;
        if (id!=undefined&&id!=null&&id!= ''){
            dd.push(id);
        }
    }
    var postData ={
        userId:userId,
        roleIds:dd.join(',')
    };
    var url='/role/saveUserRole';
    doAjax(postData,url,'POST',true,function (data) {
        closeWindow('eidt-role-window');
    });
}


function openSystemMonitorAuth(userId) {
    $('#grid').datagrid('clearSelections');
    $('#eidt-systemMonitor-window').window('open');
    $("#monitorUserId").val(userId);
    $('#systemMonitorList').tree({
        url : getFullApiUrl('/user/getSystemMonitorTree?needRoot=true&userId='+userId),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        }
    });
}

function saveSystemMonitor() {
    var userId =  $("#monitorUserId").val();
    var nodes = $('#systemMonitorList').tree('getChecked');
    var dd=new Array();
    var bussTypes=null;
    for(var i=0; i<nodes.length; i++){
        var id = nodes[i].id;
        if (id!=undefined&&id!=null&&id!= ''){
            dd.push(id);
        }
        if(id==0||id=='0'){
            //选中了全部就直接取第一个根
            bussTypes='0';
        }
    }
    if(bussTypes==null){
        bussTypes = dd.join(',');
    }
    var postData ={
        userId:userId,
        bussTypes:bussTypes
    };
    var url='/user/saveSystemMonitor';
    doAjax(postData,url,'POST',true,function (data) {
        closeWindow('eidt-systemMonitor-window');
    });
}

function openUserWxpayInfo(userId) {
    $('#grid').datagrid('clearSelections');
    var url='/user/getUserWxpayInfo?userId='+userId;
    doAjax(null,url,'GET',false,function(data){
        $('#wxpayInfo-window').window('open');
        $('#wxpayInfo-form').form('clear');
        $('#wxpayInfo-form').form('load', data);
    });
}

function saveWxpayInfo() {
    var postData = form2Json('wxpayInfo-form');
    var url='/user/editUserWxpayInfo';
    doAjax(postData,url,'POST',true,function (data) {
        closeWindow('wxpayInfo-window');
    });
}