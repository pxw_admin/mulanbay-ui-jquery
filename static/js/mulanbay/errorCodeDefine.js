$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/errorCodeDefine/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'code',
			title : '错误代码',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称'
        },{
			field : 'level',
			title : '级别',
            formatter : function(value, row, index) {
                if (value =='NORMAL') {
                    return '<font color="black">'+row.levelName+'</font>';
                }else if(value =='WARNING'){
                    return '<font color="#9acd32">'+row.levelName+'</font>';
                }else if(value =='ERROR'){
                    return '<font color="red">'+row.levelName+'</font>';
                }else if(value =='FATAL'){
                    return '<font color="#8b008b">'+row.levelName+'</font>';
                }else {
                    return value;
                }
            },
            align : 'center'
        } ,{
            field : 'notifiable',
            title : '是否提醒',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'realtimeNotify',
            title : '实时提醒',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'loggable',
            title : '记录日志',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        }, {
            field : 'bussTypeName',
            title : '系统分类',
            align : 'center'
        }, {
            field : 'count',
            title : '累计次数',
            align : 'center'
        }, {
            field : 'limitPeriod',
            title : '限流秒数',
            align : 'center'
        },{
            field : 'createdTime',
            title : '创建时间',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		}, '-', {
            id : 'reloadCacheConfigBtn',
            text : '刷新缓存',
            iconCls : 'icon-refresh',
            handler : reloadCacheConfig
        } ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var formData = {
        level:'NORMAL',
        bussType:'SYSTEM',
        realtimeNotify:false,
        notifiable:true,
        loggable:true,
        limitPeriod:0
    };
    $('#ff').form('load', formData);
}

function initForm(){
}

function loadSearchForm(){
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/errorCodeDefine/get?code='+ rows[0].code;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/errorCodeDefine/edit';
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/errorCodeDefine/delete';
    commonDeleteByIds(delUrlPrefix);
}

//重新加载内存
function reloadCacheConfig(){
    var url='/errorCodeDefine/reloadCacheConfig';
    doAjax(null,url,'POST',true,function(){

    });
}
