$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/databaseClean/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称'
        }, {
            field : 'tableName',
            title : '表名',
            formatter : function(value, row, index) {
                return '<a href="javascript:getCounts('+'\''+row.id+'\''+');"> '+value+'</a>';
            }
        }, {
            field : 'dateField',
            title : '时间字段'
        }, {
            field : 'days',
            title : '保留天数',
            formatter : function(value, row, index) {
                return '<a href="javascript:showManualClean('+row.days+','+row.id+');"> '+value+'</a>';
            },
            align : 'center'
        }, {
            field : 'cleanTypeName',
            title : '删除类型',
            align : 'center'
        }, {
            field : 'extraCondition',
            title : '含附加条件',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        }, {
            field : 'aaa',
            title : '附加操作',
            formatter : function(value, row, index) {
                return '<a href="javascript:truncateTable('+'\''+row.id+'\''+');">清空数据</a>';
            },
            align : 'center'
        }, {
            field : 'lastCleanTime',
            title : '最后一次更新时间'
        }, {
            field : 'lastCleanCounts',
            title : '最后一次更新条数',
            align : 'center'
        },{
			field : 'status',
			title : '状态',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        } ,{
            field : 'orderIndex',
            title : '排序号',
            sortable : true,
            align : 'center'
        }, {
            field : 'createdTime',
            title : '创建时间'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var formData = {
        days: 7,
        cleanType:'DATE_COMPARE',
        status:'ENABLE'
    };
    $('#ff').form('load', formData);
}

function initForm(){
}

function loadSearchForm() {

}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/databaseClean/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showManualClean(days,id) {
    $('#grid').datagrid('clearSelections');
    $('#manual-clean-window').window('open');
    var formData = {
        days: days,
        id:id
    };
    $('#manual-clean-form').form('load', formData);
}

function manualClean() {
    $.messager.confirm('提示信息', '是否要手动执行清理?', function(data) {
        if (data) {
            var para =form2Json("manual-clean-form");
            var url='/databaseClean/manualClean';
            doAjax(para,url,'POST',false,function(data){
                showInfoMsg('一共清理了'+data+'条数据');
                reloadDatagrid();
            });
        }
    });
    $('#manual-clean-window').window('close');
}

function truncateTable(id) {
    $.messager.confirm('提示信息', '是否要清空该表数据', function(data) {
        if (data) {
            var url='/databaseClean/truncate?id='+id;
            doAjax(null,url,'GET',true,function(data){
                reloadDatagrid();
            });
        }
    });
    $('#grid').datagrid('clearSelections');
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/databaseClean/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/databaseClean/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/databaseClean/delete';
    commonDeleteByIds(delUrlPrefix);
}

function getCounts(id) {
    var url='/databaseClean/getCounts?id='+id;
    doAjax(null,url,'GET',false,function(data){
        showInfoMsg('当前表中总记录数:'+data+'条');
    });
    $('#grid').datagrid('clearSelections');
}