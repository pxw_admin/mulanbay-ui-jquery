$(function() {
	initGrid();
});

var dataUrl='/taskServer/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
        onLoadError: loadDataError,
		columns : [ [
		{
            field : 'id',
            title : '序号',
            align : 'center'
        }, {
			field : 'deployId',
			title : '节点',
			sortable : true,
			align : 'center'
		},{
            field : 'ipAddress',
            title : 'IP地址',
            align : 'center'
        },{
            field : 'status',
            title : '状态',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'supportDistri',
            title : '支持分布式',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
			field : 'cejc',
			title : '正在运行job数',
            align : 'center'
        },{
            field : 'sjc',
            title : '已被调度job数',
            align : 'center'
        },{
            field : 'startTime',
            title : '启动时间',
            align : 'center'
        },{
            field : 'lastUpdateTime',
            title : '最后更新时间',
            formatter : function(value, row, index) {
                if(value==null){
                    return '--';
                }else{
                    var ss = tillNowSeconds(null,value);
                    if(ss<=-180){
                        return '<font color="red">'+value+'</font>';
                    }else{
                        return value;
                    }
                }
            },
            align : 'center'
        },{
            field : 'shutdownTime',
            title : '停止时间',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}