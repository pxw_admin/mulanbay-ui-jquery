
var dataUrlDrugDetail='/treatDrugDetail/getData';

function initGridDrugDetail(){
	$('#gridDrugDetail').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrlDrugDetail),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2Json("search-drugDetail-form"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#gridDrugDetail').datagrid('uncheckAll');
            $('#gridDrugDetail').datagrid('checkRow', rowIndex);
            editDrugDetail();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'occurTime',
            title : '用药时间',
            align : 'center'
        }] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtnDrugDetail',
			text : '新增',
			iconCls : 'icon-add',
			handler : addDrugDetail
		}, '-', {
            id : 'createBtnDrugDetailQuick',
            text : '快速新增',
            iconCls : 'icon-add',
            handler : addDrugDetailQuick
        }, '-', {
			id : 'editBtnDrugDetail',
			text : '修改',
			iconCls : 'icon-edit',
			handler : editDrugDetail
		}, '-', {
			id : 'deleteBtnDrugDetail',
			text : '删除',
			iconCls : 'icon-remove',
			handler : delDrugDetail
		}, '-', {
			id : 'searchBtnDrugDetail',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAllDrugDetail
		}, '-', {
            id : 'statBtnDrugDetail',
            text : '用药统计',
            iconCls : 'icon-stat',
            handler : openCalendarStatDrugDetail
        } ]
	});
}

function addDrugDetail() {
    if(!checkTreatDrugId()){
        $.messager.alert('提示', '请先保存药品记录!', 'error');
        return;
    }
	$('#eidt-window-drugDetail').window('open');
	$('#ff-drugDetail').form('clear');
	$('#ff-drugDetail').form.url='/treatDrugDetail/create';
	//外键值为外面看病记录的表单的id
    $("#fkTreatDrugId").val($("#treatDrugId").val());
}

function addDrugDetailQuick() {
    if(!checkTreatDrugId()){
        $.messager.alert('提示', '请先保存药品记录!', 'error');
        return;
    }
    $('#ff-drugDetail').form('clear');
    var postData = {
        treatDrugId: $("#treatDrugId").val(),
        occurTime: getNowDateTimeString()
    };
    $('#ff-drugDetail').form('load', postData);
    $('#ff-drugDetail').form.url='/treatDrugDetail/create';
    saveDataDrugDetail();
}

function initDrugDetailForm() {

}
function editDrugDetail() {
    if(!checkTreatDrugId()){
        $.messager.alert('提示', '请先保存药品记录!', 'error');
        return;
    }
    var rows = $('#gridDrugDetail').datagrid('getSelections');
    var num = rows.length;
    if (num == 0) {
        $.messager.alert('提示', '请选择一条记录进行操作!', 'info');
        return;
    }
	var url='/treatDrugDetail/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window-drugDetail').window('open');
        $('#ff-drugDetail').form('clear');
        $('#ff-drugDetail').form('load', data);
		$('#ff-drugDetail').form.url='/treatDrugDetail/edit?id=' + data.id;
		//设置字符
		$('#gridDrugDetail').datagrid('clearSelections');
        $("#fkTreatDrugId").val($("#treatDrugId").val());
    });
}

function showAllDrugDetail() {
    var vurl =dataUrlDrugDetail;
    $('#gridDrugDetail').datagrid({
        url : getFullApiUrl(vurl),
        type : 'GET',
        queryParams: form2Json("search-drugDetail-form"),
        pageNumber : 1,
        onLoadError : function() {
            $.messager.alert('错误', '加载数据异常！', 'error');
        }
    });
}

function checkTreatDrugId() {
    var id = $("#treatDrugId").val();
    if(id==null||id==0){
        return false;
    }else {
        return true;
    }
}


function saveDataDrugDetail() {
    if(!checkTreatDrugId()){
        $.messager.alert('提示', '请先保存药品记录!', 'error');
        return;
    }
    var url='/treatDrugDetail/edit';
    if($("#treatDrugDetailId").val()==null||$("#treatDrugDetailId").val()==''){
        url='/treatDrugDetail/create';
    }
    doFormSubmit('ff-drugDetail',url,function(data){
		closeWindow('eidt-window-drugDetail');
		$('#gridDrugDetail').datagrid('clearSelections');
        showAllDrugDetail();
        $('#ff-drugDetail').form('clear');
        $('#ff-drugDetail').form('load', data);
        $("#fkTreatDrugId").val(data.id);
	});
}

function getSelectedIdsDrugDetail() {
	var ids = [];
    var rows = $('#gridDrugDetail').datagrid('getSelections');
    var num = rows.length;
    if (num == 0) {
        $.messager.alert('提示', '请选择一条记录进行操作!', 'info');
        return;
    }
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function delDrugDetail() {
	var arr = getSelectedIdsDrugDetail();
	if (arr.length > 0) {
		$.messager.confirm('提示信息', '您确认要删除吗?', function(data) {
			if (data) {
                var postData={ids:arr.join(',')};
                var vurl = '/treatDrugDetail/delete';
				doAjax(postData, vurl, 'POST', true, function(data) {
					$('#gridDrugDetail').datagrid('clearSelections');
                    showAllDrugDetail();
				});
			}
		});
	} else {
		$.messager.show({
			title : '警告',
			msg : '请先选择要删除的记录。'
		});
	}
}

function closeWindowDrugDetail(){
    closeWindow('eidt-window-drugDetail');
}

function openCalendarStatDrugDetail() {
    var treatDrugId = $("#treatDrugId").val();
    showCalendarStatDrugDetailWindow(treatDrugId);
}