$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/userShares/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '股票名称'
        }, {
            field : 'code',
            title : '股票代码'
        }, {
            field : 'shares',
            title : '买入股数',
            sortable : true,
            align : 'center'
        }, {
            field : 'buyPrice',
            title : '买入价格',
            sortable : true,
            align : 'center'
        }, {
            field : 'currentPrice',
            title : '当前价格',
            sortable : true,
            formatter : function(value, row, index) {
                if(value<=0){
                    return '--';
                }else{
                    var ss='';
                    if (row.buyPrice-row.currentPrice >0) {
                        ss = '<font color="green">'+value+'</font>';
                    }else {
                        ss = '<font color="red">'+value+'</font>';
                    }
                    var gfn = row.priceGetFromNow;
                    if(gfn>600){
                        //10分钟之前
                        if(gfn<=3600){
                            ss+='('+(gfn/60).toFixed(0)+'m)';
                        }else if(gfn<=24*3600){
                            ss+='('+(gfn/3600).toFixed(0)+'h)';
                        }else {
                            ss+='('+(gfn/3600/24).toFixed(0)+'d)';;
                        }
                    }
                    return ss;
                }
            },
            align : 'center'
        }, {
            field : 'a1',
            title : '价格涨跌',
            formatter : function(value, row, index) {
                var pp = (row.currentPrice-row.buyPrice).toFixed(2);
                var perc = getPercentWithSambol(pp,row.buyPrice);
                if (pp <0) {
                    return '<font color="green">'+pp+'('+perc+')</font>';
                }else {
                    return '<font color="red">+'+pp+'(+'+perc+')</font>';
                }
            },
            align : 'center'
        }, {
            field : 'a2',
            title : '收益',
            formatter : function(value, row, index) {
                var pp = ((row.currentPrice-row.buyPrice)*row.shares).toFixed(2);
                if (pp <0) {
                    return '<font color="green">'+pp+'</font>';
                }else {
                    return '<font color="red">+'+pp+'</font>';
                }
            },
            align : 'center'
        }, {
            field : 'hisMinPrice',
            title : '历史最低价',
            formatter : function(value, row, index) {
                if(row.smb){
                    var mp = row.smb.minPrice;
                    var ss='';
                    if(mp<=row.minPrice){
                        //达到止损价格
                        ss='★';
                    }
                    if (mp <row.buyPrice) {
                        return '<font color="green">'+mp+'</font>'+ss;
                    }else {
                        return mp+ss;
                    }
                }else {
                    return '--';
                }
            },
            align : 'center'
        }, {
            field : 'hisMaxPrice',
            title : '历史最高价',
            formatter : function(value, row, index) {
                if(row.smb){
                    var mp = row.smb.maxPrice;
                    var ss='';
                    if(mp>=row.maxPrice){
                        //达到最高抛售价格
                        ss='★';
                    }
                    return mp+ss;
                }else {
                    return '--';
                }
            },
            align : 'center'
        },{
            field : 'smb',
            title : '价格变化',
            formatter : function(value, row, index) {
                if(value==null){
                    return '--';
                }else{
                    var ss = '<font color="green">跌</font>'+value.fails+'次,<font color="red">涨</font>'+value.gains+'次';
                    if(value.count>0){
                        if(value.type=='FAIL'){
                            ss+='(<font color="green">连续'+value.count+'次下跌</font>)';
                        }else{
                            ss+='(<font color="red">连续'+value.count+'次上涨</font>)';
                        }
                    }
                    return ss;
                }
            },
            align : 'center'
        }, {
            field : 'minPrice',
            title : '最低止损价格',
            formatter : function(value, row, index) {
                var pp = row.currentPrice-row.minPrice;
                if (pp <=0) {
                    return '<font color="red">'+value+'(可抛售)</font>';
                }else {
                    return value;
                }
            },
            align : 'center'
        }, {
            field : 'maxPrice',
            title : '最高抛售价格',
            formatter : function(value, row, index) {
                var pp = row.currentPrice-row.maxPrice;
                if (pp >=0) {
                    return '<font color="red">'+value+'(可抛售)</font>';
                }else {
                    return value;
                }
            },
            align : 'center'
        }, {
            field : 'score',
            title : '评分',
            sortable : true,
            formatter : function(value, row, index) {
                if(value<0){
                    return '<a href="javascript:showScoreStat('+row.id+');"><font color="#006400">'+value+'</font></a>';
                }else if(value>=85){
                    return '<a href="javascript:showScoreStat('+row.id+');"><font color="red">'+value+'★</font></a>';
                }else if(value<=40){
                    return '<a href="javascript:showScoreStat('+row.id+');"><font color="green">'+value+'</font></a>';
                }else {
                    return '<a href="javascript:showScoreStat('+row.id+');"><font color="black">'+value+'</font></a>';
                }
            },
            align : 'center'
        },{
            field : 'aa',
            title : '分析',
            formatter : function(value, row, index) {
                return '<a href="javascript:showAnalyse('+row.id+');"><img src="../../static/image/sum.png"></img></a>';
            },
            align : 'center'
        },{
            field : 'bb',
            title : '重新统计',
            formatter : function(value, row, index) {
                return '<a href="javascript:resetPriceStat('+row.id+');"><img src="../../static/image/sum.png"></img></a>';
            },
            align : 'center'
        },{
            field : 'remind',
            title : '监控价格',
            formatter : function(value, row, index) {
                if(value==null||value==false){
                    return '<img src="../../static/image/cross.png"></img>';
                }else{
                    return '<img src="../../static/image/tick.png"></img>';
                }
            },
            align : 'center'
        },{
            field : 'status',
            title : '状态',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        },{
            field : 'createdTime',
            title : '创建时间',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
            id : 'statBtn',
            text : '账户统计',
            iconCls : 'icon-stat',
            handler : statSharesAccount
        }, '-', {
            id : 'analyseBtn',
            text : '股票分析',
            iconCls : 'icon-stat',
            handler : showAnalyse
        }, '-', {
            id : 'monitorConfigBtn',
            text : '监控配置',
            iconCls : 'icon-setting',
            handler : showUpdateMonitorConfig
        }, '-', {
            id : 'scoreConfigBtn',
            text : '评分配置',
            iconCls : 'icon-setting',
            handler : openScoreConfig
        }, '-', {
            id : 'statSharesBtn',
            text : '股票实时统计',
            iconCls : 'icon-stat',
            handler : statShares
        }, '-', {
            id : 'warnStatBtn',
            text : '警告统计',
            iconCls : 'icon-stat',
            handler : openWarnStat
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var formData = {
        type:'INSURANCE',
        period:'MONTHLY',
        status:'ENABLE',
		remind:true
    };
    $('#ff').form('load', formData);
}

function initForm(){
}

function loadSearchForm(){
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/userShares/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/userShares/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/userShares/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/userShares/delete';
    commonDeleteByIds(delUrlPrefix);
}

function showAnalyse(id) {
    $('#userShares-analyse-window').window('open');
    $('#userSharesList').combotree({
        url : getFullApiUrl('/userShares/getUserSharesTree?needRoot=true'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onChange : function (newValue, oldValue) {
            if(newValue<0){
                //大盘
                analyse(4);
            }else{
                analyse(0);
            }
        }
    });
    $('#userSharesList').combotree('setValue', id);
    $('#grid').datagrid('clearSelections');
    if(id!=null&&id>0){
        analyse(0);
    }
}

function analyse(type){
    var para =form2Json("userShares-analyse-search-form");
    var url='/userShares/get?id='+para.userSharesId;
    doAjax(null,url,'GET',false,function(data){
        if(para.userSharesId<0){
            //大盘
            document.getElementById("sharesContent").innerHTML='大盘['+data.name+']当前点数:'+data.point.toFixed(0)+'(当前价格:￥'+data.currentPrice.toFixed(2)+')';
        }else{

            if(data.currentPrice>=data.buyPrice){
                document.getElementById("sharesContent").innerHTML='<font color="red">当前价格:￥'+data.currentPrice+'(买入价格:￥'+data.buyPrice+')</font>';
            }else {
                document.getElementById("sharesContent").innerHTML='<font color="green">当前价格:￥'+data.currentPrice+'(买入价格:￥'+data.buyPrice+')</font>';
            }
        }
        if(type==4){
            //本地数据
            statSharesPriceAnalyse();
        }else{
            var img='';
            if(type==0){
                //日K线图
                img ='http://image.sinajs.cn/newchart/daily/n/'+data.code+'.gif';
            }else if(type==1){
                img ='http://image.sinajs.cn/newchart/min/n/'+data.code+'.gif';
            }else if(type==2){
                img ='http://image.sinajs.cn/newchart/weekly/n/'+data.code+'.gif';
            }else if(type==3){
                img ='http://image.sinajs.cn/newchart/monthly/n/'+data.code+'.gif';
            }
            document.getElementById("container").innerHTML='<img src="'+img+'" height="420" width="900" />';
        }
    });
}

//本地统计
function statSharesPriceAnalyse(){
    var para =form2Json("userShares-analyse-search-form");
    var url='/userShares/analyse';
    doAjax(para,url,'GET',false,function(data){
        document.getElementById("containerId").innerHTML='<div id="container"\n' +
            '\t\t\t\t\t\t\t\t\t style="min-width: 900px; height: 420px; margin: 0 auto"></div>';
        createLineDataEnhanced(data,'container');
    });
}


function displayCurrentPrice(){
    var para =form2Json("ff");
    var url='/userShares/getCurrentPrice?code='+para.code;
    doAjax(null,url,'GET',false,function(data){
        document.getElementById("sharesCurrentPrice").innerText='当前价格:￥'+data.currentPrice;
        var formData = {
            name : data.name,
            buyPrice:data.currentPrice
        };
        $('#ff').form('load', formData);
    });
}

//本地统计
function statSharesAccount(){
    var url='/userShares/stat';
    doAjax(null,url,'GET',false,function(data){
        var ss ="购买时总资产:"+data.op.toFixed(2)+"元.<br>";
        ss+="当前总资产:"+data.cp.toFixed(2)+"元.<br>";;
        if(data.op<data.cp){
            ss+="<font color=\"red\">盈利:"+(data.cp-data.op)+"元.</font>";
        }else{
            ss+="<font color=\"green\">亏损:"+(data.op-data.cp)+"元.</font>";
        }
        $.messager.alert('账户统计', ss, 'info');
    });
}


function showUpdateMonitorConfig() {
    var url='/userShares/getMonitorConfig';
    doAjax(null,url,'GET',false,function(data){
        $('#update-monitor-config-window').window('open');
        $('#update-monitor-config-form').form('clear');
        $('#update-monitor-config-form').form('load', data);
    });
}

function updateMonitorConfig(){
    var para =form2Json("update-monitor-config-form");
    var url='/userShares/updateMonitorConfig';
    doAjax(para,url,'POST',true,function(){
        $('#update-monitor-config-window').window('close');
    });
}

function statShares() {
    $.messager.confirm('提示信息', '是否要统计各个股票信息且发送消息', function(data) {
        if (data) {
            var url='/userShares/statShares';
            doAjax(null,url,'POST',true,function(){
            });
        }
    });
    $('#grid').datagrid('clearSelections');
}

function resetPriceStat(id) {
    $.messager.confirm('提示信息', '是否要重新统计该股票信息', function(data) {
        if (data) {
            var url='/userShares/resetPriceStat?id='+id;
            doAjax(null,url,'POST',true,function(){
                reloadDatagrid();
            });
        }
    });
    $('#grid').datagrid('clearSelections');
}

function openWarnStat() {
    $('#warn-stat-window').window('open');
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31'
    };
    $('#warn-stat-search-form').form('load', formData);
    $('#userSharesList2').combotree({
        url : getFullApiUrl('/userShares/getUserSharesTree?needRoot=true'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onChange : function (newValue, oldValue) {
            warnStat();
        }
    });
    $('#warnTypeList').combobox({
        onChange:function(newValue,oldValue){
            warnStat();
        }
    });
    $('#dateGroupTypeList').combobox({
        onChange:function(newValue,oldValue){
            warnStat();
        }
    });
    $('#dataGroupTypeList').combobox({
        onChange:function(newValue,oldValue){
            warnStat();
        }
    });
    warnStat();
}

function warnStat() {
    var vurl = '/userShares/warnDateStat';
    var para = form2Json("warn-stat-search-form");
    doAjax(para, vurl, 'GET',false, function(data) {
        if(para.dataGroupType=='COUNT'){
            if(para.dateGroupType=='DAYCALENDAR'){
                createCalanderDataEnhanced(data,'warnStatContainer');
            }else if(para.chartType=='LINE'){
                createLineDataEnhanced(data,'warnStatContainer');
            }else{
                createBarDataEnhanced(data,'warnStatContainer');
            }
        }else{
            //生成饼图
            createPieDataEnhanced(data,'warnStatContainer');
        }

    });
}

function showScoreStat(id) {
    $('#score-stat-window').window('open');
    $('#score-stat-form').form('clear');
    $('#grid').datagrid('clearSelections');
    $('#userSharesList3').combotree({
        url : getFullApiUrl('/userShares/getUserSharesTree'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onChange : function (newValue, oldValue) {
            $('#score-stat-form').form('clear');
            statScore('FORWARD');
        }
    });
    $('#userSharesList3').combotree('setValue', id);
}

function statScore(type) {
    if(type==null||type==''){
        type='FORWARD';
    }
    var url='/userShares/getScoreStat?';
    $('#grid').datagrid('clearSelections');
    var para = form2Json("score-stat-search-form");
    para.id=$('#scoreStatId').val();
    para.type=type;
    doAjax(para,url,'GET',false,function(data){
        $('#score-stat-window').window('open');
        $('#score-stat-form').form('clear');
        $('#score-stat-form').form('load', data.scoreInfo);
        //加载雷达图
        createRadarDataEnhanced(data.scoreStatData,'scoreStatContainer');
    });
}


function openScoreConfig() {
    var url='/userShares/getScoreConfig';
    doAjax(null,url,'GET',false,function(data){
        $('#score-config-window').window('open');
        $('#score-config-form').form('clear');
        $('#score-config-form').form('load', data);
    });
}

function saveScoreConfig() {
    var url='/userShares/editScoreConfig';
    doFormSubmit('score-config-form',url,function(){
        closeWindow('score-config-window');
    });
}

function sharesCodeHelp() {
    var msg="1. 沪市（上交所）的以sh作为前缀<br>";
    msg+='2. 深市（深交所）的以sz作为前缀<br>';
    $.messager.alert('股票代码说明', msg, '');
}