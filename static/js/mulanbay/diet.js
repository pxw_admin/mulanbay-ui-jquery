$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/diet/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'foods',
            title : '食物',
            formatter : function(value, row, index) {
                if(row.score<3){
                    return '<font color="red">'+value+'</font>';
                }else{
                    return value;
                }
            }
        }, {
            field : 'tags',
            title : '标签'
        },{
            field : 'foodTypeName',
            title : '食物类型',
            align : 'center'
        },{
			field : 'dietTypeName',
			title : '餐次',
            align : 'center'
        },{
            field : 'dietSourceName',
            title : '来源',
            align : 'center'
        }, {
            field : 'shop',
            title : '店铺/品牌'
        }, {
            field : 'score',
            title : '评分',
            align : 'center'
        }, {
            field : 'price',
            title : '价格',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            },
            align : 'center'
        }, {
            field : 'location',
            title : '地点'
        },{
            field : 'occurTime',
            title : '发生时间',
            align : 'center'
        } ,{
            field : 'createdTime',
            title : '创建时间',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
            id : 'createAsTemplateBtn',
            text : '以此为模板新增',
            iconCls : 'icon-add',
            handler : addFromTemplate
        }, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
			id : 'analyseBtn',
			text : '统计',
			iconCls : 'icon-stat',
			handler : openStatDietAnalyse
		}, '-', {
            id : 'compareBtn',
            text : '比对',
            iconCls : 'icon-stat',
            handler : openStatDietCompare
        }, '-', {
            id : 'priceAnalyseBtn',
            text : '价格分析',
            iconCls : 'icon-stat',
            handler : openStatDietPriceAnalyse
        }, '-', {
            id : 'foodsAvgSimilarityBtn',
            text : '多样性分析',
            iconCls : 'icon-stat',
            handler : openStatFoodsAvgSimilarity
        }, '-', {
            id : 'wordCloudBtn',
            text : '饮食词云',
            iconCls : 'icon-stat',
            handler : opensStatWordCloud
        }, '-', {
            id : 'planStatBtn',
            text : '计划执行统计',
            iconCls : 'icon-stat',
            handler : planStat
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var url='/diet/getLastLocation';
    doAjax2(null,url,'GET',false,false,function(data){
        var formData = {
            dietType: 'BREAKFAST',
            dietSource: 'SELF_MADE',
            price:0,
            location:data,
            score:3
        };
        $('#ff').form('load', formData);
    });

}

function loadLastDiet() {
    var para =form2JsonEnhanced("ff");
    var url='/diet/getLastDiet?dietType='+para.dietType+"&dietSource="+para.dietSource;
    doAjax(null,url,'GET',false,function(data){
        if(data){
            data.id=null;
            data.occurTime=null;
            $('#ff').form('load', data);
        }
    });
}

function initForm(){
    getShopTree();
    getTagsTree();
}

function loadSearchForm(){
}


function edit() {
	var rows = getSelectedSingleRow();
	var url='/diet/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
    });
}

function addFromTemplate(){
    var rows = getSelectedSingleRow();
    var url='/diet/get?id='+ rows[0].id;
    doAjax(null,url,'GET',false,function(data){
        $('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
        var dd = new Date();
        var bt = dd.Format('yyyy-MM-dd hh:mm:ss');
        //需要删除id，否则变为修改了
        data.id="";
        data.occurTime=bt;
        $('#ff').form('load', data);
        $('#grid').datagrid('clearSelections');
    });
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/diet/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/diet/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}


function del() {
    var delUrlPrefix = '/diet/delete';
    commonDeleteByIds(delUrlPrefix);
}

function stat(){
	var para =form2JsonEnhanced("search-window");
	var url='/diet/stat';

	doAjax(para,url,'GET',false,function(data){
		$('#stat-window').window('open');
        //生成饼图
        createPieData(data);
	});
}

function planStat(){
    showUserPlanWindow('Diet');
}


function openStatDietCompare(){
    $('#diet-compare-stat-window').window('open');
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31',
        statField:'TOTAL_PRICE'
    };
    $('#diet-compare-stat-search-form').form('load', formData);
    statDietCompare();
}
function statDietCompare(){
    var para =form2Json("diet-compare-stat-search-form");
    var url='/diet/compare';
    doAjax(para,url,'GET',false,function(data){
        createBarDataEnhanced(data,'dietCompareContainer');
    });
}

function openStatDietAnalyse(){
    $('#diet-analyse-stat-window').window('open');
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31',
        page: 1,
        pageSize:10,
        minCount:1,
        field:'FOODS',
        includeUnknown:true
    };
    $('#diet-analyse-stat-search-form').form('load', formData);
    statDietAnalyse();
}

function statDietAnalyse(){
    var para =form2Json("diet-analyse-stat-search-form");
    var url='/diet/analyse';
    doAjax(para,url,'GET',false,function(data){
        if(para.chartType=='PIE'){
            createPieDataEnhanced(data,'dietAnalyseContainer');
        }else if(para.chartType=='TREE_MAP'){
            createTreeMapDataEnhanced(data,'dietAnalyseContainer');
        }else{
            createBarDataEnhanced(data,'dietAnalyseContainer');
        }
    });
}

function openStatDietPriceAnalyse(){
    $('#diet-price-analyse-stat-window').window('open');
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31',
        field:'FOODS',
        minPrice:0,
        dateGroupType:'DAY'
    };
    $('#diet-price-analyse-stat-search-form').form('load', formData);
    statDietPriceAnalyse();
}

function statDietPriceAnalyse(){
    var para =form2Json("diet-price-analyse-stat-search-form");
    var url='/diet/priceAnalyse';
    doAjax(para,url,'GET',false,function(data){
        var dg = para.dateGroupTypeStr;
        if(dg=='DAYCALENDAR'||dg=='DIET_SOURCE'||dg=='FOOD_TYPE'||dg=='DIET_TYPE'){
            createPieDataEnhanced(data,'dietPriceAnalyseContainer');
        }else {
            createBarDataEnhanced(data,'dietPriceAnalyseContainer');
        }
    });
}

function getShopTree(){
    var startDate = getDay(-365);
    $('#hisShopList').combotree({
        url : getFullApiUrl('/diet/getShopTree?needRoot=true&startDate='+startDate),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onSelect:function(node) {
            appendTagBox('shop',node.id);
        }
    });
}

function getTagsTree(){
    var startDate = getDay(-365);
    $('#hisTagsList').combotree({
        url : getFullApiUrl('/diet/getTagsTree?needRoot=true&startDate='+startDate),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onSelect:function(node) {
            appendTagBox('tags',node.id);
        }
    });
}

function showScoreDesc() {
    var msg="评分范围0-5，代表食物的质量，方便后期的饮食跟踪分析，比如哪些食物会引起个人的不适<br>";
    msg+="*5为最高等级，说明该食物对自己非常的有用<br>";
    msg+="*4为表示该食物比较不错，有些惊喜<br>";
    msg+="*3为默认值，表示该食物一般般<br>";
    msg+="*2表示该食物稍微引起自己不适<br>";
    msg+="*1表示该食物严重引起个人不适<br>";
    msg+="*0表示该食物不能食用<br>";
    $.messager.alert('提示', msg, 'info');
}

function openStatFoodsAvgSimilarity(){
    $('#diet-foodsAvgSimilarity-window').window('open');
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31'
    };
    $('#diet-foodsAvgSimilarity-search-form').form('load', formData);
    statFoodsAvgSimilarity();
}

function statFoodsAvgSimilarity(){
    var para =form2Json("diet-foodsAvgSimilarity-search-form");
    var url='/diet/getFoodsAvgSimilarity';
    doAjax(para,url,'GET',false,function(data){
        var avg = (data*100).toFixed(0);
        var gaugeChartData={};
        gaugeChartData.value = avg;
        gaugeChartData.name = '重复度';
        gaugeChartData.title='饮食多样性分析';
        gaugeChartData.subTitle='饮食重复度:'+avg+'%';
        createGaugeChartEnhanced(gaugeChartData,'foodsAvgSimilarityContainer');
    });
}

function opensStatFoodsAvgSimLog(){
    $('#diet-foodsAvgSimLog-window').window('open');
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31'
    };
    $('#diet-foodsAvgSimLog-search-form').form('load', formData);
    statFoodsAvgSimLog();
}

function statFoodsAvgSimLog(){
    var para =form2Json("diet-foodsAvgSimLog-search-form");
    var url='/diet/statFoodsAvgSimLog';
    doAjax(para,url,'GET',false,function(data){
        createLineDataEnhanced(data,'foodsAvgSimLogContainer');
    });
}

function opensStatWordCloud(){
    $('#diet-wordCloud-stat-window').window('open');
    var formData = {
        startDate: getYear(0)+'-01-01',
        endDate: getYear(0)+'-12-31'
    };
    $('#diet-wordCloud-stat-search-form').form('load', formData);
    statWordCloud();
}

function statWordCloud(){
    var urlPara =form2UrlPara("diet-wordCloud-stat-search-form");
    var picWidth = urlPara.picWidth;
    var picHeight = urlPara.picHeight;
    var vurl = '/diet/statWordCloud';
    doAjax(urlPara, vurl, 'GET',false, function(data) {
        var img = "data:image/gif;base64," + data;
        setElementInnerHTML("wordCloudContainer",'<img src="'+img+'" height="'+picHeight+'" width="'+picWidth+'" />');
    });
}