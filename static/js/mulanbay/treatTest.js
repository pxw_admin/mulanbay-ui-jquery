$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/treatTest/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2Json("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
        columns : [ [ {
            field : 'id',
            title : '记录号',
            sortable : true,
            align : 'center'
        }, {
            field : 'treatOperation.treatRecord.hospital',
            title : '医院',
            formatter : function(value, row, index) {
                return row.treatOperation.treatRecord.hospital;
            }
        }, {
            field : 'treatOperation.name',
            title : '手术/检查项目',
            formatter : function(value, row, index) {
                return row.treatOperation.name;
            }
        }, {
            field : 'treatOperation.treatRecord.tags',
            title : '疾病标签',
            formatter : function(value, row, index) {
                return row.treatOperation.treatRecord.tags;
            }
        }, {
            field : 'name',
            title : '名称',
            formatter : function(value, row, index) {
                var ss = '<a href="javascript:showFlowStat('+'\''+value+'\''+');">'+value+'</a>';
                ss+= '&nbsp;<a href="javascript:searchName('+'\''+value+'\''+');"><img src="../../static/image/search.png"></img></a>';
                return ss;
            }
        }, {
            field : 'value',
            title : '检查结果',
            align : 'center'
        }, {
            field : 'aaa',
            title : '参考范围',
            formatter : function(value, row, index) {
                return getScope(row);
            },
            align : 'center'
        }, {
            field : 'unit',
            title : '单位',
            align : 'center'
        }, {
            field : 'result',
            title : '分析结果',
            formatter : function(value, row, index) {
                if(value=='LOWER'){
                    return '<font color="purple">'+row.resultName+'</font>';
                }else if(value=='NORMAL'){
                    return '<font color="green">'+row.resultName+'</font>';
                }else if(value=='HIGHER'){
                    return '<font color="red">'+row.resultName+'</font>';
                }else if(value=='DISEASE'){
                    return '<font color="red">'+row.resultName+'</font>';
                }else {
                    return '--';
                }
            },
            align : 'center'
        }, {
            field : 'typeName',
            title : '分类/试验方法',
            align : 'center'
        }, {
            field : 'testDate',
            title : '采样时间',
            align : 'center'
        }, {
            field : 'aa',
            title : '统计分析',
            formatter : function(value, row, index) {
                return '<a href="javascript:showStat('+'\''+row.name+'\''+');"><img src="../../static/image/sum.png"></img></a>';
            },
            align : 'center'
        }, {
            field : 'treatOperation.treatRecord.disease',
            title : '疾病',
            formatter : function(value, row, index) {
                return row.treatOperation.treatRecord.disease;
            }
        }, {
            field : 'treatOperation.treatRecord.organ',
            title : '器官',
            formatter : function(value, row, index) {
                return row.treatOperation.treatRecord.organ;
            }
        }] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
            id : 'statBtn',
            text : '统计',
            iconCls : 'icon-stat',
            handler : showStat
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function getScope(row) {
    if(row.minValue!=null){
        return row.minValue+'~'+row.maxValue;
    }else if(row.referScope!=null){
        return row.referScope;
    }else {
        return '--';
    }
}

function add() {
    $.messager.alert('提示', '目前该功能不支持，请到看病记录管理中操作!', 'error');
    return;
    $('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
}

function initForm(){

}

function loadSearchForm(){
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/treatTest/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
        data.treatOperationId=data.treatOperation.id;
        $('#ff').form('load', data);
        //$("#treatRecordId").val(data.treatRecord.id);
        //设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/treatTest/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/treatTest/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
		showAll();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/treatTest/delete';
    commonDeleteByIds(delUrlPrefix);
}

function showFlowStat(name) {
    $('#flow-window').window('open');
    $('#grid').datagrid('clearSelections');
    var formData = {
        name: name
    };
    $('#flow-search-form').form('load', formData);
    flowStat()
}

function flowStat() {
    var para =form2Json("flow-search-form");
    para.page=0;
    var url='/treatTest/getData';
    doAjax(para,url,'GET',false,function(data){
        $('#flowPg').datagrid({
            url: '/treatOperation/aa',
            showGroup: true,
            scrollbarSize: 0,
            columns:[[
                {field:'name',title:'手术/检查项目',width:250,align:'center'},
                {field:'value',title:'检查结果',width:100,align:'center'},
                {field:'scope',title:'参考范围',width:100,align:'center'},
                {field:'unit',title:'单位',width:100,align:'center'},
                {field:'result',title:'分析结果',width:100,align:'center'},
                {field:'testDate',title:'采集时间',width:130,align:'center'}
            ]]
        });
        //清空所有数据
        $('#flowPg').datagrid('loadData', { total: 0, rows: [] });
        var rowDatas = data.rows;
        for (var i = 0; i < rowDatas.length; i++) {
            var row = {
                name:rowDatas[i].treatOperation.name,
                value:rowDatas[i].value,
                scope:getScope(rowDatas[i]),
                unit:rowDatas[i].unit,
                result:getResult(rowDatas[i].result),
                testDate:rowDatas[i].testDate
            };
            $('#flowPg').datagrid('appendRow',row);
        }
    });
}

function searchName(name) {
    var para ={
        name:name
    };
    $('#search-window').form('load', para);
    showAll();
    $('#grid').datagrid('clearSelections');
}

function showStatFromFlow() {
    var para =form2Json("flow-search-form");
    showStat(para.name);
}
function showStat(name){
    $('#grid').datagrid('clearSelections');
    $('#stat-window').window('open');
    $('#treatTestNameCategoryList2').combotree({
        url : getFullApiUrl('/treatTest/getTreatTestCategoryTree?needRoot=false'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onChange : function (newValue, oldValue) {
            stat();
        }
    });
    $('#treatTestNameCategoryList2').combotree('setValue', name);
    stat();
}

function stat(){
    var para =form2Json("stat-search-form");
    var url='/treatTest/stat';
    doAjax(para,url,'GET',false,function(data){
        if(data.chartType=='LINE'){
            //生成柱状图
            createLineDataEnhanced(data.chartData,'container');
        }else if(data.chartType=='PIE'){
            //生成饼图
            createPieDataEnhanced(data.chartData,'container');
        }
    });
}

function searchName(name) {
    var para ={
        name:name
    };
    $('#search-window').form('load', para);
    showAll();
    $('#grid').datagrid('clearSelections');
}