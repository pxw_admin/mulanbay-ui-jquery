$(function() {
    loadSearchForm();
    initGrid();
    loadPeriodTree();
});

var dataUrl='/userCalendar/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'title',
            title : '标题',
            formatter : function(value, row, index) {
                var n = tillNowSeconds('',row.expireTime);
                if(n>=0){
                    return '<font color="red">'+value+'</font>';
                }else{
                    return value;
                }
            }
        }, {
            field : 'delayCounts',
            title : '延迟次数',
            formatter : function(value, row, index) {
                if(value>=10){
                    return '<font color="red">'+value+'</font>';
                }else if(value<=5){
                    return '<font color="purple">'+value+'</font>';
                }else if(value<=3){
                    return '<font color="green">'+value+'</font>';
                }else {
                    return value;
                }
            },
            align : 'center'
        },{
            field : 'bussDay',
            title : '开始时间',
            align : 'center'
        },{
			field : 'expireTime',
			title : '结束时间',
            align : 'center'
        }, {
            field : 'calendarConfigId',
            title : '绑定流水',
            formatter : function(value, row, index) {
                if(value==null){
                    return '--';
                }else{
                    return '<img src="../../static/image/tick.png"></img></a>';
                }
            },
            align : 'center'
        }, {
            field : 'sourceTypeName',
            title : '类型',
            align : 'center'
        }, {
            field : 'periodName',
            title : '周期',
            align : 'center'
        }, {
            field : 'allDay',
            title : '全天日历',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        }, {
            field : 'readOnly',
            title : '只读',
            formatter : function(value, row, index) {
                return getStatusImage(value);
            },
            align : 'center'
        }, {
            field : 'finishTypeName',
            title : '完成类型',
            formatter : function(value, row, index) {
                if(row.finishType==null){
                    return '<font color="red">未完成</font>';;
                }else{
                    var ss ='';
                    if(row.finishType=='AUTO'){
                        ss = '<font color="green">'+value+'</font>';
                    }else if(row.finishType=='MANUAL'){
                        ss = '<font color="#00008b">'+value+'</font>';
                    }else if(row.finishType=='EXPIRED'){
                        ss = '<font color="#1e90ff">'+value+'</font>';
                    }else{
                        ss = value;
                    }
                    return ss;
                }
            },
            align : 'center'
        }, {
            field : 'op',
            title : '操作',
            formatter : function(value, row, index) {
                if(row.finishType==null){
                    return '<a href="javascript:finishUserCalendar('+row.id+');"><img src="../../static/image/close.png"></img></a>';
                }else{
                    return '<a href="javascript:reOpenUserCalendar('+row.id+');"><img src="../../static/image/open.png"></img></a>';
                }
            },
            align : 'center'
        },{
            field : 'finishedTime',
            title : '完成时间',
            align : 'center'
        }, {
            field : 'finishSourceId',
            title : '完成消息内容',
            formatter : function(value, row, index) {
                if(value!=null){
                    return '<a href="javascript:showFinishMessageDetail('+row.finishSourceId+');"><img src="../../static/image/info.png"></img></a>';
                }else{
                    return '--';
                }
            },
            align : 'center'
        } , {
            field : 'messageId',
            title : '源消息内容',
            formatter : function(value, row, index) {
                if(value!=null){
                    return '<a href="javascript:showMessageDetail('+row.messageId+');"><img src="../../static/image/info.png"></img></a>';
                }else{
                    return '--';
                }
            },
            align : 'center'
        },{
            field : 'bussIdentityKey',
            title : '唯一标识',
            align : 'center'
        } ,{
            field : 'createdTime',
            title : '创建时间',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
            id : 'statBtn',
            text : '统计',
            iconCls : 'icon-stat',
            handler : dailyCountStat
        }, '-', {
            id : 'planStatBtn',
            text : '计划执行统计',
            iconCls : 'icon-stat',
            handler : planStat
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function getUserCalendarConfigTree(){
    $('#userCalendarConfigList').combotree({
        url : getFullApiUrl('/userCalendarConfig/getUserCalendarConfigForUserTree?needRoot=true'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onClick: function(node){
            loadStatValueConfig(node.id,'CALENDAR',null);
        }
    });
}

function loadStatValueConfig(fid,type,bindValues){
    if(fid==null||fid==undefined){
        //说明没有绑定
        return;
    }
    //加载模板内容
    var url='/statValueConfig/getConfigs?fid='+ fid+'&type='+type;
    doAjax(null,url,'GET',false,function(data){
        $("#selectList").html('');
        var html='';
        if(data==null||data.length==0){
            $("#selectList").html('');
            return;
        }
        for(var i=0;i<data.length;i++){
            if(data[i].list.length==0){
                $.messager.alert('提示', data[i].promptMsg, 'info');
                $("#selectList").html('');
                return;
            }else{
                html+=data[i].name+':&nbsp;&nbsp;';
                var vid ='bindValuesList'+i;
                html+='<select class="selector" id="'+vid+'" class="'+vid+'" name="bindValues" style="width:90px;height:25px">\n';
                for(var j=0;j<data[i].list.length;j++){
                    if(isSelectValueEquals(bindValues,i,data[i].list[j].id)){
                        html+='<option selected="selected" value="'+data[i].list[j].id+'">'+data[i].list[j].text+'</option>\n';
                    }else{
                        html+='<option value="'+data[i].list[j].id+'">'+data[i].list[j].text+'</option>\n';
                    }
                }
                html+='</select>';
            }
            $("#selectList").html(html);
        }
    });
}

//判断下拉框是否选中
function isSelectValueEquals(bindValues,index,vid) {
    if(bindValues==null||bindValues==''){
        return false;
    }else{
        var strs= new Array();
        strs=bindValues.split(",");
        if(strs[index]==vid){
            return true;
        }
    }
    return false;
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    getUserCalendarConfigTree();
    var formData = {
        bussIdentityKey: 'manual_'+(new Date().Format("yyyyMMddhhmmss")),
        sourceType: 'MANUAL',
        allDay:false,
        period:'ONCE'
    };
    $('#ff').form('load', formData);
}

function initForm(){
}

function loadSearchForm(){
    combotreeLoad('periodSearchList','/common/getEnumTree?enumClass=PeriodType&needRoot=true');
}

function loadPeriodTree() {
    $('#periodList').combotree({
        url : getFullApiUrl('/common/getEnumTree?enumClass=PeriodType'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onSelect:function(node) {
            setPeriodValuesHtml(node.id,'');
        }
    });
}
function setPeriodValuesHtml(period,periodValues) {
    var html='';
    if(period=='WEEKLY'){
        html ='周一<input name="periodValues" type="checkbox" value="1" style="width:20px;"/>&nbsp;' +
            '周二<input name="periodValues" type="checkbox" value="2" style="width:20px;"/>&nbsp;' +
            '周三<input name="periodValues" type="checkbox" value="3" style="width:20px;"/>&nbsp;' +
            '周四<input name="periodValues" type="checkbox" value="4" style="width:20px;"/>&nbsp;' +
            '周五<input name="periodValues" type="checkbox" value="5" style="width:20px;"/>&nbsp;' +
            '周六<input name="periodValues" type="checkbox" value="6" style="width:20px;"/>&nbsp;' +
            '周日<input name="periodValues" type="checkbox" value="7" style="width:20px;"/>&nbsp;';
    }else if(period=='MONTHLY'){
        for(var i=1;i<=31;i++){
            if(i<=9){
                html+='&nbsp;&nbsp;';
            }
            html+=i+'号<input name="periodValues" type="checkbox" value="'+i+'" style="width:20px;"/>';
            if(i%10==0){
                html+='<br>';
            }
        }
    }
    setElementInnerHTML('periodValueList',html);
    if(isStrEmpty(periodValues)==false){
        var pvs = periodValues.split(',');
        $('#ff').form('load', {periodValues:pvs});
    }
}
function edit() {
	var rows = getSelectedSingleRow();
	var url='/userCalendar/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
        getUserCalendarConfigTree();
        $('#ff').form('load', data);
        $('#userCalendarConfigList').combotree('setValue', data.calendarConfigId);
        loadStatValueConfig(data.calendarConfigId,'CALENDAR',data.bindValues);
        setPeriodValuesHtml(data.period,data.periodValues);
        //设置字符
		$('#grid').datagrid('clearSelections');
    });
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/userCalendar/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/userCalendar/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}


function del() {
    var delUrlPrefix = '/userCalendar/delete';
    commonDeleteByIds(delUrlPrefix);
}

function dailyCountStat(){
	var url='/userCalendar/dailyCountStat';
	doAjax(null,url,'GET',false,function(data){
		$('#dailyCountStat-window').window('open');
        //生成饼图
        createLineData(data);
	});
}

function planStat(){
    showUserPlanWindow('UserCalendar');
}

function refreshRate() {
    var url='/userCalendar/refreshRate';
    doAjax(null,url,'GET',false,function(data){
        $.messager.alert('成功', data, 'info');
        reloadDatagrid();
    });
}

function showMessageDetail(id) {
    $('#grid').datagrid('clearSelections');
    var url='/userMessage/getByUser?id='+ id;
    doAjax(null,url,'GET',false,function(data){
        if(data){
            $.messager.alert(data.title, data.content, 'info');
        }else{
            $.messager.alert('提示', '未找到原始的消息内容', 'info');
        }
    });
}

function showFinishMessageDetail(id) {
    $('#grid').datagrid('clearSelections');
    var url='/userMessage/getByUser?id='+ id;
    doAjax(null,url,'GET',false,function(data){
        $.messager.alert(data.title, data.content, 'info');
    });
}


function finishUserCalendar(id) {
    $.messager.confirm('提示信息', '是否手动设置为已完成', function(data) {
        if (data) {
            var url='/userCalendar/finish';
            var para ={
                id:id
            };
            doAjax(para,url,'POST',false,function() {
                $('#grid').datagrid('clearSelections');
                reloadDatagrid();
            });
        }else{
            $('#grid').datagrid('clearSelections');
        }
    });

}

function reOpenUserCalendar(id) {
    $.messager.confirm('提示信息', '是否重新开启该日历', function(data) {
        if (data) {
            var url='/userCalendar/reOpen';
            var para ={
                id:id
            };
            doAjax(para,url,'POST',false,function() {
                $('#grid').datagrid('clearSelections');
                reloadDatagrid();
            });
        }else{
            $('#grid').datagrid('clearSelections');
        }
    });

}