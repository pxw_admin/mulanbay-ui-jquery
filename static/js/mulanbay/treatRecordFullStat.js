$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/treatRecord/fullStat';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2Json("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            //edit();
        },
        onLoadError: loadDataError,
        columns : [ [ {
            field : 'id',
            title : '记录号',
            sortable : true,
            align : 'center'
        }, {
            field : 'tags',
            title : '疾病'
        }, {
            field : 'totalCount',
            title : '看病总次数',
            align : 'center'
        }, {
            field : 'minTreatDate',
            title : '初次看病时间'
        }, {
            field : 'maxTreatDate',
            title : '最近一次看病时间'
        },{
            field : 'aa',
            title : '时长',
            formatter : function(value, row, index) {
                var dd = new Date(Date.parse(row.maxTreatDate.replace(/-/g,"/")));
                var bd = new Date(Date.parse(row.minTreatDate.replace(/-/g,"/")));
                var days = tillNowDays(bd,dd);
                return formatDays(days);
            },
            align : 'center'
        }, {
            field : 'totalFee',
            title : '总共花费',
            align : 'center',
            formatter : function(value, row, index) {
                return '<font color="red">'+formatMoneyWithSymbal(value)+'</font>';
            },
        } , {
            field : 'medicalInsurancePaidFee',
            title : '医保花费',
            align : 'center',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'personalPaidFee',
            title : '个人支付费用',
            align : 'center',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'bb',
            title : '检验报告',
            formatter : function(value, row, index) {
                return '<a href="javascript:showTreatTest('+'\''+row.tags+'\''+');"><img src="../../static/image/sum.png"></img></a>';
            },
            align : 'center'
        }] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function initForm(){

}

function loadSearchForm(){
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}


function showTreatTest(tags) {
    $('#treatTest-window').window('open');
    $('#tagsList').combotree({
        url : getFullApiUrl('/treatRecord/getTagsTree?needRoot=true'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onSelect:function(node) {
        }
    });
    var para ={
        tags:tags
    };
    $('#treatTest-window').form('load', para);
    $('#grid').datagrid('clearSelections');
    initGridTreatTest();
}