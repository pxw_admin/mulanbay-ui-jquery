$(function () {
    initGrid();
});

var dataUrl = '/backup/getData';

function initGrid() {
    $('#grid').datagrid({
        iconCls: 'icon-save',
        url: getFullApiUrl(dataUrl),
        method: 'GET',
        loadFilter: function (data) {
            return loadDataFilter(data);
        },
        idField: 'id',
        loadMsg: '正在加载数据...',
        pageSize: 30,
        queryParams: form2JsonEnhanced("search-window"),
        remoteSort: false,
        frozenColumns: [[{
            field: 'ID',
            checkbox: true
        }]],
        onLoadError: loadDataError,
        columns: [[
            {
                field: 'id',
                title: '序号',
                formatter: function (value, row, index) {
                    return index+1;
                },
                align: 'center'
            }, {
                field: 'fileName',
                title: '文件名',
                formatter: function (value, row, index) {
                    var ss = value;
                    var dd = dateDiff(row.lastModifyTime,new Date());
                    if(dd<1){
                        ss+='<img src="../../static/image/new2.png"></img>';
                    }
                    return ss;
                }
            }, {
                field: 'bb',
                title: '类型',
                formatter: function (value, row, index) {
                    var img = '<img src="../../static/image/file.png"></img>';
                    if(row.directory==true){
                        img = '<img src="../../static/image/folder.png"></img>';
                    }else{
                        var index = row.fileName.lastIndexOf(".");
                        if(index>=0){
                            var ft = row.fileName.substr(index+1).toLowerCase();
                            if(ft=='sql'){
                                img = '<img src="../../static/image/database.png"></img>';
                            }else if(ft=='gz'||ft=='rar'||ft=='zip'){
                                img = '<img src="../../static/image/zip.png"></img>';
                            }
                        }
                    }
                    return img;
                },
                align: 'center'
            }, {
                field: 'size',
                title: '大小',
                formatter: function (value, row, index) {
                    return bytesToSize(value);
                },
                align: 'center'
            }, {
                field: 'path',
                title: '路径',
                width: 320,
                formatter: function (value, row, index) {
                    if (value.length > 35) {
                        var ss = encodeURI(value);
                        return value.substring(0, 45) + '<a href="javascript:showDetail(' + '\'' + value + '\'' + ');"> ......</a>';
                    }
                    return value;
                }
            }, {
                field: 'directory',
                title: '文件夹',
                formatter: function (value, row, index) {
                    if(value==true){
                        return '<img src="../../static/image/tick.png"></img>';
                    }else{
                        return '--';
                    }
                },
                align: 'center'
            }, {
                field: 'aa',
                title: '下载',
                formatter: function (value, row, index) {
                    return '<img src="../../static/image/download.png"></img>';
                },
                align: 'center'
            }, {
                field: 'lastModifyTime',
                title: '最后修改时间',
                align: 'center'
            }]],
        pagination: true,
        rownumbers: true,
        singleSelect: false,
        toolbar: [{
            id: 'searchBtn',
            text: '刷新',
            iconCls: 'icon-refresh',
            handler: showAll
        }]
    });
}

function showAll() {
    refreshDatagrid(dataUrl, 1, true);
}

function showDetail(msg) {
    $('#grid').datagrid('uncheckAll');
    $.messager.alert('详情', msg, 'info');
}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};