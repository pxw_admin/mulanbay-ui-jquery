$(function() {
    loadSearchForm();
    initGrid();
});

var dataUrl='/account/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '名称'
        }, {
            field : 'amount',
            title : '金额',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            },
        }, {
            field : 'cardNo',
            title : '卡号'
        }, {
            field : 'typeName',
            title : '类型'
        },{
            field : 'aa',
            title : '分析',
            formatter : function(value, row, index) {
                return '<a href="javascript:openAccountAnalyseWindow('+row.id+');"><img src="../../static/image/sum.png"></img></a>';;
            },
            align : 'center'
        }, {
            field : 'remark',
            title : '说明'
        }, {
            field : 'createdTime',
            title : '创建时间'
        }, {
            field : 'lastModifyTime',
            title : '最后更新时间'
        },{
			field : 'statusName',
			title : '资产状态',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'createBtn',
			text : '新增',
			iconCls : 'icon-add',
			handler : add
		}, '-', {
			id : 'editBtn',
			text : '修改',
			iconCls : 'icon-edit',
			handler : edit
		}, '-', {
			id : 'deleteBtn',
			text : '删除',
			iconCls : 'icon-remove',
			handler : del
		}, '-', {
            id : 'statBtn',
            text : '统计',
            iconCls : 'icon-stat-pie',
            handler : showStat
        }, '-', {
            id : 'snapshotBtn',
            text : '生成快照',
            iconCls : 'icon-stat',
            handler : openCreateSnapshotWindow
        }, '-', {
            id : 'accountAnalyseBtn',
            text : '账户变化',
            iconCls : 'icon-stat',
            handler : openAccountAnalyseWindow
        }, '-', {
            id : 'accountForecastBtn',
            text : '账户预测',
            iconCls : 'icon-stat',
            handler : openAccountForecastWindow
        }, '-', {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var formData = {
        type:'BANK',
        status:'MOVABLE'
    };
    $('#ff').form('load', formData);

}

function initForm(){
}

function loadSearchForm(){
}

function edit() {
	var rows = getSelectedSingleRow();
	var url='/account/get?id='+ rows[0].id;
	doAjax(null,url,'GET',false,function(data){
		$('#eidt-window').window('open');
        $('#ff').form('clear');
        initForm();
		$('#ff').form('load', data);
		//设置字符
		$('#grid').datagrid('clearSelections');
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function saveData() {
    var url='/account/edit';
    if($("#id").val()==null||$("#id").val()==''){
        url='/account/create';
    }
	doFormSubmit('ff',url,function(){
		closeWindow('eidt-window');
		$('#grid').datagrid('clearSelections');
        reloadDatagrid();
	});
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

function del() {
    var delUrlPrefix = '/account/delete';
    commonDeleteByIds(delUrlPrefix);
}

function showStat() {
    $('#account-stat-window').window('open');
    loadSnapshotList(null,null);
    stat();
}

function loadSnapshotList(maxId,minId) {
    var mai='';
    if(maxId!=null){
        mai=maxId;
    }
    var mii='';
    if(minId!=null){
        mii=minId;
    }
    $('#snapshotList').combotree({
        //取最新的20条
        url : getFullApiUrl('/accountSnapshotInfo/getSnapshotInfoTree?needRoot=true&page=1&pageSize=20&maxId='+mai+'&minId='+mii),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onChange : function (newValue, oldValue) {
            stat();
        },
        onLoadSuccess : function (node,data) {
            //alert(JSON.stringify(data));
            var md = data[0].children;
            //alert(JSON.stringify(md));
            if(maxId!=null){
                $('#snapshotList').combotree('setValue', { id: md[0].id, text: md[0].text });
            }
            if(minId!=null){
                var l = md.length;
                $('#snapshotList').combotree('setValue', { id: md[l-1].id, text: md[l-1].text });
            }
        }
    });
}

function preShapshot() {
    var minId = $('#snapshotList').combotree('getValue');
    loadSnapshotList(null,minId);
    stat();
}

function nextSnapshot() {
    var maxId = $('#snapshotList').combotree('getValue');
    loadSnapshotList(maxId,null);
    stat();
}

function stat(){
    var para =form2Json("account-stat-search-form");
    var url='/account/stat';

    doAjax(para,url,'GET',false,function(data){
        $('#stat-form').form('clear');
        //生成饼图
        createPieData(data);
    });
}

function openCreateSnapshotWindow() {
    $('#create-snapshot-window').window('open');
}

function createSnapshot(){
	var msg="是否要生成快照";
    $.messager.confirm('提示信息', msg, function(data) {
        if (data) {
            var para =form2JsonEnhanced("create-snapshot-form");
            var vurl='/account/createSnapshot';
            doAjax(para, vurl, 'POST', true, function() {
                $('#create-snapshot-window').window('close');
            });
        }else{
            //$('#grid').datagrid('clearSelections');
        }
    });
}

function openAccountAnalyseWindow(accountId) {
    $('#account-analyse-stat-window').window('open');
    $('#accountList').combotree({
        url : getFullApiUrl('/account/getAccountTree?needRoot=true'),
        valueField : 'id',
        textField : 'text',
        loadFilter: function(data){
            return loadDataFilter(data);
        },
        onChange : function (newValue, oldValue) {
            statAccountAnalyse();
        }
    });
    $('#accountList').combotree('setValue', accountId);
    $('#grid').datagrid('clearSelections');
    statAccountAnalyse();
}

function statAccountAnalyse(){
    var para =form2Json("account-analyse-stat-search-form");
    var url='/accountFlow/analyse';
    doAjax(para,url,'GET',false,function(data){
        createLineDataEnhanced(data,'accountAnalyseContainer');
    });
}

function showChange() {
    $('#change-account-window').window('open');
    $("#changeAccountId").val($("#id").val());
    var formData = {
        afterAmount:null
    };
    $('#change-account-form').form('load', formData);
}

function change(){
    var para =form2Json("change-account-form");
    var url='/account/change';

    doAjax(para,url,'POST',true,function(data){
        closeWindow('eidt-window');
        closeWindow('change-account-window');
        reloadDatagrid();
    });
}

function deleteSnapshot(){
    var msg="是否要删除所选取的快照";
    $.messager.confirm('提示信息', msg, function(data) {
        if (data) {
            var id = $('#snapshotList').combotree('getValue');
            var postData={id:id};
            var vurl='/accountSnapshotInfo/delete';
            doAjax(postData, vurl, 'POST', true, function() {
                showStat();
            });
        }else{
            //$('#grid').datagrid('clearSelections');
        }
    });
}

function openAccountForecastWindow(accountId) {
    $('#account-forecast-stat-window').window('open');
    loadCurrentAccountData();
}

function statAccountForecast(){
    var para =form2Json("account-forecast-stat-search-form");
    var url='/account/forecast';
    doAjax(para,url,'GET',false,function(data){
        createLineDataEnhanced(data,'accountForecastContainer');
    });
}

function loadCurrentAccountData(){
    var url='/account/getCurrentAccountInfo';
    doAjax(null,url,'GET',false,function(data){
        var formData = {
            currentAmount:data.currentAccountAmount,
            monthlySalary:data.lastMonthSalary,
            monthlyOtherIncome:0,
            monthlyConsume:data.lastMonthConsume,
            yearlyIncome:0,
            yearlySalaryRate:10,
            yearlyAmountRate:0,
            fixExpend:0,
            amountInvestRate:50
        };
        $('#account-forecast-stat-search-form').form('load', formData);
    });
}

function forecastHelp() {
    var msg="1.月类型收入支出将在每月算入<br>";
    msg+='2.年类型收入支出将在第二年的第一个月算入<br>';
    msg+='3.每年年终收益指的是,比如：年终奖之类<br>';
    msg+='4.其他固定支出指的是，比如：突发类的大件物品如笔记本电脑<br>';
    $.messager.alert('预测规则说明', msg, '');
}