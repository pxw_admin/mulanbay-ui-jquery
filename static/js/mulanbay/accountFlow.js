$(function() {
    loadSearchForm();
	initGrid();
});

var dataUrl='/accountFlow/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'name',
            title : '账户'
        }, {
            field : 'cardNo',
            title : '卡号'
        }, {
            field : 'typeName',
            title : '账户类型'
        }, {
            field : 'beforeAmount',
            title : '调整前',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'afterAmount',
            title : '调整后',
            formatter : function(value, row, index) {
                return formatMoneyWithSymbal(value);
            }
        }, {
            field : 'adjustTypeName',
            title : '调整类型'
        }, {
            field : 'bussKey',
            title : '快照名称',
            formatter : function(value, row, index) {
                if(row.snapshotInfo!=null){
                    return row.snapshotInfo.name+'('+row.snapshotInfo.bussKey+")";
                }
            }
        }, {
            field : 'remark',
            title : '说明'
        }, {
            field : 'createdTime',
            title : '创建时间'
        }, {
            field : 'lastModifyTime',
            title : '最后更新时间'
        },{
			field : 'statusName',
			title : '资产状态',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [ {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		} ]
	});
}

function add() {
	$('#eidt-window').window('open');
	$('#ff').form('clear');
	initForm();
    var formData = {
        type:'SALARY',
        status:'ENABLE'
    };
    $('#ff').form('load', formData);

}

function loadSearchForm(){
    combotreeLoad('accountList','/account/getAccountTree?needRoot=true');
}


function initForm(){
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function getSelectedIds() {
	var ids = [];
	var rows = getSelectedRows();
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	return ids;
}

