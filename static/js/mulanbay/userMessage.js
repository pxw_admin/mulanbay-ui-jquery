$(function() {
	initGrid();
});

var dataUrl='/userMessage/getData';

function initGrid(){
	$('#grid').datagrid({
		iconCls : 'icon-save',
		url : getFullApiUrl(dataUrl),
		method:'GET',
		loadFilter: function(data){
			return loadDataFilter(data);
		},
		idField : 'id',
		loadMsg : '正在加载数据...',
		pageSize : 30,
		queryParams: form2JsonEnhanced("search-window"),
		remoteSort : false,
		frozenColumns : [ [ {
			field : 'ID',
			checkbox : true
		} ] ],
		onDblClickRow: function (rowIndex, rowData) {
            $('#grid').datagrid('uncheckAll');
            $('#grid').datagrid('checkRow', rowIndex);
            //edit();
        },
        onLoadError: loadDataError,
		columns : [ [ {
			field : 'id',
			title : '记录号',
			sortable : true,
			align : 'center'
		}, {
            field : 'userId',
            title : '用户ID',
            align : 'center'
        }, {
            field : 'title',
            title : '标题',
            align : 'center'
        }, {
            field : 'code',
            title : '代码',
            align : 'center'
        }, {
            field : 'sendStatus',
            title : '发送状态',
            formatter : function(value, row, index) {
                if (value == 'SEND_SUCCESS') {
                    return '<img src="../../static/image/tick.png"></img>';
                }else if (value == 'SEND_FAIL') {
                    return '<img src="../../static/image/cross.png"></img>';
                }else{
                    return row.sendStatusName;
                }
            },
            align : 'center'
        },{
            field : 'aa',
            title : '重发',
            formatter : function(value, row, index) {
                var ss='<a href="javascript:resend('+row.id+');"><img src="../../static/image/reload.png"></img></a>';
                return ss;
            },
            align : 'center'
        }, {
            field : 'failCount',
            title : '失败次数',
            formatter : function(value, row, index) {
                if(value==null||value==0){
                    return '0';
                }else{
                    return '<font color="red">'+value+'</font>';
                }
            },
            align : 'center'
        }, {
            field : 'content',
            title : '内容',
            formatter : function(value, row, index) {
                return '<a href="javascript:showLogDetail('+row.id+');"><img src="../../static/image/info.png"></img></a>';
            },
            align : 'center'
        },{
            field : 'expectSendTime',
            title : '期望发送时间',
            align : 'center'
        },{
            field : 'lastSendTime',
            title : '最后一次发送时间',
            align : 'center'
        }, {
            field : 'tt',
            title : '延迟时间',
            formatter : function(value, row, index) {
                if(row.lastSendTime==null){
                    return '--';
                }else{
                    var s = tillNowSeconds(row.expectSendTime,row.lastSendTime);
                    if(s<=10){
                        return '<font color="green">实时</font>';
                    }else if(s<=10){
                        return '<font color="green">'+s+'秒</font>';
                    }else if(s<=60){
                        return s+'秒';
                    }else if(s<=600){
                        return '<font color="#c71585">'+formatSeconds(s)+'</font>';
                    }else {
                        return '<font color="red">'+formatSeconds(s)+'</font>';
                    }
                }
            },
            align : 'center'
        },{
            field : 'nodeId',
            title : '服务器节点',
            align : 'center'
        } ,{
            field : 'createdTime',
            title : '创建时间',
            align : 'center'
        } ] ],
		pagination : true,
		rownumbers : true,
		singleSelect : false,
		toolbar : [  {
			id : 'searchBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : showAll
		},{
            id : 'sendBtn',
            text : '手动发送',
            iconCls : 'icon-add',
            handler : showSend
        }]
	});
}

function showAll() {
	refreshDatagrid(dataUrl,1,true);
}

function showLogDetail(id) {
    var url='/userMessage/get?id='+ id;
    doAjax(null,url,'GET',false,function(data){
        $('#grid').datagrid('uncheckAll');
        $.messager.alert('详情', data.content, 'info');
    });
}

function resend(id) {
    $('#grid').datagrid('uncheckAll');
    $.messager.confirm('提示信息', '是否确定要重新发送', function(data) {
        if (data) {
            var url='/userMessage/resend?id='+ id;
            doAjax(null,url,'GET',true,function(){
                reloadDatagrid();
            });
        }
    });

}

function showSend() {
    $('#eidt-window').window('open');
    $('#ff').form('clear');
    var formData = {
        notifyTime:getNowDateTimeString()
    };
    $('#ff').form('load', formData);
}

function send(){
    var para =form2JsonEnhanced("ff");
    var url='/userMessage/send';
    doAjax(para,url,'POST',false,function(data){
        $('#eidt-window').window('close');
        showAll();
        if(para.code==null||para.code==''){
            if(data==true){
                $.messager.alert('发送成功',  '消息发送成功', 'info');
            }else{
                $.messager.alert('发送失败',  '消息发送失败', 'error');
            }
        }else{
            $.messager.alert('发送成功',  '消息发送成功,返回消息编号:'+data, 'info');
        }
    });
}